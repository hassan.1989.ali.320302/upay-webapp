import axios, { AxiosInstance, AxiosResponse } from "axios";

const instance: AxiosInstance = axios.create({
  baseURL: "https://api.upaybd.com/api/v2",
});

export const getHomePage = async (): Promise<AxiosResponse<any>> => {
  return await instance.get("/pages/home");
};

export const getHomeSlider = async (): Promise<AxiosResponse<any>> => {
  return await instance.get("/pages/Home-Slider");
};

export const getWhoWeArePage = async (): Promise<AxiosResponse<any>> => {
  return await instance.get("/pages/who-we-are");
};

export const getPartnerWithUsPage = async (): Promise<AxiosResponse<any>> => {
  return await instance.get("/pages/Partner-with-us");
};

export const getLatestOfferPage = async (): Promise<AxiosResponse<any>> => {
  return await instance.get("/pages/Latest-offer");
};

export const getCashInPage = async (): Promise<AxiosResponse<any>> => {
  return await instance.get("/pages/Cash-in");
};

export const getCashOutPage = async (): Promise<AxiosResponse<any>> => {
  return await instance.get("/pages/Cash-Out");
};

export const getNeedHelpPage = async (): Promise<AxiosResponse<any>> => {
  return await instance.get("/pages/need-help");
};

export const getPayBillPage = async (): Promise<AxiosResponse<any>> => {
  return await instance.get("/pages/Pay-Bill");
};

export const getAddMoneyPage = async (): Promise<AxiosResponse<any>> => {
  return await instance.get("/pages/Add-Money");
};

export const getInnerPage = async (slug): Promise<AxiosResponse<any>> => {
  return await instance.get(`/pages/${slug}`);
};

export const partnerForm = async (data): Promise<AxiosResponse<any>> => {
  return await instance.post(`/partner_with_us/`, data);
};

export const getServiceLocationPage = async (): Promise<AxiosResponse<any>> => {
  return await instance.get(`/pages/Service-location`);
};

export const getLetUsHelp = async (): Promise<AxiosResponse<any>> => {
  return await instance.get(`/pages/Navigation`);
};

export const getMediaCenter = async (): Promise<AxiosResponse<any>> => {
  return await instance.get(`/pages/Media`);
};

export const getMediaContact = async (): Promise<AxiosResponse<any>> => {
  return await instance.get(`/pages/Media-Contact`);
};

export const getNewsRoom = async (): Promise<AxiosResponse<any>> => {
  return await instance.get(`/pages/Newsroom`);
};

export const getBlogPage = async (): Promise<AxiosResponse<any>> => {
  return await instance.get(`/pages/Blog`);
};

export const getChargesPage = async (): Promise<AxiosResponse<any>> => {
  return await instance.get(`/pages/limits-and-charges`);
};

export const getCalc = async (): Promise<AxiosResponse<any>> => {
  return await instance.get(`/calculator/`);
};

export const getChargesTable = async (): Promise<AxiosResponse<any>> => {
  return await instance.get(`/pages/schedule-charges`);
};

export const searchContent = async (
  search_term,
  abortSignal
): Promise<AxiosResponse<any>> => {
  return await instance.post(`/search/`, {
    search_term,
  });
};

export const getApp = async (): Promise<AxiosResponse<any>> => {
  return await instance.get(`pages/Get-App`);
};

export const getFooter = async (): Promise<AxiosResponse<any>> => {
  return await instance.get(`pages/Footer`);
};

export const getDropDown = async (): Promise<AxiosResponse<any>> => {
  return await instance.get(`pages/product-and-campaigns`);
};

export const getFAQ = async (): Promise<AxiosResponse<any>> => {
  return await instance.get(`pages/faq`);
};

export const getTypes = async (): Promise<AxiosResponse<any>> => {
  return await instance.get(`type_list/`);
};

export const getDivisions = async (): Promise<AxiosResponse<any>> => {
  return await instance.get(`division_list/`);
};
