import AwesomeDebouncePromise from "awesome-debounce-promise";
import { useEffect, useState } from "react";
import { useAsyncAbortable } from "react-async-hook";
import useConstant from "use-constant";
import { searchContent } from "../server";

const useSearchService = () => {
  const [inputText, setInputText] = useState("");

  // Debounce the original search async function
  const debouncedSearchStarwarsHero = useConstant(() =>
    AwesomeDebouncePromise(searchContent, 500)
  );

  const suggestiveSearch = useAsyncAbortable(
    async (abortSignal, inputText) => {
      // If the input is empty, return nothing immediately (without the debouncing delay!)
      if (inputText.length === 0) {
        return {
          data: [],
        };
      }
      // Else we use the debounced api
      else {
        return debouncedSearchStarwarsHero(inputText, abortSignal);
      }
    },
    // Ensure a new request is made everytime the text changes (even if it's debounced)
    [inputText]
  );

  // Return everything needed for the hook consumer
  return {
    inputText,
    setInputText,
    suggestiveSearch,
  };
};

export default useSearchService;
