import React, { Component } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

type Props = {
  sliders: [];
};

class StorySlider extends Component<Props> {
  slider: any;
  constructor(props: Props) {
    super(props);
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
  }
  next(): void {
    this.slider.slickNext();
  }
  previous(): void {
    this.slider.slickPrev();
  }

  render() {
    const settings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      // dots: false,
      // centerMode: true,
      arrows: false,
      variableWidth: true,
    };
    return (
      <div>
        <div className="our__story__slide">
          <Slider ref={(c) => (this.slider = c)} {...settings}>
            {this.props.sliders.map(
              (
                slider: {
                  image: string;
                  ["image sd"]: string;
                },
                key: number
              ) => {
                return (
                  <div key={key}>
                    <div className="image__slidePart">
                      <picture>
                        <source
                          media="(max-width: 767px)"
                          srcSet={process.env.IMAGE_URL + slider["image sd"]}
                        />
                        <source
                          media="(min-width: 768px)"
                          srcSet={process.env.IMAGE_URL + slider.image}
                        />
                        <img
                          src={process.env.IMAGE_URL + slider["image sd"]}
                          alt="sliders"
                          // style={{ height: "203px", width: "336px" }}
                        />
                      </picture>
                    </div>
                  </div>
                );
              }
            )}
            {/* <div key={2}>
              <div className="image__slidePart">
                <img src="/images/banner--img.jpg" alt="" />
              </div>
            </div>
            <div key={3}>
              <div className="image__slidePart">
                <img src="/images/banner--img.jpg" alt="" />
              </div>
            </div>
            <div key={4}>
              <div className="image__slidePart">
                <img src="/images/banner--img.jpg" alt="" />
              </div>
            </div>
            <div key={5}>
              <div className="image__slidePart">
                <img src="/images/banner--img.jpg" alt="" />
              </div>
            </div>
            <div key={6}>
              <div className="image__slidePart">
                <img src="/images/banner--img.jpg" alt="" />
              </div>
            </div> */}
          </Slider>
          <div className="arrow__btn--parts">
            <button className="button prev__btn" onClick={this.previous}>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="35"
                height="35"
                viewBox="0 0 35 35"
                fill="none"
              >
                <path
                  d="M21.4676 33.3699L5.74436 18.7633L21.4676 4.15671"
                  stroke="#4E4E50"
                  strokeWidth="3"
                />
              </svg>
            </button>
            <button className="button next__btn" onClick={this.next}>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="35"
                height="35"
                viewBox="0 0 35 35"
                fill="none"
              >
                <path
                  d="M13.5324 33.3699L29.2556 18.7633L13.5324 4.15671"
                  stroke="#4E4E50"
                  strokeWidth="3"
                />
              </svg>
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default StorySlider;
