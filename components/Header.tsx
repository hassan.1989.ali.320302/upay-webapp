import React, { useCallback, useEffect, useRef, useState } from "react";
import Link from "next/link";
import useSearchService from "./SearchService";
import { getDropDown, searchContent } from "../server";
import Buttons from "../translations/button.json";

export enum Locales {
  Bangla,
  English,
}

const Header = () => {
  const [show, setShow] = useState<boolean>(false);
  const [dropDown, setDropDown] = useState(null);
  const [search, setSearch] = useState<boolean>(false);
  const [advanceSearch, setAdvanceSearch] = useState<boolean>(false);
  const [madvanceSearch, setMadvanceSearch] = useState<boolean>(false);
  const onClickSearch = () => setSearch(true);
  const onClickMadvanceSearch = () => setMadvanceSearch(true);
  const [locale, setLocale] = useState<Locales>(Locales.Bangla);
  const { inputText, setInputText, suggestiveSearch } = useSearchService();

  const handle = useCallback((event: any): void => {
    // console.log(event);
    const searchEl = document.querySelector("div.menu__search");
    const advaEl = document.querySelector("div.desk__menu--advancesearch");
    // console.log(
    //   "compare",
    //   event.target.matches("#search-btn"),
    //   !!event.target.closest(".menu__search"),
    //   !searchEl?.contains(event.target),
    //   !advaEl?.contains(event.target),
    //   !event.target.closest(".main__menu--desk")
    // );

    if (!searchEl?.contains(event.target) && !advaEl?.contains(event.target)) {
      // console.log("search?", search);

      setSearch(false);
      setAdvanceSearch(false);
    }

    if (event.target.matches("#search-btn")) {
      onClickSearch();
    }
  }, []);

  useEffect(() => {
    console.log(madvanceSearch);
  }, [madvanceSearch]);

  useEffect(() => {
    document.addEventListener("click", handle, false);

    return () => {
      document.removeEventListener("click", handle, false);
    };
  }, []);

  useEffect(() => {
    fetchDropDown();
    if (typeof window !== "undefined") {
      const lang = +localStorage.getItem("locale");
      !lang && localStorage.setItem("locale", Locales.Bangla.toString());
      setLocale(lang ?? Locales.Bangla);
    }
  }, []);

  const fetchDropDown = async () => {
    try {
      const res = await getDropDown();
      setDropDown(res.data.rows.Data[0]);
    } catch (error) {
      console.error;
    }
  };

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const onClickAdvanceSearch = () => setAdvanceSearch(true);
  const handleAdClose = () => setMadvanceSearch(false);

  const handleLocale = (): void => {
    if (Locales.Bangla === locale) {
      setLocale(Locales.English);
      localStorage.setItem("locale", Locales.English.toString());

      var localeEvent = new CustomEvent("locale", {
        detail: {
          lang: Locales.English,
        },
      });

      window.dispatchEvent(localeEvent);
    }

    if (Locales.English === locale) {
      setLocale(Locales.Bangla);
      localStorage.setItem("locale", Locales.Bangla.toString());

      var localeEvent = new CustomEvent("locale", {
        detail: {
          lang: Locales.Bangla,
        },
      });

      window.dispatchEvent(localeEvent);
    }
  };

  const onSearchClick = () => {
    setSearch(false);
    setAdvanceSearch(false);
    setInputText("");
    setMadvanceSearch(false);
    setShow(false);
  };

  return (
    <div>
      <div className="main__menu--desk">
        <div className="main__menu--wrapper">
          <div className="left__logo">
            <Link href="/">
              <a>
                <img src="/images/Upay-Logo.jpg" alt="" />
              </a>
            </Link>
          </div>

          {!search ? (
            <div className="right__content__wrap">
              <ul className="list-inline content__list">
                <li className="list-inline-item">
                  <Link href="/">
                    <a className=" search-btn submenu__link">
                      {locale !== Locales.English
                        ? Buttons.home.en
                        : Buttons.home.bn}
                    </a>
                  </Link>
                </li>

                <li className="list-inline-item">
                  <Link href="/who-we-are">
                    <a className=" search-btn submenu__link">
                      {locale !== Locales.English
                        ? Buttons.who.en
                        : Buttons.who.bn}
                    </a>
                  </Link>
                </li>

                <li className="list-inline-item nav-item dropdown">
                  <a
                    className=" search-btn submenu__link nav-link  dropdown-toggle"
                    data-toggle="dropdown"
                  >
                    {locale !== Locales.English
                      ? Buttons.products.en
                      : Buttons.products.bn}
                  </a>
                  <ul className="dropdown-menu desk__dropdown--menu">
                    <div className="row">
                      <div className="col-xl-4 col-md-4">
                        <h5 className="desk__dropdown--menu--head">
                          {locale !== Locales.English
                            ? Buttons.cam.en
                            : Buttons.cam.bn}
                        </h5>
                        {dropDown?.Campaigns.map((item, idx) => {
                          return (
                            <li key={idx}>
                              <Link href={item?.["slug url"]}>
                                <a className="dropdown-item dropdown-item-subitem">
                                  {locale !== Locales.English
                                    ? item?.title
                                    : item?.["title bn"]}
                                </a>
                              </Link>
                            </li>
                          );
                        })}
                      </div>
                      <div className="col-xl-4 col-md-4">
                        <h5 className="desk__dropdown--menu--head">
                          {locale !== Locales.English
                            ? Buttons.serv.en
                            : Buttons.serv.bn}
                        </h5>
                        {dropDown?.Services.map((item, idx) => {
                          return (
                            <li key={idx}>
                              <Link href={item?.["slug url"]}>
                                <a className="dropdown-item dropdown-item-subitem">
                                  {locale !== Locales.English
                                    ? item?.title
                                    : item?.["title bn"]}
                                </a>
                              </Link>
                            </li>
                          );
                        })}
                      </div>
                      <div className="col-xl-4 col-md-4">
                        <h5 className="desk__dropdown--menu--head">
                          {locale !== Locales.English
                            ? Buttons.pay.en
                            : Buttons.pay.bn}
                        </h5>
                        {dropDown?.Payments.map((item, idx) => {
                          return (
                            <li key={idx}>
                              <Link href={item?.["slug url"]}>
                                <a className="dropdown-item dropdown-item-subitem">
                                  {locale !== Locales.English
                                    ? item?.title
                                    : item?.["title bn"]}
                                </a>
                              </Link>
                            </li>
                          );
                        })}
                      </div>
                    </div>
                  </ul>
                </li>

                <li className="list-inline-item">
                  <Link href="/partner-with-us">
                    <a className=" search-btn submenu__link">
                      {locale !== Locales.English
                        ? Buttons.partner.en
                        : Buttons.partner.bn}
                    </a>
                  </Link>
                </li>

                <li className="list-inline-item">
                  <Link href="/service-location">
                    <a className=" search-btn submenu__link">
                      {locale !== Locales.English
                        ? Buttons.service.en
                        : Buttons.service.bn}
                    </a>
                  </Link>
                </li>

                <li className="list-inline-item">
                  <Link href="/Media">
                    <a className=" search-btn submenu__link">
                      {locale !== Locales.English
                        ? Buttons.media.en
                        : Buttons.media.bn}
                    </a>
                  </Link>
                </li>

                <li className="list-inline-item">
                  <a
                    className="list-inline-link search-btn"
                    onClick={onClickSearch}
                  >
                    <svg
                      width="28"
                      height="28"
                      viewBox="0 0 35 35"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      id="search-btn"
                    >
                      <path
                        d="M22.6042 20.4167H21.4521L21.0438 20.0229C22.4729 18.3604 23.3333 16.2021 23.3333 13.8542C23.3333 8.61875 19.0896 4.375 13.8542 4.375C8.61875 4.375 4.375 8.61875 4.375 13.8542C4.375 19.0896 8.61875 23.3333 13.8542 23.3333C16.2021 23.3333 18.3604 22.4729 20.0229 21.0438L20.4167 21.4521V22.6042L27.7083 29.8813L29.8813 27.7083L22.6042 20.4167ZM13.8542 20.4167C10.2229 20.4167 7.29167 17.4854 7.29167 13.8542C7.29167 10.2229 10.2229 7.29167 13.8542 7.29167C17.4854 7.29167 20.4167 10.2229 20.4167 13.8542C20.4167 17.4854 17.4854 20.4167 13.8542 20.4167Z"
                        fill="white"
                      />
                    </svg>
                  </a>
                </li>

                {/* Locale Button Here */}

                {/* <li className="list-inline-item">
                  <a
                    onClick={handleLocale}
                    className=" search-btn lang__btn"
                    style={{ cursor: "pointer" }}
                  >
                    {Locales[locale]}
                  </a>
                </li> */}

                {/* <li className="list-inline-item">
                  <a href="#!" className="search-btn">
                    <svg
                      width={30}
                      height={30}
                      viewBox="0 0 35 35"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      xmlnsXlink="http://www.w3.org/1999/xlink"
                      id="search-btn"
                    >
                      <rect width={35} height={35} fill="url(#pattern0)" />
                      <defs>
                        <pattern
                          id="pattern0"
                          patternContentUnits="objectBoundingBox"
                          width={1}
                          height={1}
                        >
                          <use
                            xlinkHref="#image0"
                            transform="scale(0.00166667)"
                          />
                        </pattern>
                        <image
                          id="image0"
                          width={600}
                          height={600}
                          xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAlgAAAJYCAYAAAC+ZpjcAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQ1IDc5LjE2MzQ5OSwgMjAxOC8wOC8xMy0xNjo0MDoyMiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTkgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkM1N0IwMzg4NEU4MDExRUI5N0UxQzM2QkE2M0Q4M0VCIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkM1N0IwMzg5NEU4MDExRUI5N0UxQzM2QkE2M0Q4M0VCIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6QzU3QjAzODY0RTgwMTFFQjk3RTFDMzZCQTYzRDgzRUIiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6QzU3QjAzODc0RTgwMTFFQjk3RTFDMzZCQTYzRDgzRUIiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz61IzxmAAAwtklEQVR42uzdB7weVZ038BNDEQUhFEVpGiyI2AjYwRWDlUUsYFekJOqW17ICq+772apk1dVdX3WJLYqoEAuiIEJU7IpERVwVhKiAsIgQqki97//sTDQLN+Te55n+fL+fz19Awsw8Z86d53fPnDkzZ2pqKgEAUJ27aAIAAAELAEDAAgAQsAAAELAAAAQsAAABCwAAAQsAQMACABCwAAAQsAAABCwAAAELAEDAAgBAwAIAELAAAAQsAAAELAAAAQsAQMACAEDAAgAQsAAABCwAAAQsAAABCwBAwAIAELAAABCwAAAELAAAAQsAAAELAEDAAgAQsAAAELAAAAQsAAABCwAAAQsAQMACABCwAAAELAAABCwAAAELAEDAAgBAwAIAELAAAAQsAAAELAAAAQsAQMACAEDAAgAQsAAABCwAAAELAAABCwBAwAIAELAAABCwAAAELAAAAQsAAAELAEDAAgAQsAAABCwAAAQsAAABCwBAwAIAQMACABCwAAAELAAABCwAAAELAEDAAgBAwAIAELAAAAQsAAABCwAAAQsAQMACABCwAACYrQ00QW/Midou6r5R94vatqxtoraK2jJq86iNo+aV53YzzQYAjX9fC1gddI+o3aIeGvXwqJ3LULVTGZ4AgI4TsNqVR5geHfW4qAVlqLqfZgEAAYuZy7f0nhT12KjHp2KEaq5mAQABi5nbKGqvqKdHPSUVt/7maBYAELCYnTyHar+oA8pQtbkmAQABi9m7W9Szol4UtW8yGR0ABCxGkm/17R11WCpGqzbVJACAgDWaPFH9kLJ21hwAgIA1uryUwmtSMVq1oeYAAASs0dylDFSvLwMWAICANaJNog6Oel3U/TUHACBgjResXhl1RCrmWgEACFgjyguCLop6k2AFAAhY48lLLbwg6l+SdwACAALW2P4s6l+j9tQNAIAq3WUCP/MOUR+P+qpwBQAIWOPJo3VviPpp1AudegCgztAxCXaPen/5VwCAWg19BCsvu5DnWX1PuAIAmjLkEay9opZFzR/Y57o56qKoX5V1adQVt6sboq4t//x15X8DAPxv9yi/SwWsGX6mf4g6Mmpujz/H76POifphKuaN5To/6uKoW/1MAMDYbqszjAxJfq3NcVGP6uGx5/D0jajvlPUzQQoA+mlIAevgqHdHbdqT470y6vSoU6NOi7pEdwQAAasr7hr1nqhDenCs+TbfZ6JOiTozGaECAAGrg3aK+nTUgg4f43lRy6OOT8WcKgBAwOqsp6ZivtVWHTy2a6I+GbU0aqVuBgACVh8cEfWW1L2nBL+bigVNT0jF8ggAgIDVi+PN860WdeiY8jyqPK/q7amYVwUACFi9sVkqRoae1pHjuT7qQ1HvilqlKwEAfQtY20WdHPXwDhxLXiX9fVFHR12uCwEAfQxYu6RinagdWj6Om6KOKYOVNasAgN4GrD2ivhi1dYvHMJWKJwKPirpQlwEA+hywnpCK24L3aPEY8nsAXxP1dV0FAJipu3T0uJ6YilfItBWurog6LBUjaMIVADArXRzB2jsVr5K5W0v7/0QqRq1+q3sAAEMIWI+OOqmlcJXnV706FbclAQBG1qVbhA9NxcjV5i3s+8NRuwlXAEAVujKC9aCoL0dt2fB+V0ctTsXLmAEABhOwto9aEbVNw/s9I+plURfpBgBAldq+RZhvB55Shqym5HWt8ouiFwpXAEAd2hzBmpuKBTwf2uA+r4l6edSJTj0AMMSA9e7U7Iubfxr1nKhznXYAoE5t3SI8IupVDe7v9KjHCVcAwFAD1lNSMQeqKR+MembU1U43ADDEgHXfqI+nYv5V3fJk9jel4pU3NzvVAEBTmpyDtUnUp6O2amBft0YdnooFRAEABhuw3hO1ewP7uSkV61sd7/QCAEMOWPk23Ssa2M8fog6K+rxTCwC0Zc7U1FTd+9glamWq/wXON0btH3Wa0woAzEBe8PyqqrNV/p+6J7lvnIrFROsOV/m24IHCFQDQBXUHrH+KenjN+8gT2vOcK7cFAYBOqPMW4d5RX605xOWDPzR5WhAAmL3e3SLMSzJ8KNU/QvZG4QoA6Jq6AtA/RO1c87G/P+popxAA6Jo6bhHmta6+l+pdAuIrUU9PxeR2AIBR9OYWYQ5VH6g5XP006jnCFQDQVVUHrNdFPbLG4726DFde3AwAdFaVtwi3i/p51KY1HWs+0OdFfcZpAwAq0ItbhG+vMVxlbxWuAIA+qGoEK6959bUajzNPan9KKhYVBQCoQqdHsPKG3l7jh/9d1EuFKwCgL6oIWC+K2rPGY8wrtV/iVAEAkxKwNoz6xxqP74NRJzlNAMAkBayDo+bXdGwXRb3WKQIAJilgbRz15hqP7VVR1zpFAMAkBazDonas6bg+EXWy0wMA9NGoyzRsEnV+1H1qOKYronaN+q3TAwDUqHPLNBxaU7jKjhKuAIA+G2UEK7/I+YJUz+3BH6RiyYfbnBoAoGadGsF6fk3hKie91whXAEDfjRKwXl/TsSyP+oZTAgBMWsDaK+qRNRzHjVFHOB0AwCQGrL+o6TjeH/VrpwMAGILZTHLfJuriqI0qPoYbou6fvG8QAGhWJya5v6yGcJW9V7gCAIZkpiNYOY39NGqXivd/XdTOybpXAEDzWh/BekIN4Sr7sHAFAAzNTAPW4TXs+5ao/3AKAIBJDFj5vYMH1LDvz6TifYYAABMXsJ4etVkN+36n5gcAJjVgPb+G/Z4Z9V3NDwBMYsC6e9Qza9jvUk0PAExqwHpmGbKqdG3U8ZoeAJjUgPW8Gvb5iVSsfwUAMHEBa27UvjXs8wOaHQCY1ID12KgtKt5fXpbh+5q9sw6MOrKso6OmZlBXrvXf5JqvGaFz8s/mBbf72T2h/JnvmgXTXH/WXGfmOZX0xZ29Kuefo95U8f7yNv9Os3fqopvKi1nVlketTMUDDatb+nyLygvy0T04F6vKtlpfe80vvxQXDSDMLin7yPKa9zPJbbagDFJ39rlXpHruVox6TVrfz+u+5TG7PlCF2l6Vc2cBK/8Q717xTh8WdY7z2ap8UVnY8G+uK8ovhKaeHs1fKseUf+2jdX2BLCo/19Dkz3pQTV8ck9xmOVSdlWY26rO83F7Xw9Uae5TfUa4P9C5g3Svq0jV/qCI/i9rVuWz14tX2EPvq8jfwJTWPVpyV+n0rYXX5BbJqrf/vwHIkYqjqGEWZ9DY7pgyYM7U4tbeETv55vWAWP7f5Z2Nn1we6HLDWNQdrn4rDVfYp57G1YDVV/mbY9kVlzXD8Bam+EbQhzNOYN037HDnwfrqwrKr7wqS22bwRfsbanI+1aJY/t/NH7C9DvT7QQesKWHvXsK8vau7GL74XpG7OL5hfjiycUPHFbt4sf2PvsgW3+1wLJqTPVtkXJrnNFozws7WwxfCxoIH+MtTrAz0LWE+seD/5CZAzNXdj8q2B01P3J/Tm38LOqvA4Fw70fE7Kk1PztFlln3PUL+C2rhkLGjjWhQlaDlhbRu1S8X5OjbpVc9duzfyCRT085gUVbWso1p7AOylPDa3u6LYmpc3aDKbzGvhvhnp9oEcB69Gp+vlXp2nqRn4DPD31c+h4XkXHPpRRi/ylufx2/zwJF9QVFbehNuvXNWAI+2jj+kCPAtZjatjPlzV1I+Gqz7+h5YvfCWN+hqFcQPPj8rd/QmjJwPvwihrCgjZjUq4P9CRgPbLiffwq6mJNXXu4qvLikb+YjkrFI+Bz1lF7lH/mqAr3m8PVMRN6HleVbbnlOr40l1fc1l0LCnWswaTNmJTrAx20wTT/X9WLi35bM9f6G1lVT+ItX6tmYmX60y2YHMjWvGZn3Nt8eSJqnkPW5Ho8fVm4b0l5fqzkrs3cInJ9oONuP4K1TdR2Fe/jW5q5NsdU8IWRLxx7lL8Nj3PRXl5uJy9WOO7E2y6s2dXl32Tzl2xeZHHOmDXqwp5HVbDvOeV2lmuzTrYZUHHA2q2GfQhY9Tgyjb/Y3OLyC6PKycBL03ivsUhpWOvVACBgpYdUvP0bo/5LM1cuj1qNs4Domlct1HUbblUFwU3AAmAwAavqdwXmcHWLZq5cFeGq7kfY837GeYFvDpFWKwZgEAHrARVv/8eauHJ5Evg4twbzyFJTj/jm/Swe478XsAAQsKZxjiau3DgvsM0jSk0vvpgn5I66HpGJ7gD0PmBtFLW9gNVpC9Po79OazRIMVTtqxGC32ikHoO8BK4eruRVv/xeauFLj3Bpse8HFUW4VWnsGgN4HrB0q3nae3G4F9+rMGyNg5XDV9qsV8gjWbG4VLk1eBwHAAALWjhVv++LkCcIqHZhGn5PUlYUJZ3qrcGUa7itOAJiwgHXPirf9K81bqXHmXnVpJGiP9QS+/O/yk47mXwHQW2u/i/DeFW/7l5q3MwGra/LTjAum+Ux5ztVKpxqAIQWsrSve9n9r3srkMDLK7cHVqbvvLVspTAEwVGvfItyq4m1foXkrM+rolafwAKDlgLWlgNVZ80f874wQAUDLAWtzAauzRn1ljIAFAC1Yew7WZhVv+3LNWxkjWEATTtcEUI21R7A2qXjb12veSsxLo09wt9TBzL9Uplqqo9N4K/QDrg90PGBtVPG2r9W8lRh19Moq6P2QX959QtQxmgJwfRhmwLp7xdu+TfNWYtTV241e9cuisgBcHwYWsDaoeNvXaN5KGMGarIsogOvDwAJW1aY072DlYeuzUvXzDa4st33khLbrAl0LcH0Yhg00AbOQR9NOqPEHPd8OPXqtEJdfqWOxVAB65y6agFmoM1xNF7ZOn7Df2iyrAbg+CFhMmANbCjtHT1AbL9XNANcHAYtum1fx9ua39DkWpskYxVriAgq4PgyHOVjdN+rTgPMG1AY5YA11eDxfOFckc80A1wcBi0aNup5V1SNObS77UHdY3NcFDFr9WZjSJgyNW4TdN2qwqTpgrUgWLwUAAWsgxnmn4PyKj2PfZAFTABCwBmLUUFP15PA8D2rnqMVRRzktACBg9dmoE7zrevIvP82SJ1/OGaF2djoBELDogq6MYAEAAtZgjPoES15Dap7mAwABizvKtwhHmeg+rwxZAICAxTRGHcU6UNMBgIDF9JaPEbDmaz4AELC4o3EW+jxS8wGAgMUd5XA16ss+FyWjWAAgYDGtcd6mfrTmAwABizvK62GNMxfLrUIAELCYxpIx/ts8imXxUQAQsLidvCbWOLcKTxeyAEDA4o7yKNaoTxTOazFk5cn2Zzl9AAhYdFGei3XUGP/9vDLoLGowWF0QdUzy6h4ABCw6LN8mXD7mNnLgqWs0KwepPKn+ynI/4ywTscrpBkDAoimLKwgf+V2FZ5VBq4rX6uRQdUIZrI5O1YxYrai5HfNnn+pAnVW2n1E+6A7XB0aygSbotTwPa9/yB2/cH7qF6U8vhl5aBrf1LW669mt46lrMdGkafb5Z3ywo68jyvK7UxQHXBwGLdqwqf9hOr/A3m7XnZh3TcoBcMoHndM2DCDtPULgEXB8GxS3CYVhZhqwhzVVaPcJnGtLFZl6q/yEEF2dt5ty5PiBgMcOQtXIgn2WPET7L0L78FvrC8SWtXZ27lq4PCFisZVUZTJb2+DMcVX6GVSN+/iGp+wXdwoI268rnW93Af+P6gIDF2PLThQf17Msgz7XaMo0352rFwM5j3V8Iq0fsI5O8bEZf2mxlR/tclce70vUBAYs2LK8gsDTxZZVHrOaUf11dwfaWDugcrmion8y2jVf42ep8m60Y4edpRWrvl7ImApbrAwIWlTqqg0ErXxgW13Rc47xGqGvhs4kvg+Uj/PlJv7XYlzZbXvOfr9Jsl2NZOWLAcH1AwKLyH8a1R4ramAi/cq2wt2+NF4c1y1as6vn52rehL4IVswi5476iaUgjB31os6Nm8XOwvOUv7NkuybLY9cEcyq6bMzU1tebvpyre9hZRV2vizlrzmO/8VN/jvket9VtjGxaVn/PonpyTlWt90TV98TxmPf0gfyEdlCxu2Lc2ywtTnpDufEL08vI4u+DI9fy8ri6PdYXrAxXZPOqqqrOVgMV0F7c1Dkwze0fhqtv95jvq0D3tW1ie90XTXNSXaJ5et9mR0/xML1+rumRBeaxH3i5YLREuELAELABgggOWOVgAABUTsAAABCwAAAELAEDAAgBAwAIAELAAAAQsAAAELAAAAQsAQMACAEDAAgAQsAAABCwAAAELAAABCwBAwAIAELAAABCwAAAELAAAAQsAAAELAEDAAgAQsAAAELAAAAQsAAABCwBAwAIAQMACABCwAAAELAAABCwAAAELAEDAAgBAwAIAELAAAAQsAAAELAAAAQsAQMACABCwAAAQsAAABCwAAAELAIBZ2EAT0CFzoh4R9ZSoh0TtFrVd1KZRdyv/zPVR10X9Juq/yloR9YOoKU0IQCe+0Kam/vidVPWX0xZRV2tiZmDHqL+OekEZqEZxSdQJUf8R9UtNCsAMbB51VQ2DBQIWrcojU/8Y9ZdRG1a0zZuj3hf15qhrNXFl8gji46IeG7VL1AOi7h21ddRde/Q5cp+4ba2/vzXqhqhrysrXrN+V9d9lXRT166jLklFS/UA/ELAELDruz6KWRe1U0/bzxfDQqNM19cjyHM1nR70s6mlRG014e+Qv4PPK+nHUD6POKr9w9QP94DKXDAFLwKJtecTqXVFza95P/g31b6Pe5jfOWdu/bLcHaor1OjfqjDLMfykVcwT1A/3gOk0iYAlYNOnoqCMb3ud/Rr1ayJqRfJtnadRLNcVIbiy/XD8a9YXyn/UD/eBGTSJgCVjU6YioJS3t+61Rb3QK7lSeX3Nq1F6aohKXRx0T9d6oS/UD/aBn/UDAErDoiadHndLyMeTfxj/mVKzTp6Oeoxkq94fyC3ZJT75g9QP9QMASsOiJ/JRRXq9qXsvHkSen7h71c6fkDvIE5o9ohtr7X75F/rby7/UD/eAGzSFgCViMI69PdWBHjuVbUXunPz2iTUqbRK2K2lZTNOKCqMOjvqof6Acd7AcCVkUBy6tyqNtTOxSussdHLXZa/peX+FJt1M5RX4l6Z+rWkgf6gX5AlSnLCBY1ygE+rxHzsI4dV148MD92vtop+h/fjXq0ZmjF96OeF3WhfqAfdKQfTBojWPTSizsYrrK86vSRTs//uKcv1VbtGfW9qEfpB/pBB/oBFY8wQF19600dPr6/SsXk+0m3jyZoXb4td0bUk/UD/aDlfoCARQ/kV2s8qMPHl9f6ea3TlBZogk7IE8y/GLWffqAftNgPELDogTf04BjzEzybTvh52lVX7Yz8wvP8xO2T9AP9oKV+gIBFx+W1pvownyM/iDHprwLZSXftlDyC8dmoB+sH+kEL/QABi457dY+O9fAJP1f30l07Jz/VlN9ft6V+oB803A8QsOiwe0S9sEfH+8g02fNPXLy7aX7UB1P5uLd+oB9oCgELcri6W8+O+VDXADrogKhX6Qf6QYP9gApZaJSq5QXz9ujZMV+TiiUbfj9pP//JK4O67vqoh0T9Wj/QD2ruB5PKQqP0wsN6GK6yfFvzwAk8X1O6bOfdPeo9+oF+0EA/oGICFlU6zLFD5Z6Zind6oh/oBz3iFiFVuWvUJVHzevwZdok6d8LOm9GLfvhRKpY/mdIP9APnq1K13SLcQNtSkef0PFxli6Je71TW7idRe9W07Y1TsYbQ3FQ8GbdV+ddcO6Tiqaw1tUWP2uwRUQdFHa8f6AcD6weDZQSLqnw59f99ZpdHbR910wSdtzZ+Ez67/KJoW15cM79cN79o9wmpWBy3y9MmfliOXugH+sHuiarUNoIlYFGF/FvgBQP5LHmy+6cErIn4Yr29bVIxzyX3gTzXZW4Hj3HvqG/oB/pBTf1AwKowYJnkThUOGdBnWeR0Tqw8grms/HLNoxpvT8Xj8V3yaqdJP9AP+kHAYlxzBxawFqZiRI7J9ptUvLD8gVGf6NBxHVD+xo1+oB8IWAzcM1KxSOdQ5KFdSzawRn4y9kWpeENBF0Yx8tO6z3Na9AP9QMBi+IZ4Sy2PyG3k1LKWT0Y9OeraDhzLAU6HfqAfCFgM232int7Qvlak5ibi3svFi2l8LxUjGG2/Vibfxr6b06Ef6AcCFsOVR3qaesLmbalYCqIpi51epnFy1D+0fAz59tDjnAr9QD8QsBhu32lqcvtlZbj6WIOfL6/p9QCnmWm8JWply8fwZKdBP9APBCyGKQeQ+zW0rzzv4daoT0fd0OBnfKXTzDRuifrLlo/h0U6DfqAfCFgMU5OT248r/3pd1IkN7vflqXjlBtzed6M+1+L+93T91g/0AwGL4dk6NTcJ/Lyo76/1z8c2+Dnz+8s8Cs26/GuL+940NTeCjH6AgEVD8sjOhg3t6+O3++fTUjEnqyluE7Iu307FC4vb8mCnQD/QDwQshqXJhTiPu90/57lYTa6onF/+uqtTzjp8ssV9ewhDP9APBCwGZK+oXRraV15v5vxp/v9jG/7MlmxgXb7Q4r531Pz6gX4gYDEcTY5erWtZhh9E/azB48i3RC3ox3TOjlrd0r7vo/n1A/1AwGIYtog6sKF95VuBJ9zJv29yFCu/VPUgp591aGstpG01vX6gHwhYDEN+2ekmDe3r9Kjf3sm/z6NbUw1+dpPdWZdzWvyFB/1APxCwGIAm175a36rtF0V9rcHjyQv6PUIXYBoXtrTfzTW9fqAfCFj034Kohze0r+vTzBbva3qy+yLdgGn8pqX9bqjp9QP9QMCi/5oMFyelYtX29cmvzvlDg8f1kqjNdAWm+YWgDXfX9PqBfiBg0W/5B/iFDe5vpi91vroMY03ZrOF2oB9+rwnQDxCwGMXzU3MjN5enYoL7TH204bawJhZdcZsmQD8QsOi3Jte+yksz3DyLP39aGcqasnsqXrAKa9y1pf1eo+n1A/1AwKK/dot6bIP7+9gs/3wOY8c33CaWbGBt81ra782aXj/QDwQs+qvJ0atVqXg9zmw1/TThC6LuoWtQuldL+71a0+sH+oGART9tHPXSBveXX+w8yuKhZ0ad1+Bx5tfmvEz3oDS/pf1epen1A/1AwKKfnhu1ZcMBa1Qmu9OWh7S038s0vX6gHwhY9FOTtwfze7zObSmcjSLPTXucLjLx5kbt0dK+L9b8+oF+IGDRP/ePelKD+xs3IP0q6psNt5FRLB6Z2puP54tVP9APBCx66NAG95XXcflkBdtperJ7Xh9sS11loj2rxX2fq/n1A/1AwKJf8rutXtHg/r4cdWkF28lraN3U4HHnhwBMdp/s6+dLWtz/eU6BfqAfCFj0yzNTs48cVzV/Kj9Nc1LDbZVvE87RZSbSn0fdt6V958Ulf+0U6Af6gYBFvzT5Yuf8subPVri9jzXcVrtEPVGXmTg5VP99i/vPD4VMOQ36gX4gYNEfO0Q9tcH9nZSqfdXDF6OuHHAgpTu/hDyixf2f6RToB/qBgEW/HNJwv6h6eYU8B6vpV+fk9cLuqetMjPyE7dtaPoYznAb9QD8QsOhXfzikwf2tTsWIU9Wafppwo6iDdZ+JsHnUiVGbtXgM+d1z33Qq9AP9QMCiP/KtwR0b3N8JqZ4XlX4n6vyG2y7fKjDZfdg2jfpCam/F7jW+GnWd06Ef6AcCFv1xWMP7q3NCetMru+8ctVAXGqxto74W9YQOHMvnnA79QD8QsOiPvCzD/g3uLz9a/K2ehrd1eaVuNEj7Rv0oavcOHMstUcudEv1APxCw6I+XR23Q4P4+kep9vDjfIvxOw224f/kbLsNw76hlUaelZteFuzOnRF3u1OgH+oGART/kuUOHN7zPJkaYmp7sngPqYbpT7903FU+HrSp/8eiSpU6PfqAf9OSLdWrqj4MIVY8mbBF1tSbuhT9LxYTJppydmlk7Jr8nML+CZ6MGP9uFUfdLxfsV+6CNRQqbOv+z7Sv5DQYvTsWtoC7+8plfifLgmvqWfqAfTKr8ROhVVWerNb9xQ9OjLk1NQM8LjuZlIJp8EWt+CvNpqRjCp6O/WEbtFLVb1OOj9o56TOr+iP4SX6r6gX7QHwIW+Te25zW4v/yb8scb3N+xqfk33b9SwGr8OrZp+fdblH/drPz7PH9mm6j7lH+/ayoer9+0Z5/xgqiPOtX6gX4gYNEfeSh84wb3lx9x/k2D+8vr1Vy11gW3CfkWQ37l0EW617Qenrw/bbbenIonx/QD/eAWzdAPJrnT9Hv0ml6f6sZULGja9M/VoboWFfl61Cc1g36gH/SLSe6T7dFR321wf/k9gfdsoV/k+RVNv1LiklTM7+j6b5tGELot/4KQJ4L/XD/QDxroB5OotknuRrAmW9OjV19oKXR/O+pXDe8zz/XYTxdjTH/nSxX9oJ8ErMmVJ38e1PA+j2vps+bfzttY2X2RbsYYTo16h2bQD/QDAYt+eWFq9gmaPHJ1couf99gW9pmXa5ivqzGC/Cqp/ACKx/H1A/1AwKJnml77Kr8368YWP29enO/Mhvc5J1nZndnL80Genop13NAP9AMBix7Jj0fv2fA+j+vA525jFOuQqA11OWbohlSs2/YzTaEf6AcCFv3T9KhKXvfq6x343Men5p/qyy+HfbYuxwy/VPfryM8K+gECFrO0SdRLG95nXrm9C3MI8tvnT21hv4t1O9bjiqgnR31FU+gH+oGART/l1+Js3vA+j+vQ52/jNuE+UffX9ViHX6RirbbvaAr9QD8QsOivwxve339Fnd2hz39S1DUt7PdVuh7r6I97RJ2rKfQD/UDAor8eFLVXw/s8rmNt8IdUPNHYtJenZt/5SLfleTavjjqgpcCPfoCARYXaWDLg4x1shzYWHd0q6rm6IGFF1MOi3pe8okY/0A8ELHpvo1SMojTpG6lYKK9r8tM5F7awX7cJJ9uqqBdE7Rt1vubQD/QDAYth2D9qm4b3eVxH2+K2lo7tCVG76ooT5+Ko/xP14FQsFYJ+oB8IWAxI0+/Fuzm1M9dppo6dkPNAe36citvyO0f9R9RNmkQ/0A8mx5ypqT/e+q36HvAWqXj/HO3bKeqXqXh1S1PyUzHP6ni7nBW1oOF95p+J+0T9viNtYO5Hta6N+kzUB6K+2aPj1g/0g0mVly26qupslf9nA207EQ5rOFxlx/WgXT7WQsDKP8wHRS3TLQcjh+ZTok6M+nwqngxDP9APJpwRrOGbm4qJ5ts1uM/8uPG2PbjA5NfY/KZsoyblhQQf15E2MHIxe3mpj++nYrXtr5Tn8+aefyb9QD+YVEawGNkzGg5X2Wd78tvbZVGnpeKN9U16bNQjon6ke3ZevvDmxXLPScVcmvyFerYvUv1AP2B9BKzhO7SFfR7Xo/Y5toWAleXJ7q/WPRt1fSpe9n1j+QtA/nK8cq26JOrSVIxq5kfpLyj/f/QD/YBZc4tw2O4ddVFq/hYY63dteX6ub/k42rg19JPU7BsFrtLd9AP9gHVwi5CRvEK46qzNol4ctXQCP/utvuzQDxg662ANV07Qh2mGTlusCQAELPpln6j7aYZO2z01v0wEAAIWYzhcE/SCie4AAhY9sXXUszVDL+SXvt5DMwAIWHTfy6I20gy9cLeol2gGAAGL7jO5vV9epQkABCy67fFRD9YMvbJb6s6rcwAQsJiGye39tEgTAAhYdFOeLH2QZuilPNl9S80AIGDRPXmy9CaaoZc2TsXDCQAIWHTMoZqg1/JtwjmaAUDAojvyiuC7a4Zeyw8nPFEzAAhYdIfRq2Ew2R1AwKIj7p4sVjkUz43aRjMACFi078CozTTDIOQV+F+hGQAELNpn7athMdkdQMCiZbsmq4APzc5RT9YMAAIW7TF6NUzeTwggYNGSvDjlSzXDIO0fta1mABCwaN6zo7bSDIO0QbL0BkCvL+L0Vxu3B29LxbyvyyakjTeMOi9qixb2nSe7v7VscwAELBowP2qfFvb75ahzJ6ytP5HamRO1Y9TTok7R3QH6xS3C/jqspf1+ZALbelmL+16sqwMIWDQjjzy2sRjldVEnTmB7nxn1s5b2vV/UDro8gIBFM1+6bTxh9umo6ye0zZe1+DNqsjuAgEUD2vrC/cgEt/mxqb3J5vl2sPmSAAIWNcq3i57Rwn4vjPraBLf7pVFfamnf26Vi1BKAas0VsFjj4JbO23HJcgHLWtz3Il0foHKbVry92wSsfsrny9OD7flc1FUt7Tsv13BfpwCg8u/VKl0rYPXTwlSsjdS0/BTduZo/3ZiKNbHaMCeZ7A5Qtc0q3t5NAlY/tTV69VFN/0fLWtz3wanG+QIAE+juFW/vBgGrf7aOelYL+7056pOa/4/aXBNr+1TcKgSgGttUvL1rBKz+eWnURi3s9+SoKzT//7KsxX27TQhQna0q3t7VAlb/HNLSfk1uv6M218T686h7OgUAnQxYqwWsfnlU1G4t7PfK5EXD08lrYp3a0r7zgqPPcQoAKrF1Dd+bAlaPvKCl/ea5Vzdp/ml9uMV9H6j5ASpR9Wvnfidg9cddWgxYbg+u2+fX/k2lYU+M2tIpABjbfSve3iUCVn/sFXXvFvZ7XiqemGN6ba6JlZdqeLZTANC5gPVbAas/2rodZPRq/Za1uO9naH6AseQ5rdtXvM0LBaz+eGoL+5xKxZNy3Lmzor7f0r6f1OOf31t1HfQDOmD7MmRV6WIBqx/yAmj3b2G/X4q6SPPPyHta2u+8qAf3tM2u1W3QD+iAB1a8vdvW/u4UsLrtES3t94Oafsbyk5a/a2nfu/e0zW7TbdAP6ICqlz/K4cq7CHti5xb2mVdtP0nTz1ie7P6BHv/2dVULx325btM5+gGT6KEVb+/8tf9BwOq2bVvY57HJ2lez9b7UznyS+1Swjf9u4bgv02U6Rz9gElV9l+g8Aas/5rSwz6WafdbyUyOfb2G/VUzO/HULx71Kl+kc/YBJk6+fVc9j/amA1R+3NLy/06J+ptlH8u4W9vmHqi8IDfmx7tI5+gGT5iFRGwtYfqtsyr9r8pF9tYUvqV9UsI2mF5PNt1LP0l06Rz9g0jy+hm3+RMDqjybXWMr3jk/V5CPLa4e9s+F9fruCbXy5PPamfC/qGt2lc/QDBKzx5Ffk/FbA6o98u+6ChvaVw4HHpsfz0dTcZOFLyi+pceUnub7RYBudoJt0kn6AgDWeH9z+/xCwuq+JSed57Y4Paeqx5acvlzS0r2NSdU8uLmvomPOcseN0k87SD5gUeQX3nQQs3ht1ac37eEuyNEOVwafuVfDz4+3/VuH2jkvNrNyf2+Z3ukhn6QdMiifXsM3vClj9c13Uq2rcfp5Ib/SqOjdEvaHmfbyy7BdVyeH6yJqPOc9N+Efdo9P0AyZF1e/4zfMXvydg9dPnot5W07Zfl4xeVe34qC/UtO23R51Yw3Y/EfWZmo45X3wOjbpS1+g8/YChm1tDwPr5dP1awOqP/Jtl1fMWTqzxYjrp8hdJ1ctsfLzmEYaDo86pYbtH1Bg40Q9gNh4VtWXF2/z6dP+ngNUf+be/l0W9q6Lt5acTD9Ostcm3Qp4W9ZuKtre0PP91Pul5bSrmJlS1JlI+1r9Jxagb/aEfMGRPr2GbAtYA5AvVa6NemMZ7OWteDG2fVLzYmfrkYeP8KPA4yynkL7vDoxanZt53mB/X3zvqPWm8dZHygxn7Rb1DN+gl/YChel7F28s/H18RsIbjk1EPSsXrWWbzupTro/4las9UvD+P+uXbhE+Iek35pTVTN6biaat8nj/Q8DHnff9l1GOivjRCIMxLVewa9UWnv9f0A4bmoan69w/+MK1j/cM5U1NTa6ewKm0RdbXzWbvczs+JekrUglSs7bHhWoHql2UHOD3qJOekVfm9Vwek4tbh7lHzozZd68vsV1E/Ks9Vnhu3uiPHfb+yj+1dfmHuGLVR+e/y04yryj6Wv4TzAxm/d6oHST+g7/4p6s0VbzMvc/QmAQsAmFT5lXAPqHibe0V9c7p/4RYhADB0e9YQrvJc6O+s618KWADA0B1awzbzdI5bBSwAYBLlua4vqmG7n7qzfylgAQBD9vyozSreZn6I42QBCwCYVItq2GYOV9cLWADAJMrruD2qhu0ev74/IGABAEP12hq2mRfSPUXAAgAm0f2jnlvDdvNCujcIWADAJPrrqLk1bHfpTP6QldwBgKG5Z9QF6U+vI6vKual4n+F6M5MRLABgaI6oIVxlH0gzHJAyggUADMl9os6P2qTi7d4UtX3U5TP5w0awAIAheWMN4Sr73EzDlYAFAAzJTlGH1bTt987mDwtYAMBQ/GvUxjVs94dRZwhYAMCk2SvqwJq2/c7Z/gcmuQMAfZcHjM6MWlDDti+M2jnqltkeEABAnx1SU7jK3jHbcJUZwQIA+mybqJ9GbV3Dti9NxejVDbP9D41gAQB99q6awlX21lHCVWYECwDoq2dGfaGmbV8U9YCoG0f5j41gAQB9tFnU+2rc/j+PGq4ELACgr/4taoeatv3LqA+PswEBCwDom/1TfSu2Z/836uZxNmAOFgDQJ/eOOjsVTw/WYWXUnuPmIiNYAEBfzI36WI3hKnt9qmDQScACAPri76P2qXH7y6O+VsWG3CIEAPrggKjP5OxS0/avj9o1Fa/GGZsRLACg6x4YtazGcJX9U1XhKjOCBQB02eZR307F6FJdfhK1exrzycG1GcECALpqw1TcFqwzXN0W9coqw5WABQB02b+neie1Z++O+lbVG3WLEADooqNS8bLlOq2KelgqJrhXyggWANA1B0e9peZ95FuDh9YRrgQsAKBrDor6QKr3icHsnVFn1LVxtwgBgK7YL+rTURvVvJ9zUvE6nBvr2oERLACgC/ZNxUrqdYer30c9v85wJWABAF3w51EnRd21gX29Nupnde9EwAIA2pRHkz7VULhaFrW0iQ8lYAEAbTk46rhU/23B7IdRf9HUBxOwAIA2/G3Uh6LmNrCvK6Kem4r5V43YwPkFABqUs8f7og5raH+3Rr046pdNf0gAgCZslor5Vk9pcJ9vivpSGykSAKBuD0zFi5sf0uA+80jZkjY+rDlYAEDd9o/6fsPh6itRf9XWBxawAIC65Ans/xx1YtQ9GtxvXqn9OamYf9UKtwgBgDpsn4p1p57c8H4vjnpGavl1fUawAICqPS/q7BbC1eWpeOXOxW03gIAFAFQlPyWY17bK7xTcsuF9XxW1MOrnXWgItwgBgCrk23L5qb0dW9j3NVFPi/pxVxrDCBYAMI57puJ1Nye3FK7y6uz5KcXvdalRBCwAYNQMcUjUT6Ne1NIxXJuKkbOvda1x3CIEAGbrcVH/L+qRLR7D6lTcFjyzq+kTAGAm8i3Aj0d9s+VwdVnUk7oarjIjWADA+mwb9bdRi6M2bvlY8hIMeSmGn3e5wQQsAGBdtok6IuovojbpwPHkpwSfmTqwzpWABQDM1vyo16RiEvvdO3JMX4o6MBUT2ztPwAIA1nhU1N+k4j1+czt0XO+PenXULX1pSAELACbbpqkYGVoU9ZiOHdttUW+MWtK3RhWwAGAyLYg6POoFUZt38PiuiHpJ1Kl9bFwBCwAmx0Oinl/WAzt8nD+Mem7UL/va0AIWAAxXnkeV51Xl1c7zvKpde3DMH4l6ZdQf+tzwAhYADMu9o/aJ2i/qKVFb9uS4r0vFk4sfHMJJELAAoL/yCNWDUzE5/bFRe0U9oIef4/tRL476xVBOjIAFAP0IUttF3T8Vt/nyXKr8qpqHRt2tx5/r1lQ8Ifj3UTcP6YTVGbC2SN51CADT2TAVyyOkMiDlxTy3Kmvr8q/5Vt99y9qh/G+GZFXUwVHfGOIJnjM1NbXm76f0dwCgZnmx0HdF/d+oG4b6Id0iBACa8qOow6JWDv2DuoUHANTt91FHRe05CeEqM4IFANTphKg3RF04SR9awAIA6pBXY39d1BmT+OHdIgQAqvTrVKxptcekhqvMCBYAUIXfRv1L1H9G3TTpjSFgAQDjuCzqbWWwul5zCFgAwOjOj3pn1LJUPCWIgAUAjOjbUe+IOjHqNs0hYAEAo8nvCfxs1L+XAQsBCwAY0blRH4z6SComsSNgAQAjyBPV8+2/pal4EbN3FQtYAMAI8iT1U6KOjzo5DfglzAIWAFCna6O+FPXpqM8nSywIWADASH4SdXrUF6O+liwIKmABALNyaxmo8jyq/OTfGVGXahYBCwCYuV9FnZOKlyx/M+q7qbgNiIAFANyJvLDnRalYRf0XUWenYpQqB6urNY+ABQAUI0y3RK2OurEMSfnvr4i6PBW39C4rQ9XFUReWf44eBaw5mgMAYHx30QQAAAIWAICABQAgYAEAIGABAAhYAAACFgAAAhYAgIAFACBgAQAgYAEACFgAAAIWAICABQCAgAUAIGABAAhYAAAIWAAAAhYAgIAFAICABQAgYAEACFgAAAhYAAACFgCAgAUAIGABACBgAQAIWAAAAhYAAAIWAICABQAgYAEAIGABAAhYAAACFgAAAhYAgIAFACBgAQAIWAAACFgAAAIWAICABQCAgAUAIGABAAhYAAAIWAAAAhYAgIAFAICABQAgYAEACFgAAAIWAAACFgCAgAUAIGABACBgAQAIWAAAAhYAAAIWAICABQAgYAEACFgAAAhYAAACFgCAgAUAgIAFACBgAQAIWAAACFgAAAIWAICABQCAgAUAIGABAAhYAAACFgAAAhYAgIAFACBgAQAwW/9fgAEAEI+XiLMR+3YAAAAASUVORK5CYII="
                        />
                      </defs>
                    </svg>
                  </a>
                </li> */}
              </ul>
            </div>
          ) : (
            <div className="menu__search">
              <div className="search__barMain">
                <form className="search__mainForm">
                  <div className="form-group curstom__form-group">
                    <input
                      type="text"
                      className="form-control curstom__form-control"
                      placeholder="Enter Keyword to Search"
                      name="search"
                      autoComplete="on"
                      value={inputText}
                      onChange={(e) => setInputText(e.target.value)}
                      onClick={onClickAdvanceSearch}
                    />
                  </div>
                  <button type="submit" className="btn search__go--btn">
                    <svg
                      width={26}
                      height={26}
                      viewBox="0 0 26 26"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M18.6042 16.4167H17.4521L17.0438 16.0229C18.4729 14.3604 19.3333 12.2021 19.3333 9.85417C19.3333 4.61875 15.0896 0.375 9.85417 0.375C4.61875 0.375 0.375 4.61875 0.375 9.85417C0.375 15.0896 4.61875 19.3333 9.85417 19.3333C12.2021 19.3333 14.3604 18.4729 16.0229 17.0438L16.4167 17.4521V18.6042L23.7083 25.8813L25.8813 23.7083L18.6042 16.4167ZM9.85417 16.4167C6.22292 16.4167 3.29167 13.4854 3.29167 9.85417C3.29167 6.22292 6.22292 3.29167 9.85417 3.29167C13.4854 3.29167 16.4167 6.22292 16.4167 9.85417C16.4167 13.4854 13.4854 16.4167 9.85417 16.4167Z"
                        fill="#F6F6F6"
                      />
                    </svg>
                  </button>
                </form>
                <div className="options__mobile--sec">
                  <a className="btn options__btn">
                    Options{" "}
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width={19}
                      height={18}
                      viewBox="0 0 19 18"
                      fill="none"
                    >
                      <path
                        d="M19 0.987305H0V17.6123H19V0.987305ZM1.1875 2.1748H17.8125V4.5498H1.1875V2.1748ZM17.8125 16.4248H1.1875V5.7373H17.8125V16.4248ZM5.9375 10.4873V6.9248H2.375V10.4873H5.9375ZM4.75 8.1123V9.2998H3.5625V8.1123H4.75ZM5.9375 15.2373V11.6748H2.375V15.2373H5.9375ZM4.75 12.8623V14.0498H3.5625V12.8623H4.75ZM15.4375 9.2998V8.1123H8.3125V9.2998H15.4375ZM15.4375 14.0498V12.8623H8.3125V14.0498H15.4375Z"
                        fill="black"
                      />
                    </svg>
                  </a>
                </div>
              </div>
            </div>
          )}
        </div>

        {advanceSearch && (
          <div className="desk__menu--advancesearch">
            <div className="search_result">
              {suggestiveSearch.loading && (
                <div className="container">Loading...</div>
              )}
              {suggestiveSearch?.result?.data?.map((item, index) => {
                return (
                  <div
                    className="search_bottom"
                    key={index}
                    onClick={onSearchClick}
                  >
                    <Link href={item.url}>
                      <p className="pointer">
                        {locale !== Locales.English
                          ? item?.["title_english"]
                          : item?.["title_bangla"]}
                      </p>
                    </Link>
                  </div>
                );
              })}
            </div>
            {!inputText && (
              <div className="container">
                <h1>
                  {locale !== Locales.English
                    ? Buttons.popular.en
                    : Buttons.popular.bn}
                </h1>
                <div className="row">
                  {dropDown?.Services.slice(0, 5).map((item, idx) => {
                    return (
                      <div className="col-xl-4" key={idx}>
                        <a className="advance__options" onClick={onSearchClick}>
                          <div className="media advance__options-media">
                            <div className="media-body">
                              <Link href={item?.["slug url"]}>
                                <h5 className="mt-0 pointer text-capitalize">
                                  {locale !== Locales.English
                                    ? item.title
                                    : item["title bn"]}
                                </h5>
                              </Link>
                            </div>
                          </div>
                        </a>
                      </div>
                    );
                  })}

                  <div className="col-xl-4">
                    <a className="advance__options" onClick={onSearchClick}>
                      <div className="media advance__options-media">
                        <div className="media-body">
                          <Link href="/limits-and-charges">
                            <h5 className="mt-0 pointer text-capitalize">
                              {locale !== Locales.English
                                ? Buttons.limits.en
                                : Buttons.limits.bn}
                            </h5>
                          </Link>
                        </div>
                      </div>
                    </a>
                  </div>
                </div>
                {/* <div className="right__arrow--btnPart text-center">
                <a
                  href="#!"
                  className="btn right__arrow--btn primary__bg--btn inline__blck--btn"
                >
                  View All Results
                </a>
              </div> */}
              </div>
            )}
          </div>
        )}
      </div>

      <div className="main__menu__mob">
        <div className="hamburger__menuIcon">
          <a onClick={handleShow}>
            <svg
              width="30"
              height="30"
              viewBox="0 0 30 30"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <g clipPath="url(#clip0)">
                <path
                  d="M0 5.3999V6.5999H30V5.3999H0ZM0 14.3999V15.5999H30V14.3999H0ZM0 23.3999V24.5999H30V23.3999H0Z"
                  fill="#4E4E50"
                />
              </g>
              <defs>
                <clipPath id="clip0">
                  <rect width="30" height="30" fill="white" />
                </clipPath>
              </defs>
            </svg>
          </a>
        </div>

        <div className="main__logo-part">
          <Link href="/">
            <a>
              <img src="/images/Upay-Logo.jpg" alt="" />
            </a>
          </Link>
        </div>

        <div className="right__sect">
          <a
            className="get__appBtn"
            href="https://cutt.ly/NzXvJDf"
            target="_blank"
          >
            <img src="/images/get__appIcon.svg" alt="" className="getAppSize" />
          </a>
        </div>
      </div>

      {/* <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Modal heading</Modal.Title>
        </Modal.Header>
        <Modal.Body>Woohoo, you're reading this text in a modal!</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleClose}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal> */}

      {/* <Modal show={show} onHide={handleClose}>
        <div className="mobile__sideBar__menu--main">
          <div className="close__btn--part">
            <a href="#!" onClick={handleClose}>
              <svg
                width="16"
                height="16"
                viewBox="0 0 16 16"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M2.75 2.04297L2.04297 2.75L2.39844 3.10156L7.29297 8L2.04297 13.25L2.75 13.957L8 8.70703L12.8945 13.6055L13.25 13.957L13.957 13.25L13.6055 12.8945L8.70703 8L13.957 2.75L13.25 2.04297L8 7.29297L3.10156 2.39844L2.75 2.04297Z"
                  fill="black"
                />
              </svg>
            </a>
          </div>

          <div className="side__menuList--wrapper">
            <div className="css__collapse">
              <div className="tab">
                <label className="tab__label" htmlFor="tab-1">
                  <span>{locale !== Locales.English ? Buttons.home.en : Buttons.home.bn}</span>
                </label>
              </div>
              <div className="tab">
                <label className="tab__label" htmlFor="tab-2">
                  <span>Who We Are</span>
                </label>
              </div>

              <div className="tab">
                <input className="tab__input" id="tab-3" type="checkbox" />
                <label className="tab__label" htmlFor="tab-3">
                  <span>{locale !== Locales.English ? Buttons.products.en : Buttons.products.bn}</span>
                </label>
                <div className="tab__content">
                  <div className="content__wrap">
                    <div className="multi__subMenu">
                      <ul>
                        <p>{locale !== Locales.English ? Buttons.cam.en : Buttons.cam.bn}</p>
                        <li>
                          <a href="#!">{locale !== Locales.English ? Buttons.latest.en : Buttons.latest.bn}</a>
                          <a href="#!">{locale !== Locales.English ? Buttons.limits.en : Buttons.limits.bn}</a>
                        </li>
                      </ul>

                      <ul>
                        <p>Services</p>
                        <li>
                          <a href="#!">Send Money</a>
                          <a href="#!">Mobile Topup</a>
                          <a href="#!">Cash In</a>
                          <a href="#!">Cash Out</a>
                          <a href="#!">Make Payment</a>
                          <a href="#!">Pay Bill</a>
                          <a href="#!">Add Money</a>
                          <a href="#!">Request Money</a>
                          <a href="#!">Request Money</a>
                          <a href="#!">Remittance</a>
                        </li>
                      </ul>

                      <ul>
                        <p>Payments</p>
                        <li>
                          <a href="#!">Traffic</a>
                          <a href="#!">IVAC</a>
                          <a href="#!">Ticket</a>
                          <a href="#!">Donation</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>

              <div className="tab">
                <input className="tab__input" id="tab-4" type="checkbox" />
                <label className="tab__label" htmlFor="tab-4">
                  <span>{locale !== Locales.English ? Buttons.partner.en : Buttons.partner.bn}</span>
                </label>
                <div className="tab__content">
                  <div className="content__wrap">
                    <div className="single__subMenu">
                      <ul>
                        <li>
                          <a href="#!">Merchant</a>
                        </li>

                        <li>
                          <a href="#!">Agent</a>
                        </li>

                        <li>
                          <a href="#!">Corporates</a>
                        </li>

                        <li>
                          <a href="#!">SMEs</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>

              <div className="tab">
                <label className="tab__label" htmlFor="tab-5">
                  <span>{locale !== Locales.English ? Buttons.service.en : Buttons.service.bn}</span>
                </label>
              </div>

              <div className="tab">
                <label className="tab__label" htmlFor="tab-6">
                  <span>Media</span>
                </label>
              </div>
            </div>
          </div>
          <div className="store__connect--part">
            <a href="#!">
              <img src="/images/Google-Play-btn-mob.jpg" alt="" />
            </a>
            <a href="#!">
              <img src="/images/Apple-App-btn-mob.jpg" alt="" />
            </a>
          </div>
        </div>
      </Modal> */}
      <div className={show ? "mob__menu-drwr-true" : "mob__menu-drwr-false"}>
        <div className="mobile__sideBar__menu--main">
          <div className="close__btn--part">
            <a
              className="search__br-btn"
              onClick={onClickMadvanceSearch}
              style={{
                cursor: "pointer",
                textDecoration: "none",
                listStyle: "none",
                color: "black",
                fontWeight: "bold",
                position: "relative",
                right: "10px",
                top: "2px",
              }}
            >
              <svg
                width="28"
                height="28"
                viewBox="0 0 35 35"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M22.6042 20.4167H21.4521L21.0438 20.0229C22.4729 18.3604 23.3333 16.2021 23.3333 13.8542C23.3333 8.61875 19.0896 4.375 13.8542 4.375C8.61875 4.375 4.375 8.61875 4.375 13.8542C4.375 19.0896 8.61875 23.3333 13.8542 23.3333C16.2021 23.3333 18.3604 22.4729 20.0229 21.0438L20.4167 21.4521V22.6042L27.7083 29.8813L29.8813 27.7083L22.6042 20.4167ZM13.8542 20.4167C10.2229 20.4167 7.29167 17.4854 7.29167 13.8542C7.29167 10.2229 10.2229 7.29167 13.8542 7.29167C17.4854 7.29167 20.4167 10.2229 20.4167 13.8542C20.4167 17.4854 17.4854 20.4167 13.8542 20.4167Z"
                  fill="black"
                ></path>
              </svg>
            </a>

            {/* Local Button Here */}

            {/* <a
              onClick={handleLocale}
              className="lang__btn"
              style={{
                cursor: "pointer",
                textDecoration: "none",
                listStyle: "none",
                color: "black",
                fontWeight: "bold",
                position: "relative",
                right: "10px",
                top: "2px",
              }}
            >
              {Locales[locale]}
            </a> */}
            <a onClick={handleClose}>
              <svg
                width="16"
                height="16"
                viewBox="0 0 16 16"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M2.75 2.04297L2.04297 2.75L2.39844 3.10156L7.29297 8L2.04297 13.25L2.75 13.957L8 8.70703L12.8945 13.6055L13.25 13.957L13.957 13.25L13.6055 12.8945L8.70703 8L13.957 2.75L13.25 2.04297L8 7.29297L3.10156 2.39844L2.75 2.04297Z"
                  fill="black"
                />
              </svg>
            </a>
          </div>

          <div className="side__menuList--wrapper">
            <div className="css__collapse">
              <div className="tab">
                <label
                  className="tab__label"
                  htmlFor="tab-1"
                  onClick={handleClose}
                >
                  <Link href="/">
                    <span>
                      {locale !== Locales.English
                        ? Buttons.home.en
                        : Buttons.home.bn}
                    </span>
                  </Link>
                </label>
              </div>
              <div className="tab">
                <label
                  className="tab__label"
                  htmlFor="tab-2"
                  onClick={handleClose}
                >
                  <Link href="/who-we-are">
                    <span>
                      {locale !== Locales.English
                        ? Buttons.who.en
                        : Buttons.who.bn}
                    </span>
                  </Link>
                </label>
              </div>

              <div className="tab">
                <input className="tab__input" id="tab-3" type="checkbox" />
                <label className="tab__label" htmlFor="tab-3">
                  <span>
                    {locale !== Locales.English
                      ? Buttons.products.en
                      : Buttons.products.bn}
                  </span>
                </label>
                <div className="tab__content">
                  <div className="content__wrap">
                    <div className="multi__subMenu">
                      <ul>
                        <p>
                          {locale !== Locales.English
                            ? Buttons.cam.en
                            : Buttons.cam.bn}
                        </p>
                        <li>
                          {dropDown?.Campaigns.map((item, idx) => {
                            return (
                              <Link href={item?.["slug url"]} key={idx}>
                                <a onClick={handleClose}>
                                  {locale !== Locales.English
                                    ? item?.title
                                    : item?.["title bn"]}
                                </a>
                              </Link>
                            );
                          })}
                        </li>
                      </ul>

                      <ul>
                        <p>
                          {locale !== Locales.English
                            ? Buttons.serv.en
                            : Buttons.serv.bn}
                        </p>
                        <li>
                          {dropDown?.Services.map((item, idx) => {
                            return (
                              <Link href={item?.["slug url"]} key={idx}>
                                <a onClick={handleClose}>
                                  {locale !== Locales.English
                                    ? item?.title
                                    : item?.["title bn"]}
                                </a>
                              </Link>
                            );
                          })}
                        </li>
                      </ul>

                      <ul>
                        <p>
                          {locale !== Locales.English
                            ? Buttons.pay.en
                            : Buttons.pay.bn}
                        </p>
                        <li>
                          {dropDown?.Payments.map((item, idx) => {
                            return (
                              <Link href={item?.["slug url"]} key={idx}>
                                <a onClick={handleClose}>
                                  {locale !== Locales.English
                                    ? item?.title
                                    : item?.["title bn"]}
                                </a>
                              </Link>
                            );
                          })}
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>

              <div className="tab">
                {/* <input className="tab__input" id="tab-4" type="checkbox" /> */}
                <label
                  className="tab__label"
                  htmlFor="tab-4"
                  onClick={handleClose}
                >
                  <Link href="/partner-with-us">
                    <span>
                      {locale !== Locales.English
                        ? Buttons.partner.en
                        : Buttons.partner.bn}
                    </span>
                  </Link>
                </label>
                {/* <div className="tab__content">
                  <div className="content__wrap">
                    <div className="single__subMenu">
                      <ul>
                        <li>
                          <Link href="/partner-with-us#merchant">
                            <a>Merchant</a>
                          </Link>
                        </li>

                        <li>
                          <Link href="/partner-with-us#agent">
                            <a>Agent</a>
                          </Link>
                        </li>

                        <li>
                          <Link href="/partner-with-us">
                            <a>Corporates</a>
                          </Link>
                        </li>

                        <li>
                          <Link href="/partner-with-us">
                            <a>SMEs</a>
                          </Link>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div> */}
              </div>

              <div className="tab">
                <label
                  className="tab__label"
                  htmlFor="tab-5"
                  onClick={handleClose}
                >
                  <Link href="/service-location">
                    <span>
                      {locale !== Locales.English
                        ? Buttons.service.en
                        : Buttons.service.bn}
                    </span>
                  </Link>
                </label>
              </div>

              <div className="tab">
                <label
                  className="tab__label"
                  htmlFor="tab-6"
                  onClick={handleClose}
                >
                  <Link href="/Media">
                    <span>
                      {locale !== Locales.English
                        ? Buttons.media.en
                        : Buttons.media.bn}
                    </span>
                  </Link>
                </label>
              </div>
            </div>
          </div>
          <div className="store__connect--part">
            <a href="https://cutt.ly/NzXvJDf">
              <img src="/images/Google-Play-btn-mob.jpg" alt="" />
            </a>
            {/* <a href="#!">
              <img src="/images/Apple-App-btn-mob.jpg" alt="" />
            </a> */}
          </div>
        </div>
      </div>
      <div
        className={
          madvanceSearch ? "mob__menu-drwr-true" : "mob__menu-drwr-false"
        }
      >
        <div className="mobile__sideBar__menu--main">
          <div className="close__btn--part">
            <a onClick={handleAdClose}>
              <svg
                width="16"
                height="16"
                viewBox="0 0 16 16"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M2.75 2.04297L2.04297 2.75L2.39844 3.10156L7.29297 8L2.04297 13.25L2.75 13.957L8 8.70703L12.8945 13.6055L13.25 13.957L13.957 13.25L13.6055 12.8945L8.70703 8L13.957 2.75L13.25 2.04297L8 7.29297L3.10156 2.39844L2.75 2.04297Z"
                  fill="black"
                />
              </svg>
            </a>
          </div>

          {/* <div className='side__menuList--wrapper'>
            <div className='css__collapse'>
              <div className='tab'>
                <label className='tab__label' htmlFor='tab-1'>
                  <span>Home</span>
                </label>
              </div>
              <div className='tab'>
                <label className='tab__label' htmlFor='tab-2'>
                  <span>Who We Are?</span>
                </label>
              </div>

              <div className='tab'>
                <input className='tab__input' id='tab-3' type='checkbox' />
                <label className='tab__label' htmlFor='tab-3'>
                  <span>Products & Campaigns</span>
                </label>
                <div className='tab__content'>
                  <div className='content__wrap'>
                    <div className='multi__subMenu'>
                      <ul>
                        <p>Campaign</p>
                        <li>
                          <a href='#!'>Latest Offer</a>
                          <a href='#!'>Limits and Charges</a>
                        </li>
                      </ul>

                      <ul>
                        <p>Services</p>
                        <li>
                          <a href='#!'>Send Money</a>
                          <a href='#!'>Mobile Topup</a>
                          <a href='#!'>Cash In</a>
                          <a href='#!'>Cash Out</a>
                          <a href='#!'>Make Payment</a>
                          <a href='#!'>Pay Bill</a>
                          <a href='#!'>Add Money</a>
                          <a href='#!'>Request Money</a>
                          <a href='#!'>Request Money</a>
                          <a href='#!'>Remittance</a>
                        </li>
                      </ul>

                      <ul>
                        <p>Payments</p>
                        <li>
                          <a href='#!'>Traffic</a>
                          <a href='#!'>IVAC</a>
                          <a href='#!'>Ticket</a>
                          <a href='#!'>Donation</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>

              <div className='tab'>
                <input className='tab__input' id='tab-4' type='checkbox' />
                <label className='tab__label' htmlFor='tab-4'>
                  <span>Partner with Us</span>
                </label>
                <div className='tab__content'>
                  <div className='content__wrap'>
                    <div className='single__subMenu'>
                      <ul>
                        <li>
                          <a href='#!'>Merchant</a>
                        </li>

                        <li>
                          <a href='#!'>Agent</a>
                        </li>

                        <li>
                          <a href='#!'>Corporates</a>
                        </li>

                        <li>
                          <a href='#!'>SMEs</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>

              <div className='tab'>
                <label className='tab__label' htmlFor='tab-5'>
                  <span>Service Location</span>
                </label>
              </div>

              <div className='tab'>
                <label className='tab__label' htmlFor='tab-6'>
                  <span>Media</span>
                </label>
              </div>
            </div>
          </div> */}

          {/* <div className='store__connect--part'>
            <a href='#!'>
              <img src='/images/Google-Play-btn-mob.jpg' alt='' />
            </a>
            <a href='#!'>
              <img src='/images/Apple-App-btn-mob.jpg' alt='' />
            </a>
          </div> */}

          <div className="mobmenu-search-box">
            <div className="menu__search">
              <div className="search__barMain">
                <form className="search__mainForm">
                  <div className="form-group curstom__form-group">
                    <input
                      type="text"
                      className="form-control curstom__form-control"
                      placeholder="Enter Keyword to Search"
                      name="search"
                      autoComplete="on"
                      value={inputText}
                      onChange={(e) => setInputText(e.target.value)}
                    />
                  </div>
                  <button type="submit" className="btn search__go--btn">
                    <svg
                      width={26}
                      height={26}
                      viewBox="0 0 26 26"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M18.6042 16.4167H17.4521L17.0438 16.0229C18.4729 14.3604 19.3333 12.2021 19.3333 9.85417C19.3333 4.61875 15.0896 0.375 9.85417 0.375C4.61875 0.375 0.375 4.61875 0.375 9.85417C0.375 15.0896 4.61875 19.3333 9.85417 19.3333C12.2021 19.3333 14.3604 18.4729 16.0229 17.0438L16.4167 17.4521V18.6042L23.7083 25.8813L25.8813 23.7083L18.6042 16.4167ZM9.85417 16.4167C6.22292 16.4167 3.29167 13.4854 3.29167 9.85417C3.29167 6.22292 6.22292 3.29167 9.85417 3.29167C13.4854 3.29167 16.4167 6.22292 16.4167 9.85417C16.4167 13.4854 13.4854 16.4167 9.85417 16.4167Z"
                        fill="#F6F6F6"
                      />
                    </svg>
                  </button>
                </form>
                {/* <div className='options__mobile--sec'>
                  <a href='#!' className='btn options__btn'>
                    Options{' '}
                    <svg
                      xmlns='http://www.w3.org/2000/svg'
                      width={19}
                      height={18}
                      viewBox='0 0 19 18'
                      fill='none'>
                      <path
                        d='M19 0.987305H0V17.6123H19V0.987305ZM1.1875 2.1748H17.8125V4.5498H1.1875V2.1748ZM17.8125 16.4248H1.1875V5.7373H17.8125V16.4248ZM5.9375 10.4873V6.9248H2.375V10.4873H5.9375ZM4.75 8.1123V9.2998H3.5625V8.1123H4.75ZM5.9375 15.2373V11.6748H2.375V15.2373H5.9375ZM4.75 12.8623V14.0498H3.5625V12.8623H4.75ZM15.4375 9.2998V8.1123H8.3125V9.2998H15.4375ZM15.4375 14.0498V12.8623H8.3125V14.0498H15.4375Z'
                        fill='black'
                      />
                    </svg>
                  </a>
                </div> */}
              </div>
            </div>
          </div>

          <div className="desk__menu--advancesearch">
            <div className="mbl-search-res">
              {suggestiveSearch.loading && (
                <div className="container">Loading...</div>
              )}
              {suggestiveSearch?.result?.data?.map((item, index) => {
                return (
                  <div
                    className="search_bottom"
                    key={index}
                    onClick={onSearchClick}
                  >
                    <Link href={item.url}>
                      <p className="pointer text-uppercase">
                        {locale !== Locales.English
                          ? item?.["title_english"]
                          : item?.["title_bangla"]}
                      </p>
                    </Link>
                  </div>
                );
              })}
            </div>
            <div className="container mb-cont">
              {!inputText && (
                <div className="container">
                  <h1>
                    {locale !== Locales.English
                      ? Buttons.popular.en
                      : Buttons.popular.bn}
                  </h1>
                  <div className="ad-sear-list-wrap">
                    <div className="row">
                      {dropDown?.Services.slice(0, 5).map((item, idx) => {
                        return (
                          <div className="col-12" key={idx}>
                            <a
                              className="advance__options"
                              onClick={onSearchClick}
                            >
                              <div className="media advance__options-media">
                                <div className="media-body">
                                  <Link href={item?.["slug url"]}>
                                    <h5 className="mt-0 pointer text-uppercase">
                                      {locale !== Locales.English
                                        ? item.title
                                        : item["title bn"]}
                                    </h5>
                                  </Link>
                                </div>
                              </div>
                            </a>
                          </div>
                        );
                      })}

                      <div className="col-xl-4">
                        <a className="advance__options" onClick={onSearchClick}>
                          <div className="media advance__options-media">
                            <div className="media-body">
                              <Link href="/limits-and-charges">
                                <h5 className="mt-0 pointer">
                                  {locale !== Locales.English
                                    ? Buttons.limits.en
                                    : Buttons.limits.bn}
                                </h5>
                              </Link>
                            </div>
                          </div>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
