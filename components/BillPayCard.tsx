import React from "react";
import Link from "next/link";

type Data = {
  data: {
    icon: string;
    "icon sd": string;
    title: string;
    "title bn": string;
  };
  isEn: boolean;
};

const BillPayCard = ({ data, isEn }: Data): JSX.Element => {
  const url = data?.["slug url"].split("/")[
    data?.["slug url"].split("/").length - 1
  ];
  return (
    <div>
      <div className="bill_pay-card">
        <div className="media bill_pay-media">
          <div style={{ width: "120px" }}>
            <picture>
              <source
                media="(min-width:650px)"
                srcSet={`${process.env.IMAGE_URL}${data?.icon}`}
              />
              <source
                media="(max-width:649px)"
                srcSet={`${process.env.IMAGE_URL}${data?.icon}`}
              />
              <img
                className="comp__icon align-self-center mr-3"
                src={`${process.env.IMAGE_URL}${data?.icon}`}
                alt="Ucbl"
              />
            </picture>
          </div>

          {/* <img
            className="align-self-center mr-3 comp__icon"
            src={img}
            alt="Generic placeholder image"
          /> */}
          <div className="media-body bill_pay-media-body">
            <h5
              className="mt-0"
              dangerouslySetInnerHTML={{
                __html: isEn ? data?.title : data?.["title bn"],
              }}
            ></h5>
            <div className="right__arrow--btnPart">
              <Link href={data?.["slug url"]}>
                <a className="btn right__arrow--btn primary__bg--btn inline__blck--btn">
                  Read More{" "}
                  <svg
                    width={40}
                    height={24}
                    viewBox="0 0 40 24"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M26.4575 11H6.60938V13H26.4575V16L33.0515 12L26.4575 8V11Z"
                      fill="black"
                    />
                  </svg>
                </a>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BillPayCard;
