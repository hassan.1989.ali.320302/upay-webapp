import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { HomeSlider } from "../interfaces/homeSlider";
import ButtonText from "../translations/button.json";
import Fade from "react-reveal/Fade";

type Props = {
  sliders: Object[];
  isEn: boolean;
  isHome: boolean;
};

const MainSlider = ({ sliders, isEn, isHome = true }: Props): JSX.Element => {
  var settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    initialSlide: 0,
    arrows: false,
    autoplay: true,
    // responsive: [
    //   {
    //     breakpoint: 1024,
    //     settings: {
    //       slidesToShow: 3,
    //       slidesToScroll: 3,
    //       infinite: true,
    //       dots: true,
    //     },
    //   },
    //   {
    //     breakpoint: 600,
    //     settings: {
    //       slidesToShow: 2,
    //       slidesToScroll: 2,
    //       initialSlide: 2,
    //     },
    //   },
    //   {
    //     breakpoint: 480,
    //     settings: {
    //       slidesToShow: 1,
    //       slidesToScroll: 1,
    //     },
    //   },
    // ],
  };
  return (
    <div>
      <div className="home__slider--main">
        <Slider {...settings}>
          {sliders.map((slider: HomeSlider, key) => {
            return (
              <div key={key} className="home__slider--mainPart">
                {/* <img src={i.img} alt={i.imgName} /> */}
                <picture>
                  <source
                    media="(min-width:671px)"
                    srcSet={process.env.IMAGE_URL + slider?.image}
                  />
                  <source
                    media="(max-width:670px)"
                    srcSet={process.env.IMAGE_URL + slider?.["image sd"]}
                  />
                  <img
                    src={process.env.IMAGE_URL + slider?.image}
                    alt="Upay ucbl"
                    // style={{ width: "100%", maxHeight: "100vh" }}
                  />
                </picture>
                {isHome && (
                  <div className="home__slider--descPart">
                    <Fade left>
                      <h3>{isEn ? slider?.title : slider?.["title bn"]}</h3>
                      <div className="right__arrow--btnPart">
                        <a
                          href={slider?.["slug url"]}
                          className="btn right__arrow--btn primary__bg--btn inline__blck--btn"
                        >
                          {isEn
                            ? ButtonText.read_more.en
                            : ButtonText.read_more.bn}{" "}
                          <svg
                            width="40"
                            height="24"
                            viewBox="0 0 40 24"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              d="M26.4575 11H6.60938V13H26.4575V16L33.0515 12L26.4575 8V11Z"
                              fill="black"
                            />
                          </svg>
                        </a>
                      </div>
                    </Fade>
                  </div>
                )}
                <div className="home-slide-overlay"></div>
              </div>
            );
          })}
        </Slider>
      </div>
    </div>
  );
};

export default MainSlider;
