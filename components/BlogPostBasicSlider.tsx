import React, { Component } from "react";
import Slider from "react-slick";
import Fade from "react-reveal/Fade";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

type Props = {
  sliders: [];
};

class BlogPostBasicSlider extends Component<Props> {
  slider: any;
  constructor(props) {
    super(props);
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
  }
  next() {
    this.slider.slickNext();
  }
  previous() {
    this.slider.slickPrev();
  }
  render() {
    const settings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: this.props.sliders.length > 3 ? 3 : 2,
      slidesToScroll: 1,
      // dots: false,
      centerMode: true,
      arrows: false,
      responsive: [
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 575,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    };
    return (
      <div>
        <Fade bottom>
          <div className="blog__post-mainslides">
            <Slider ref={(c) => (this.slider = c)} {...settings}>
              {this.props.sliders?.map((slider, index) => {
                return (
                  <div key={index}>
                    <div className="slider__imgWrap">
                      {slider?.["image"] && (
                        <picture>
                          <source
                            media="(min-width:650px)"
                            srcSet={process.env.IMAGE_URL + slider?.["image"]}
                          />
                          <source
                            media="(max-width:649px)"
                            srcSet={
                              process.env.IMAGE_URL + slider?.["image sd"]
                            }
                          />
                          <img
                            src={process.env.IMAGE_URL + slider?.["image"]}
                            alt="Blog Ucbl"
                          />
                        </picture>
                      )}
                    </div>
                  </div>
                );
              })}
            </Slider>
            <div className="arrow__btn--parts">
              <button className="button prev__btn" onClick={this.previous}>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="35"
                  height="35"
                  viewBox="0 0 35 35"
                  fill="none"
                >
                  <path
                    d="M21.4676 33.3699L5.74436 18.7633L21.4676 4.15671"
                    stroke="#4E4E50"
                    strokeWidth="3"
                  />
                </svg>
              </button>
              <button className="button next__btn" onClick={this.next}>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="35"
                  height="35"
                  viewBox="0 0 35 35"
                  fill="none"
                >
                  <path
                    d="M13.5324 33.3699L29.2556 18.7633L13.5324 4.15671"
                    stroke="#4E4E50"
                    strokeWidth="3"
                  />
                </svg>
              </button>
            </div>
          </div>
        </Fade>
      </div>
    );
  }
}

export default BlogPostBasicSlider;
