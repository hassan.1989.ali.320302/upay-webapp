import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const OurMarchantPartner = ({ data }): JSX.Element => {
  var settings = {
    dots: false,
    infinite: true,
    autoplay: true,
    speed: 500,
    slidesToShow: 2,
    slidesToScroll: 1,
    initialSlide: 0,
    arrows: false,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,
        },
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,
        },
      },
    ],
  };
  return (
    <div>
      <div className="ourvalue-agent--slider">
        <Slider {...settings}>
          {data?.map((slider, index) => {
            return (
              <div key={index}>
                <div className="plf-pd">
                  <div className="partner__card">
                    <picture>
                      <source
                        media="(min-width:671px)"
                        srcSet={process.env.IMAGE_URL + slider.image}
                      />
                      <source
                        media="(max-width:670px)"
                        srcSet={process.env.IMAGE_URL + slider["image sd"]}
                      />
                      <img
                        src={process.env.IMAGE_URL + slider.image}
                        alt="Marchent Partner"
                      />
                    </picture>
                  </div>
                </div>
              </div>
            );
          })}
        </Slider>
      </div>
    </div>
  );
};

export default OurMarchantPartner;
