import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const LetsHelpSlider = ({ data, isEn }): JSX.Element => {
  var settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    initialSlide: 0,
    arrows: false,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
        },
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,
        },
      },
    ],
  };

  const isMobile =
    typeof window !== "undefined" && window?.screen?.width <= 425;

  if (data) {
    return (
      <div>
        <div className="lets__help--slider">
          <Slider {...settings}>
            {data.map((item, index) => {
              let isNumber = item?.title.toLowerCase().includes("talk");
              return (
                <div key={index}>
                  <a
                    className="lcard-link"
                    href={isNumber ? `tel:${item?.url}` : item?.url}
                  >
                    <div
                      className="card lets__help__cardMains"
                      style={{
                        backgroundImage: isMobile
                          ? `url(${process.env.IMAGE_URL + item["image sd"]})`
                          : `url(${process.env.IMAGE_URL + item.image})`,
                      }}
                    >
                      <div className="overlay" />

                      <picture>
                        <source
                          media="(min-width:650px)"
                          srcSet={process.env.IMAGE_URL + item.icon}
                        />
                        <source
                          media="(max-width:649px)"
                          srcSet={process.env.IMAGE_URL + item?.["icon sd"]}
                        />
                        <img
                          src={process.env.IMAGE_URL + item.icon}
                          alt="partners ucbl"
                          className="card-img-top"
                          style={{
                            marginTop: "30px",
                          }}
                        />
                      </picture>
                      <div className="card-body">
                        <h5
                          className="card-title"
                          dangerouslySetInnerHTML={{
                            __html: isEn ? item.title : item["title bn"],
                          }}
                        ></h5>
                        <p
                          className="card-text mb-3"
                          dangerouslySetInnerHTML={{
                            __html: isEn ? item.desc : item["desc bn"],
                          }}
                        ></p>
                      </div>
                    </div>
                  </a>
                </div>
              );
            })}
          </Slider>
        </div>
      </div>
    );
  } else return null;
};

export default LetsHelpSlider;
