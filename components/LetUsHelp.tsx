import React, { useEffect, useState } from "react";
import LetsHelpSlider from "./LetsHelpSlider";
import { GetStaticProps, InferGetStaticPropsType } from "next";
import { getLetUsHelp } from "../server";
import Fade from "react-reveal/Fade";

// type Page = {
//   data: any;
//   error: string;
// };

// export const getStaticProps: GetStaticProps = async (context) => {
//   let page: Page;
//   try {
//     const res = await getLetUsHelp();
//     page = {
//       data: res.data,
//       error: "",
//     };
//   } catch (err) {
//     page = {
//       data: {},
//       error: "Something Went Wrong!",
//     };
//   }
//   //   console.log("here", page);
//   return {
//     props: {
//       page,
//     },
//   };
// };

function LetUsHelp({ isEn }): JSX.Element {
  const [helpdata, setHelpData] = useState(null);

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    try {
      setHelpData((await getLetUsHelp()).data.rows.Data[0]);
    } catch (error) {
      console.log(error);
    }
  };

  if (helpdata) {
    return (
      <section className="bg__gray">
        <div className="lets__help__you--sectionMain tb__padding">
          <Fade bottom cascade>
            <div className="container">
              <div className="dashmid__wDesc__header--part bottom__margin">
                <h1
                  dangerouslySetInnerHTML={{
                    __html: isEn ? helpdata.title : helpdata["title bn"],
                  }}
                ></h1>
                <p
                  dangerouslySetInnerHTML={{
                    __html: isEn ? helpdata.desc : helpdata["desc bn"],
                  }}
                ></p>
              </div>
              <div className="lets__help__sliderMain">
                <LetsHelpSlider data={helpdata["app list"]} isEn={isEn} />
              </div>
            </div>
          </Fade>
        </div>
      </section>
    );
  } else {
    return null;
  }
}

export default LetUsHelp;
