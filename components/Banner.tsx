import React from "react";
import Fade from "react-reveal/Fade";

type Cover = {
  image: string;
  imagesd: string;
};

const Banner: React.FC<{ name: string; cover: Cover }> = ({
  name,
  cover,
}): JSX.Element => {
  return (
    <div>
      <div className="main__banner--section">
        <div className="img__part">
          <picture>
            <source
              media="(max-width: 767px)"
              srcSet={process.env.IMAGE_URL + cover.imagesd}
            />
            <source
              media="(min-width: 768px)"
              srcSet={process.env.IMAGE_URL + cover.image}
            />
            <img
              src={cover.image}
              alt="banner"
              // style={{ maxHeight: "100vh" }}
            />
          </picture>
        </div>
        <div className="overlay"></div>
        <div className="dashmid__wDesc__header--part">
          <Fade bottom collapse>
            <h1 dangerouslySetInnerHTML={{ __html: name }} />
          </Fade>
        </div>
      </div>
    </div>
  );
};

export default Banner;
