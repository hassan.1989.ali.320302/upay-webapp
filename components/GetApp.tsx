import React, { useState, useEffect } from "react";
import Fade from "react-reveal/Fade";
import { getApp } from "../server";

const GetApp = ({ isEn }) => {
  const [data, setData] = useState(null);

  useEffect(() => {
    fetchData();
  }, []);
  const fetchData = async () => {
    try {
      const res = await getApp();
      setData(res.data.rows.Data[0]);
    } catch (error) {
      // console.log(error);
    }
  };
  return (
    <div>
      <div className="get__app-mainPart">
        <div className="container">
          <div className="row">
            <div className="col-xl-5 col-lg-5 col-md-6 custom__col">
              <Fade left cascade>
                <div className="left__part">
                  <h2
                    dangerouslySetInnerHTML={{
                      __html: isEn ? data?.title : data?.["title bn"],
                    }}
                  ></h2>
                  <p
                    dangerouslySetInnerHTML={{
                      __html: isEn ? data?.desc : data?.["desc bn"],
                    }}
                  ></p>
                </div>
              </Fade>
            </div>
            <div className="col-xl-7 col-lg-7 col-md-6 custom__col">
              <Fade right cascade>
                <div className="right__part">
                  {data?.["segment one"].map((btn, index) => {
                    return (
                      <a
                        href={btn.url}
                        target="_blank"
                        className={
                          index % 2 === 0 ? "play_store-btn" : "apple_store-btn"
                        }
                        key={index}
                      >
                        <picture>
                          <source
                            media="(min-width:650px)"
                            srcSet={process.env.IMAGE_URL + btn?.icon}
                          />
                          <source
                            media="(max-width:649px)"
                            srcSet={process.env.IMAGE_URL + btn?.["icon sd"]}
                          />
                          <img
                            src={process.env.IMAGE_URL + btn?.icon}
                            alt="App of Ucbl"
                            // style={{
                            //   width: "40%",
                            //   objectFit: "cover",
                            // }}
                          />
                        </picture>
                      </a>
                    );
                  })}
                </div>
              </Fade>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default GetApp;
