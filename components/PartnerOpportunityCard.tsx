import React from "react";

type Prop = {
  img: string;
  imgsd: string;
  title: string;
};

const PartnerOpportunityCard = ({ img, imgsd, title }: Prop): JSX.Element => {
  return (
    <div
      // style={{ marginLeft: "2rem", marginRight: "2rem" }}
      className="col-6 col-md-3 col-sm-6"
    >
      <a className="partner-opportunity-card">
        <div className="text-center partner-opp-icon">
          <picture>
            <source
              media="(min-width:671px)"
              srcSet={process.env.IMAGE_URL + img}
            />
            <source
              media="(max-width:670px)"
              srcSet={process.env.IMAGE_URL + imgsd}
            />
            <img
              src={process.env.IMAGE_URL + img}
              alt="Partner"
              // style={{
              //   marginTop: "1.5rem",
              //   width: "50%",
              //   height: "50%",
              //   textAlign: "center",
              //   borderRadius: "50%",
              // }}
            />
          </picture>
        </div>
        <div className="desc-main">
          <p
            className="po-card-desc"
            dangerouslySetInnerHTML={{ __html: title }}
          ></p>
        </div>
      </a>
    </div>
  );
};

export default PartnerOpportunityCard;
