import { useState } from "react";
import { useForm } from "react-hook-form";
import Loader from "react-loader-spinner";
import { partnerForm } from "../server";
import { toast } from "react-toastify";

export default function Form({ isEn, headers, divs, types }) {
  const [loading, setLoading] = useState<boolean>(false);
  const { register, handleSubmit, errors, reset } = useForm();

  const onSubmit = (data) => {
    setLoading(true);
    try {
      delete data.aggree;
      partnerForm(data);
      toast.success("Thanks for your interest! We will get back to you soon!");
      reset();
    } catch (err) {
      toast.error("Please try later again!");
    }
    setLoading(false);
  };

  return (
    <section>
      <div className="want-to-join tb__padding" id="want-to-join-form">
        <div className="container">
          <div className="row">
            <div className="col-xl-12">
              <div className="dashleft__wDesc__header--part">
                <h1>{isEn ? headers?.title : headers?.["title bn"]}</h1>
                <p>{isEn ? headers?.desc : headers?.["desc bn"]}</p>
              </div>

              <div>
                <div className="fill__up__mainFormPart">
                  <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="form-row">
                      <div className="form-group fill__up-form-group col-md-6">
                        <div className="input-group fill__up-input-group">
                          <div className="input-group-prepend fill__up-input-group-prepend">
                            <span
                              className="input-group-text fill__up-input-group-text"
                              id="basic-addon1"
                            >
                              <svg
                                width={17}
                                height={18}
                                viewBox="0 0 17 18"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                              >
                                <path
                                  d="M8.90269 2.69974C10.0534 2.69974 10.9859 3.65952 10.9859 4.84393C10.9859 6.02833 10.0534 6.98811 8.90269 6.98811C7.75198 6.98811 6.81951 6.02833 6.81951 4.84393C6.81951 3.65952 7.75198 2.69974 8.90269 2.69974ZM8.90269 11.8891C11.8489 11.8891 14.9538 13.3798 14.9538 14.0333V15.1564H2.85157V14.0333C2.85157 13.3798 5.95649 11.8891 8.90269 11.8891ZM8.90269 0.759766C6.7104 0.759766 4.93474 2.58743 4.93474 4.84393C4.93474 7.10042 6.7104 8.92809 8.90269 8.92809C11.095 8.92809 12.8706 7.10042 12.8706 4.84393C12.8706 2.58743 11.095 0.759766 8.90269 0.759766ZM8.90269 9.94913C6.25408 9.94913 0.966797 11.3173 0.966797 14.0333V17.0964H16.8386V14.0333C16.8386 11.3173 11.5513 9.94913 8.90269 9.94913Z"
                                  fill="#FFD602"
                                />
                              </svg>
                            </span>
                          </div>
                          <input
                            type="text"
                            className="form-control fill__up-input-form-control"
                            placeholder="First Name"
                            name="first_name"
                            ref={register({ required: true })}
                          />
                        </div>
                        <span className="text-danger">
                          {" "}
                          {errors.first_name && "Please enter first name"}
                        </span>
                      </div>
                      <div className="form-group fill__up-form-group col-md-6">
                        <div className="input-group fill__up-input-group">
                          <div className="input-group-prepend fill__up-input-group-prepend">
                            <span
                              className="input-group-text fill__up-input-group-text"
                              id="basic-addon1"
                            >
                              <svg
                                width={17}
                                height={18}
                                viewBox="0 0 17 18"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                              >
                                <path
                                  d="M8.90269 2.69974C10.0534 2.69974 10.9859 3.65952 10.9859 4.84393C10.9859 6.02833 10.0534 6.98811 8.90269 6.98811C7.75198 6.98811 6.81951 6.02833 6.81951 4.84393C6.81951 3.65952 7.75198 2.69974 8.90269 2.69974ZM8.90269 11.8891C11.8489 11.8891 14.9538 13.3798 14.9538 14.0333V15.1564H2.85157V14.0333C2.85157 13.3798 5.95649 11.8891 8.90269 11.8891ZM8.90269 0.759766C6.7104 0.759766 4.93474 2.58743 4.93474 4.84393C4.93474 7.10042 6.7104 8.92809 8.90269 8.92809C11.095 8.92809 12.8706 7.10042 12.8706 4.84393C12.8706 2.58743 11.095 0.759766 8.90269 0.759766ZM8.90269 9.94913C6.25408 9.94913 0.966797 11.3173 0.966797 14.0333V17.0964H16.8386V14.0333C16.8386 11.3173 11.5513 9.94913 8.90269 9.94913Z"
                                  fill="#FFD602"
                                />
                              </svg>
                            </span>
                          </div>
                          <input
                            type="text"
                            className="form-control fill__up-input-form-control"
                            placeholder="Last Name"
                            name="last_name"
                            ref={register({ required: true })}
                          />
                        </div>
                        <span className="text-danger">
                          {" "}
                          {errors.last_name && "Please enter last name"}
                        </span>
                      </div>
                      <div className="form-group fill__up-form-group col-md-6">
                        <div className="input-group fill__up-input-group">
                          <div className="input-group-prepend fill__up-input-group-prepend">
                            <span
                              className="input-group-text fill__up-input-group-text"
                              id="basic-addon1"
                            >
                              <svg
                                width={19}
                                height={20}
                                viewBox="0 0 19 20"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                              >
                                <path
                                  d="M3.75574 8.75761C5.22603 11.6472 7.59485 14.0058 10.4844 15.4863L12.7307 13.24C13.0064 12.9643 13.4148 12.8724 13.7721 12.9949C14.9157 13.3727 16.1512 13.5769 17.4173 13.5769C17.9788 13.5769 18.4383 14.0364 18.4383 14.598V18.1614C18.4383 18.723 17.9788 19.1824 17.4173 19.1824C7.82969 19.1824 0.0595703 11.4123 0.0595703 1.82475C0.0595703 1.26318 0.519038 0.803711 1.08061 0.803711H4.65425C5.21582 0.803711 5.67529 1.26318 5.67529 1.82475C5.67529 3.10105 5.8795 4.3263 6.25728 5.46986C6.3696 5.82723 6.28791 6.22543 6.00202 6.51133L3.75574 8.75761Z"
                                  fill="#FFD602"
                                />
                              </svg>
                            </span>
                          </div>
                          <input
                            type="text"
                            className="form-control fill__up-input-form-control"
                            placeholder="Phone Number"
                            name="phone_number"
                            ref={register({
                              required: true,
                              pattern: {
                                value: /(^(\+88|0088)?(01){1}[3456789]{1}(\d){8})$/,
                                message: "Please enter valid mobile no",
                              },
                            })}
                          />
                        </div>
                        <span className="text-danger">
                          {" "}
                          {errors.phone_number && "Please enter phone number"}
                        </span>
                      </div>
                      <div className="form-group fill__up-form-group col-md-6">
                        <div className="input-group fill__up-input-group">
                          <div className="input-group-prepend fill__up-input-group-prepend">
                            <span
                              className="input-group-text fill__up-input-group-text"
                              id="basic-addon1"
                            >
                              <svg
                                width={22}
                                height={18}
                                viewBox="0 0 22 18"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                              >
                                <path
                                  d="M19.6254 0.824219H2.55517C1.38159 0.824219 0.432056 1.74315 0.432056 2.8663L0.421387 15.1188C0.421387 16.2419 1.38159 17.1609 2.55517 17.1609H19.6254C20.799 17.1609 21.7592 16.2419 21.7592 15.1188V2.8663C21.7592 1.74315 20.799 0.824219 19.6254 0.824219ZM19.6254 15.1188H2.55517V4.90838L11.0903 10.0136L19.6254 4.90838V15.1188ZM11.0903 7.9715L2.55517 2.8663H19.6254L11.0903 7.9715Z"
                                  fill="#FFD602"
                                />
                              </svg>
                            </span>
                          </div>
                          <input
                            type="email"
                            className="form-control fill__up-input-form-control"
                            placeholder="E-mail Address"
                            name="email"
                            ref={register({
                              required: true,
                              pattern: {
                                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                message: "Please enter valid e-mail address",
                              },
                            })}
                          />
                        </div>
                        <span className="text-danger">
                          {" "}
                          {errors.email && "Please enter e-mail address"}
                        </span>
                      </div>
                      <div className="form-group fill__up-form-group col-md-6">
                        <div className="input-group fill__up-input-group">
                          <div className="input-group-prepend fill__up-input-group-prepend">
                            <span
                              className="input-group-text fill__up-input-group-text"
                              id="basic-addon1"
                            >
                              <svg
                                width={17}
                                height={5}
                                viewBox="0 0 17 5"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                              >
                                <path
                                  d="M2.95077 0.142578C1.85958 0.142578 0.966797 1.06151 0.966797 2.18466C0.966797 3.3078 1.85958 4.22674 2.95077 4.22674C4.04195 4.22674 4.93474 3.3078 4.93474 2.18466C4.93474 1.06151 4.04195 0.142578 2.95077 0.142578ZM14.8546 0.142578C13.7634 0.142578 12.8706 1.06151 12.8706 2.18466C12.8706 3.3078 13.7634 4.22674 14.8546 4.22674C15.9458 4.22674 16.8386 3.3078 16.8386 2.18466C16.8386 1.06151 15.9458 0.142578 14.8546 0.142578ZM8.90268 0.142578C7.8115 0.142578 6.91871 1.06151 6.91871 2.18466C6.91871 3.3078 7.8115 4.22674 8.90268 4.22674C9.99387 4.22674 10.8867 3.3078 10.8867 2.18466C10.8867 1.06151 9.99387 0.142578 8.90268 0.142578Z"
                                  fill="#FFD602"
                                />
                              </svg>
                            </span>
                          </div>
                          <select
                            className="form-control fill__up-input-form-control"
                            placeholder="Type"
                            name="agent_type"
                            ref={register({ required: true })}
                          >
                            <option key="" value="">
                              Select Type
                            </option>

                            {types?.map((type, idx) => (
                              <option
                                key={idx}
                                value={type?.agent_list}
                                className="text-capitalize"
                              >
                                {type?.agent_list}
                              </option>
                            ))}
                          </select>
                        </div>
                        <span className="text-danger">
                          {" "}
                          {errors.division && "Please enter division"}
                        </span>
                      </div>
                      <div className="form-group fill__up-form-group col-md-6">
                        <div className="input-group fill__up-input-group">
                          <div className="input-group-prepend fill__up-input-group-prepend">
                            <span
                              className="input-group-text fill__up-input-group-text"
                              id="basic-addon1"
                            >
                              <svg
                                width={17}
                                height={5}
                                viewBox="0 0 17 5"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                              >
                                <path
                                  d="M2.95077 0.142578C1.85958 0.142578 0.966797 1.06151 0.966797 2.18466C0.966797 3.3078 1.85958 4.22674 2.95077 4.22674C4.04195 4.22674 4.93474 3.3078 4.93474 2.18466C4.93474 1.06151 4.04195 0.142578 2.95077 0.142578ZM14.8546 0.142578C13.7634 0.142578 12.8706 1.06151 12.8706 2.18466C12.8706 3.3078 13.7634 4.22674 14.8546 4.22674C15.9458 4.22674 16.8386 3.3078 16.8386 2.18466C16.8386 1.06151 15.9458 0.142578 14.8546 0.142578ZM8.90268 0.142578C7.8115 0.142578 6.91871 1.06151 6.91871 2.18466C6.91871 3.3078 7.8115 4.22674 8.90268 4.22674C9.99387 4.22674 10.8867 3.3078 10.8867 2.18466C10.8867 1.06151 9.99387 0.142578 8.90268 0.142578Z"
                                  fill="#FFD602"
                                />
                              </svg>
                            </span>
                          </div>
                          <select
                            className="form-control fill__up-input-form-control"
                            placeholder="Division"
                            name="division"
                            ref={register({ required: true })}
                          >
                            <option value="">Select Division</option>

                            {divs?.map((div, idx) => (
                              <option
                                key={idx}
                                value={div?.division_list}
                                className="text-capitalize"
                              >
                                {div?.division_list}
                              </option>
                            ))}
                          </select>
                        </div>
                        <span className="text-danger">
                          {" "}
                          {errors.division && "Please enter division"}
                        </span>
                      </div>
                      <div className="form-group fill__up-form-group col-md-6">
                        <div className="input-group fill__up-input-group">
                          <div className="input-group-prepend fill__up-input-group-prepend">
                            <span
                              className="input-group-text fill__up-input-group-text"
                              id="basic-addon1"
                            >
                              <svg
                                width={17}
                                height={5}
                                viewBox="0 0 17 5"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                              >
                                <path
                                  d="M2.95077 0.142578C1.85958 0.142578 0.966797 1.06151 0.966797 2.18466C0.966797 3.3078 1.85958 4.22674 2.95077 4.22674C4.04195 4.22674 4.93474 3.3078 4.93474 2.18466C4.93474 1.06151 4.04195 0.142578 2.95077 0.142578ZM14.8546 0.142578C13.7634 0.142578 12.8706 1.06151 12.8706 2.18466C12.8706 3.3078 13.7634 4.22674 14.8546 4.22674C15.9458 4.22674 16.8386 3.3078 16.8386 2.18466C16.8386 1.06151 15.9458 0.142578 14.8546 0.142578ZM8.90268 0.142578C7.8115 0.142578 6.91871 1.06151 6.91871 2.18466C6.91871 3.3078 7.8115 4.22674 8.90268 4.22674C9.99387 4.22674 10.8867 3.3078 10.8867 2.18466C10.8867 1.06151 9.99387 0.142578 8.90268 0.142578Z"
                                  fill="#FFD602"
                                />
                              </svg>
                            </span>
                          </div>
                          <input
                            type="text"
                            className="form-control fill__up-input-form-control"
                            placeholder="City"
                            name="city"
                            ref={register({ required: true })}
                          />
                        </div>
                        <span className="text-danger">
                          {" "}
                          {errors.city && "Please enter city"}
                        </span>
                      </div>
                      <div className="form-group fill__up-form-group col-md-12">
                        <div className="input-group fill__up-input-group">
                          <div className="input-group-prepend fill__up-input-group-prepend">
                            <span
                              className="input-group-text fill__up-input-group-text"
                              id="basic-addon1"
                            >
                              <svg
                                width={23}
                                height={22}
                                viewBox="0 0 23 22"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                              >
                                <path
                                  d="M20.0599 9.83655C19.6191 5.84058 16.4473 2.66873 12.4513 2.22793V0.253906H10.5348V2.22793C6.53883 2.66873 3.36697 5.84058 2.92617 9.83655H0.952148V11.7531H2.92617C3.36697 15.749 6.53883 18.9209 10.5348 19.3617V21.3357H12.4513V19.3617C16.4473 18.9209 19.6191 15.749 20.0599 11.7531H22.034V9.83655H20.0599ZM11.4931 17.5027C7.78457 17.5027 4.7852 14.5033 4.7852 10.7948C4.7852 7.08633 7.78457 4.08696 11.4931 4.08696C15.2015 4.08696 18.2009 7.08633 18.2009 10.7948C18.2009 14.5033 15.2015 17.5027 11.4931 17.5027Z"
                                  fill="#FFD602"
                                />
                              </svg>
                            </span>
                          </div>
                          <input
                            type="text"
                            className="form-control fill__up-input-form-control"
                            placeholder="Address"
                            name="address"
                            ref={register({ required: true })}
                          />
                        </div>
                        <span className="text-danger">
                          {" "}
                          {errors.address && "Please enter address"}
                        </span>
                      </div>
                    </div>
                    {/* <div className="form-group fill__up-form-group">
                      <div className="checkbox_customized">
                        <div className="custom-control custom-checkbox">
                          <input
                            type="checkbox"
                            className="custom-control-input"
                            id="checkboxSignIN"
                            name="aggree"
                            ref={register({ required: true })}
                          />
                          <label
                            className="custom-control-label"
                            htmlFor="checkboxSignIN"
                          >
                            I Agree with Terms of Service
                          </label>
                        </div>
                      </div>
                    </div> */}
                    <div className="bottom__submit--btn--part">
                      <button
                        type="submit"
                        className="btn form__submit--btn"
                        disabled={loading}
                      >
                        <Loader
                          type="ThreeDots"
                          color="#181716"
                          height={30}
                          width={60}
                          visible={loading}
                        />

                        {!loading && "i am interested"}
                      </button>
                      {/* <p>
                              Already have an Account?{" "}
                              <span>
                                <a href="#!">Log In</a>
                              </span>
                            </p> */}
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
