import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const OurValueAgentCarousel = ({ data, isEn }) => {
  var settings = {
    dots: false,
    infinite: true,
    autoplay: true,
    speed: 500,
    slidesToShow: 2,
    slidesToScroll: 1,
    initialSlide: 0,
    arrows: false,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
        },
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
        },
      },
    ],
  };
  return (
    <div>
      <div className="ourvalue-agent--slider">
        <Slider {...settings}>
          {data?.map((slider, index) => {
            return (
              <div key={index}>
                <div className="plf-pd">
                  <div className="review__card">
                    <div className="media review__card-media">
                      <picture>
                        <source
                          media="(min-width:671px)"
                          srcSet={process.env.IMAGE_URL + slider.image}
                        />
                        <source
                          media="(max-width:670px)"
                          srcSet={process.env.IMAGE_URL + slider["image sd"]}
                        />
                        <img
                          className="align-self-center review__card--img mr-3"
                          src={process.env.IMAGE_URL + slider.image}
                          alt="Partner"
                          style={{
                            borderRadius: "50%",
                            width: "172px",
                            height: "172px",
                          }}
                        />
                      </picture>
                      <div className="media-body review__card-media-body">
                        <h5 className="mt-0">
                          <svg
                            width={47}
                            height={35}
                            viewBox="0 0 47 35"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              d="M20.5261 25.5281C20.5261 20.9076 17.4938 16.6337 12.129 16.6337C10.4963 16.6337 9.2134 17.0957 8.16377 17.6733C8.7469 5.89109 20.5261 8.54785 20.5261 3.46535C20.5261 1.61716 19.1265 0 16.5608 0C12.9454 0 7.69727 3.34983 5.94789 4.96699C2.33251 8.66336 0 12.5908 0 20.4455C0 28.3003 3.38213 35 10.9628 35C16.2109 35 20.5261 30.7261 20.5261 25.5281ZM47 25.5281C47 20.9076 43.9677 16.6337 38.603 16.6337C36.9702 16.6337 35.6873 17.0957 34.6377 17.6733C35.2208 5.89109 47 8.54785 47 3.46535C47 1.61716 45.6005 0 43.0347 0C39.4194 0 34.1712 3.34983 32.4218 4.96699C28.8064 8.66336 26.4739 12.5908 26.4739 20.4455C26.4739 28.3003 29.8561 35 37.4367 35C42.6849 35 47 30.7261 47 25.5281Z"
                              fill="#FFD602"
                            />
                          </svg>
                          {isEn ? slider.name : slider["name bn"]}
                        </h5>
                        <p
                          dangerouslySetInnerHTML={{
                            __html: isEn ? slider.desc : slider["desc bn"],
                          }}
                        ></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            );
          })}
        </Slider>
      </div>
    </div>
  );
};

export default OurValueAgentCarousel;
