import React, { useEffect } from "react";
import { Props } from "../interfaces/layout";
import Footer from "./Footer";
import Header from "./Header";
import { ToastContainer } from "react-toastify";

const Layout: React.FC<Props> = ({ children }): JSX.Element => {
  // useEffect(() => {
  //   if (typeof window !== "undefined") {
  //     let iframe: HTMLIFrameElement = document.createElement("iframe");
  //     iframe.setAttribute(
  //       "src",
  //       "https://webchat.zerocium.com/chat/web?webId=ce8edd35-41b1-4fde-b7b8-78f4c8cbbd2e"
  //     );
  //     iframe.style.position = "fixed";
  //     iframe.frameBorder = "0";
  //     iframe.style.height = "700px";
  //     iframe.style.width = "380px";
  //     iframe.style.right = "20px";
  //     iframe.style.bottom = "0px";
  //     document.body.appendChild(iframe);
  //   }
  // }, []);
  return (
    <>
      <Header />
      {children}
      <Footer />
      <ToastContainer />
    </>
  );
};

export default Layout;
