import { Map, InfoWindow, Marker, GoogleApiWrapper } from "google-maps-react";
import React, { Component } from "react";

export class MapContainer extends Component {
  state = {
    activeMarker: {},
    selectedPlace: {},
    showingInfoWindow: false,
    selectedLoc: this.props.selected,
  };

  onMarkerClick = (props, marker) => {
    // console.log("clicked", props, marker, this.state);
    this.setState({
      activeMarker: marker,
      selectedPlace: props,
      showingInfoWindow: true,
    });
  };
  onInfoWindowClose = () =>
    this.setState({
      activeMarker: null,
      showingInfoWindow: false,
    });

  onMapClicked = () => {
    if (this.state.showingInfoWindow)
      this.setState({
        activeMarker: null,
        showingInfoWindow: false,
      });
  };

  componentDidMount() {
    this.props.onRef(this);
  }

  componentWillUnmount() {
    this.props.onRef(undefined);
  }

  handle(id) {
    this.onMarkerClick(
      this.refs[`marker${id}`]?.props,
      this.refs[`marker${id}`]?.marker
    );
  }

  render() {
    return (
      <Map
        initialCenter={{
          lat: this.props.data[0].lat,
          lng: this.props.data[0].lng,
        }}
        center={{
          lat: this.props.data[this.state.selectedLoc].lat,
          lng: this.props.data[this.state.selectedLoc].lng,
        }}
        zoom={5}
        google={this.props.google}
        onClick={this.onMapClicked}
        style={{
          height: "100%",
          width: "100%",
        }}
        containerStyle={{
          position: "relative",
          width: "100%",
          height: "563px",
        }}
      >
        {this.props.data.map((marker, index) => {
          return (
            <Marker
              ref={`marker${index}`}
              name={`<div>
                        <h6 class="vid-text text-center">${marker.title}</h6>${marker.location}
                      </div>`}
              onClick={this.onMarkerClick}
              position={{ lat: marker.lat, lng: marker.lng }}
              key={index}
            />
          );
        })}

        <InfoWindow
          marker={this.state.activeMarker}
          onClose={this.onInfoWindowClose}
          visible={this.state.showingInfoWindow}
        >
          <div>
            <p
              className="font-weight-bold"
              dangerouslySetInnerHTML={{
                __html: this.state.selectedPlace.name,
              }}
            ></p>
          </div>
        </InfoWindow>
      </Map>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: process.env.MAP_KEY,
})(MapContainer);
