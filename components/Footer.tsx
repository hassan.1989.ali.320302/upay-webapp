import React, { useState, useEffect } from "react";
import Link from "next/link";
import { getFooter } from "../server";
import { Locales } from "./Header";

const Footer = (): JSX.Element => {
  const [data, setData] = useState(null);
  const [isEn, setIsEn] = useState<boolean>(true);

  useEffect(() => {
    if (typeof window !== "undefined") {
      +localStorage.getItem("locale") === Locales.English && setIsEn(false);

      window.addEventListener("locale", handleLocaleEvent, false);

      return () => {
        window.removeEventListener("locale", handleLocaleEvent);
      };
    }
  }, []);

  const handleLocaleEvent = () => {
    +localStorage.getItem("locale") === Locales.English
      ? setIsEn(false)
      : setIsEn(true);
  };

  useEffect(() => {
    fetchData();
  }, []);
  const fetchData = async () => {
    try {
      const res = await getFooter();
      setData(res.data.rows.Data[0]);
    } catch (error) {
      // console.log(error);
    }
  };

  return (
    <div>
      <div className="footer__main-sec">
        <div className="container">
          <div className="row">
            <div className="col-xl-12">
              <div className="footers__item-top-part">
                <div className="part__one">
                  <div className="logo_sec">
                    <picture>
                      <source
                        media="(min-width:650px)"
                        srcSet={process.env.IMAGE_URL + data?.["upay logo"]}
                      />
                      <source
                        media="(max-width:649px)"
                        srcSet={process.env.IMAGE_URL + data?.["upay logo sd"]}
                      />
                      <img
                        src={process.env.IMAGE_URL + data?.["upay logo"]}
                        alt="App of Ucbl"
                        style={{
                          width: "95px",
                          height: "95px",
                        }}
                      />
                    </picture>
                  </div>
                  <p
                    dangerouslySetInnerHTML={{
                      __html: isEn ? data?.desc : data?.["desc bn"],
                    }}
                  ></p>

                  <div className="bt__social--icons">
                    <h3>STAY CONNECTED</h3>
                    <div className="social__icon">
                      {data?.["Stay Connected"].map((item, index) => {
                        return (
                          <a href={item?.url} key={index}>
                            <picture>
                              <source
                                media="(min-width:650px)"
                                srcSet={process.env.IMAGE_URL + item?.icon}
                              />
                              <source
                                media="(max-width:649px)"
                                srcSet={
                                  process.env.IMAGE_URL + item?.["icon sd"]
                                }
                              />
                              <img
                                src={process.env.IMAGE_URL + item?.icon}
                                alt="App of Ucbl"
                                className="stay-connected-icon"
                              />
                            </picture>
                          </a>
                        );
                      })}
                    </div>
                  </div>
                </div>

                <div className="part__two">
                  <h3>Get In Touch</h3>
                  <div className="bottom__list">
                    <ul>
                      {data?.["Get In Touch"].map((item, index) => {
                        return (
                          <li key={index}>
                            <picture>
                              <source
                                media="(min-width:650px)"
                                srcSet={process.env.IMAGE_URL + item?.icon}
                              />
                              <source
                                media="(max-width:649px)"
                                srcSet={
                                  process.env.IMAGE_URL + item?.["icon sd"]
                                }
                              />
                              <img
                                src={process.env.IMAGE_URL + item?.icon}
                                alt="App of Ucbl"
                                className="get-in-touch-icon"
                              />
                            </picture>
                            {" " + isEn ? item?.title : item?.["title bn"]}
                          </li>
                        );
                      })}
                    </ul>
                  </div>
                </div>

                <div className="part__three part__three-pc">
                  <h3>Useful Links</h3>
                  <div className="bottom__list">
                    <ul>
                      {data?.["Usefull Links"].map((link, index) => {
                        return (
                          <li key={index}>
                            <a href={link?.["slug url"]}>
                              {isEn ? link?.title : link?.["title bn"]}
                            </a>
                          </li>
                        );
                      })}
                    </ul>
                  </div>
                </div>

                <div className="part__four part__four-pc">
                  <h3>Company Info</h3>
                  <div className="bottom__list">
                    <ul>
                      {data?.["Company Info"].map((link, index) => {
                        return (
                          <li key={index}>
                            <a href={link?.["slug url"]}>
                              {isEn ? link?.title : link?.["title bn"]}
                            </a>
                          </li>
                        );
                      })}
                    </ul>
                  </div>
                </div>

                <div className="mobile__f__two--part">
                  <div className="part__three">
                    <h3>Useful Links</h3>
                    <div className="bottom__list">
                      <ul>
                        {data?.["Usefull Links"].map((link, index) => {
                          return (
                            <li key={index}>
                              <a href={link?.["slug url"]}>
                                {isEn ? link?.title : link?.["title bn"]}
                              </a>
                            </li>
                          );
                        })}
                      </ul>
                    </div>
                  </div>

                  <div className="part__four">
                    <h3>Company Info</h3>
                    <div className="bottom__list">
                      <ul>
                        {data?.["Company Info"].map((link, index) => {
                          return (
                            <li key={index}>
                              <a href={link?.["slug url"]}>
                                {isEn ? link?.title : link?.["title bn"]}
                              </a>
                            </li>
                          );
                        })}
                      </ul>
                    </div>
                  </div>
                </div>

                <div className="part__five">
                  <h3>STAY CONNECTED</h3>
                  <div className="bottom__part">
                    <div className="social__icon">
                      {data?.["Stay Connected"].map((item, index) => {
                        return (
                          <a href={item?.url} key={index}>
                            <picture>
                              <source
                                media="(min-width:650px)"
                                srcSet={process.env.IMAGE_URL + item?.icon}
                              />
                              <source
                                media="(max-width:649px)"
                                srcSet={
                                  process.env.IMAGE_URL + item?.["icon sd"]
                                }
                              />
                              <img
                                src={process.env.IMAGE_URL + item?.icon}
                                alt="App of Ucbl"
                                className="stay-connected-icon"
                              />
                            </picture>
                          </a>
                        );
                      })}
                    </div>

                    <div className="store__links text-center">
                      {data?.["Get app"].map((item, index) => {
                        return (
                          <div
                            className={
                              index % 2 === 0
                                ? "g__play-store-part"
                                : "apple-store-part"
                            }
                            key={index}
                          >
                            <a href={item?.url} target="_blank">
                              <picture>
                                <source
                                  media="(min-width:650px)"
                                  srcSet={process.env.IMAGE_URL + item?.icon}
                                />
                                <source
                                  media="(max-width:649px)"
                                  srcSet={
                                    process.env.IMAGE_URL + item?.["icon sd"]
                                  }
                                />
                                <img
                                  src={process.env.IMAGE_URL + item?.icon}
                                  alt="App of Ucbl"
                                  className="app--icon"
                                />
                              </picture>
                            </a>
                          </div>
                        );
                      })}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* <div className="footer__secondary--sec">
        <p>
          <a href="#!">view on mobile</a>
        </p>
      </div> */}
    </div>
  );
};

export default Footer;
