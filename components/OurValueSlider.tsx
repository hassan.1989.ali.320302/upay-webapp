import React, { Component } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { Value } from "../interfaces/value";

type Props = {
  values: [];
  isEn: boolean;
};

class OurValueSlider extends Component<Props> {
  slider: any;
  constructor(props) {
    super(props);
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
  }
  next() {
    this.slider.slickNext();
  }
  previous() {
    this.slider.slickPrev();
  }

  render() {
    const settings = {
      dots: false,
      infinite: true,
      speed: 400,
      slidesToShow: 1,
      slidesToScroll: 1,
      // dots: false,
      centerMode: false,
      arrows: false,
      autoplay: true,
    };
    return (
      <div>
        <div className="our__value__slide">
          <Slider ref={(c) => (this.slider = c)} {...settings}>
            {this.props.values.map((value: Value, key: number) => {
              return (
                <div key={key}>
                  <div className="our__value--details">
                    <div className="row">
                      <div className="col-xl-7 col-lg-7">
                        <div className="left__part">
                          <picture>
                            <source
                              media="(max-width: 767px)"
                              srcSet={
                                process.env.IMAGE_URL + value?.["image sd"]
                              }
                            />
                            <source
                              media="(min-width: 768px)"
                              srcSet={process.env.IMAGE_URL + value.image}
                            />
                            <img
                              src={process.env.IMAGE_URL + value?.["image sd"]}
                              alt="sliders"
                            />
                          </picture>
                        </div>
                      </div>
                      <div
                        className="col-xl-5 col-lg-5"
                        style={{ alignSelf: "center" }}
                      >
                        <div className="right__part">
                          <h4>
                            {this.props.isEn ? value.title : value["title bn"]}
                          </h4>
                          <p
                            dangerouslySetInnerHTML={{
                              __html: this.props.isEn
                                ? value.desc
                                : value["desc bn"],
                            }}
                          ></p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              );
            })}
          </Slider>
          <div className="arrow__btn--parts">
            <button className="button prev__btn" onClick={this.previous}>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="35"
                height="35"
                viewBox="0 0 35 35"
                fill="none"
              >
                <path
                  d="M21.4676 33.3699L5.74436 18.7633L21.4676 4.15671"
                  stroke="#4E4E50"
                  strokeWidth="3"
                />
              </svg>
            </button>
            <button className="button next__btn" onClick={this.next}>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="35"
                height="35"
                viewBox="0 0 35 35"
                fill="none"
              >
                <path
                  d="M13.5324 33.3699L29.2556 18.7633L13.5324 4.15671"
                  stroke="#4E4E50"
                  strokeWidth="3"
                />
              </svg>
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default OurValueSlider;
