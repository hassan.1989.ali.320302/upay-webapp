import React from "react";
import Link from "next/link";
import Other from "../translations/otherService.json";
import { useRouter } from "next/router";
import Flip from "react-reveal/Flip";
import Fade from "react-reveal/Fade";

const OtherServices = ({ isEn, services }): JSX.Element => {
  const { query } = useRouter();

  return (
    <div>
      <div className="other_services">
        <div className="container">
          <div className="row">
            <Fade bottom cascade>
              <div className="col-xl-12">
                <h1
                  dangerouslySetInnerHTML={{
                    __html: isEn ? Other.en : Other.bn,
                  }}
                ></h1>
                <div className="other_services-lists">
                  {/* <Flip top cascade> */}
                  <ul className="list-inline">
                    {services?.map((service, key) => {
                      const url = service["slug url"].split("/")[
                        service["slug url"].split("/").length - 1
                      ];
                      return (
                        <li
                          className={`list-inline-item ${
                            query?.slug === url && "active"
                          }`}
                          key={key}
                        >
                          <Link href={url}>
                            <a
                              className="list-inline-link overflow-auto"
                              dangerouslySetInnerHTML={{
                                __html: isEn
                                  ? service.title
                                  : service["title bn"],
                              }}
                            ></a>
                          </Link>
                        </li>
                      );
                    })}
                  </ul>
                  {/* </Flip> */}
                </div>
              </div>
            </Fade>
          </div>
        </div>
      </div>
    </div>
  );
};

export default OtherServices;
