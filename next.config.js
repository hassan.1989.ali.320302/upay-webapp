module.exports = {
  reactStrictMode: true,
  images: {
    domains: ["api.upaybd.com"],
  },
  env: {
    IMAGE_URL: "https://api.upaybd.com",
    MAP_KEY: "",
    FB_PXL_ID: 1236650333348175,
  },
  distDir: "build",
};
