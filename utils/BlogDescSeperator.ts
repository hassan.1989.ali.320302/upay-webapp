export const descSeperator = (content: { desc }, lang = "en") => {
  let first: string;
  let last: string;
  if (lang === "bn") {
    let arr: [] = content["desc bn"].split(".");
    let len: number = arr.length;
    first = arr.slice(0, Math.round(len / 2)).join(".");
    last = arr.slice(Math.round(len / 2), len).join(".");
  } else {
    let arr: [] = content.desc.split(".");
    let len: number = arr.length;
    first = arr.slice(0, Math.round(len / 2)).join(".");
    last = arr.slice(Math.round(len / 2), len).join(".");
  }
  return {
    first,
    last,
  };
};
