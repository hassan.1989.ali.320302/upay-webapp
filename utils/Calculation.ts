export const calculate = (
  data: [],
  service: string,
  transaction: string,
  amount: number
) => {
  let charge: number;
  let total: number;
  let selection: object[] = data.filter(
    (it) =>
      it["service_name"] === service && it["transaction_type"] === transaction
  );

  if (selection.length > 1) {
    let filter_main_data = selection.filter((it) => {
      let max =
        it["max_charge"] === "" ? Number.MAX_SAFE_INTEGER : +it["max_charge"];
      return amount >= +it["min_charge"] && amount <= max;
    })[0];
    charge = Number(filter_main_data["charge"]);
    total = amount + charge;
  } else {
    let main_data = selection[0];

    if (main_data["calculation_type"] === "BDT") {
      charge = Number(main_data["charge"]);
      total = amount + charge;
    } else if (main_data["calculation_type"] === "%") {
      charge = +((amount * Number(main_data["charge"])) / 100.0).toFixed(2);
      total = amount + charge;
    } else if (main_data["calculation_type"].includes("%+")) {
      let bdtCharge = main_data["calculation_type"].slice(2);
      charge =
        +((amount * Number(main_data["charge"])) / 100.0).toFixed(2) +
        Number(bdtCharge);
      total = amount + charge;
    } else {
      charge = 0;
      total = amount;
    }
  }
  return {
    charge,
    total,
  };
};

export const noChargeCheck = (
  data: [],
  service: string,
  transaction: string
) => {
  let selection: object[] = data.filter(
    (it) =>
      it["service_name"] === service && it["transaction_type"] === transaction
  )[0];

  if (selection?.["charge"] === "No charge") {
    // console.log("fasdfa");
    return true;
  } else return false;
};
