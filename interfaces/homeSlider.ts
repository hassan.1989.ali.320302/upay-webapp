export interface HomeSlider {
  title: string;
  titlebn: string;
  image: string;
  imagesd: string;
}
