export interface Value {
  title: string;
  ["title bn"]: string;
  desc: string;
  ["desc bn"]: string;
  image: string;
  ["image sd"]: string;
}
