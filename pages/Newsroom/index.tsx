import React, { useEffect, useState } from "react";
import Banner from "../../components/Banner";
import { GetStaticProps, InferGetStaticPropsType } from "next";
import Link from "next/link";
import LetUsHelp from "../../components/LetUsHelp";
import Fade from "react-reveal/Fade";
import { Locales } from "../../components/Header";
import { getNewsRoom } from "../../server";
import ModalVideo from "react-modal-video";
import ButtonLocale from "../../translations/button.json";

export const getStaticProps: GetStaticProps = async (context) => {
  let page: {
    data: object;
    error: string;
  };
  try {
    const res = await getNewsRoom();
    // console.log("resss", res);
    page = {
      data: res.data,
      error: "",
    };
  } catch (err) {
    page = {
      data: {},
      error: "Something Went Wrong!",
    };
  }

  return {
    props: {
      page,
    },
    revalidate: 1,
  };
};

const Newsroom = ({
  page: { data, error },
}: InferGetStaticPropsType<typeof getStaticProps>): JSX.Element => {
  const pageData = data?.rows?.Data[0];
  const commercials = pageData?.["segment one"][0];
  const externals = pageData?.["Segment two"][0];

  const [isEn, setIsEn] = useState<boolean>(true);
  const [videoId, setVideoId] = useState<String>("L61p2uyiMSo");
  const [isOpen, setOpen] = useState(false);

  const [currPage, setCurrPage] = useState<number>(1);
  const [totalPage, setTotalPage] = useState<number>(1);

  useEffect(() => {
    if (typeof window !== "undefined") {
      +localStorage.getItem("locale") === Locales.English && setIsEn(false);

      window.addEventListener("locale", handleLocaleEvent, false);

      return () => {
        window.removeEventListener("locale", handleLocaleEvent);
      };
    }
  }, []);

  useEffect(() => {
    if (externals) {
      setTotalPage(Math.ceil(externals?.["Under Segment two"].length / 6));
    }
  }, [externals]);

  const handleLocaleEvent = () => {
    +localStorage.getItem("locale") === Locales.English
      ? setIsEn(false)
      : setIsEn(true);
  };

  const pagination = () => {
    let pages = [];
    for (let i = 1; i <= totalPage; i++) {
      pages.push(
        <li
          className={`list-inline-item ${currPage === i && "active"}`}
          style={{ cursor: "pointer" }}
          key={i}
        >
          <a onClick={() => setCurrPage(i)} className="list-inline-link">
            {i}
          </a>
        </li>
      );
    }
    return pages;
  };

  if (error) {
    return <div>{error}</div>;
  } else
    return (
      <div>
        <Banner
          name={isEn ? pageData.title : pageData["title bn"]}
          cover={{
            image: pageData["cover image"],
            imagesd: pageData["cover image sd"],
          }}
        />

        <div className="newsroom__secMain tb__padding">
          <div className="container">
            <div className="row">
              <div className="col-xl-12">
                <div className="top__parts mb-5">
                  <div className="dashleft__header--part">
                    <Fade left>
                      <h1
                        dangerouslySetInnerHTML={{
                          __html: isEn
                            ? commercials?.title
                            : commercials?.["title bn"],
                        }}
                      ></h1>
                    </Fade>
                  </div>
                  <div className="card__sectionMain">
                    <div className="row justify-content-center">
                      {commercials?.["Under segment one"].map(
                        (commercial, index) => {
                          const url = commercial.url;
                          const vId = url.slice(url.lastIndexOf("=") + 1);
                          // console.log(vId);
                          return (
                            <div
                              className="col-xl-4 col-lg-4 col-md-6"
                              key={index}
                            >
                              <Fade bottom>
                                <div className="commercial__primary--cardMain">
                                  <div className="card tutorial__large-card">
                                    <div className="top__video--part">
                                      <picture>
                                        <source
                                          media="(min-width:650px)"
                                          srcSet={
                                            process.env.IMAGE_URL +
                                            commercial.image
                                          }
                                        />
                                        <source
                                          media="(max-width:649px)"
                                          srcSet={
                                            process.env.IMAGE_URL +
                                            commercial["image sd"]
                                          }
                                        />
                                        <img
                                          src={
                                            process.env.IMAGE_URL +
                                            commercial.image
                                          }
                                          className="card-img-top"
                                          alt="Media in Ucbl"
                                          // style={{
                                          //   width: "40%",
                                          //   objectFit: "cover",
                                          // }}
                                        />
                                      </picture>

                                      <a
                                        className="play__icon"
                                        onClick={() => {
                                          setOpen(true);
                                          setVideoId(vId);
                                        }}
                                      >
                                        <img
                                          src="/images/video-icon.svg"
                                          alt=""
                                        />
                                      </a>
                                    </div>
                                    <div className="card-body newsRoom--card-body">
                                      <h5
                                        className="card-title"
                                        dangerouslySetInnerHTML={{
                                          __html: isEn
                                            ? commercial?.title
                                            : commercial?.["title bn"],
                                        }}
                                      ></h5>
                                      {/* <Link href="/newsroom/details">
                                <a
                                  href="#"
                                  className="mediaCenter__card__readBtn"
                                >
                                  <span>Read More</span>
                                  <svg
                                    width={28}
                                    height={24}
                                    viewBox="0 0 28 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                  >
                                    <path
                                      d="M18.0709 11H4.64844V13H18.0709V16L22.5301 12L18.0709 8V11Z"
                                      fill="#4E4E50"
                                    />
                                  </svg>
                                </a>
                              </Link> */}
                                    </div>
                                  </div>
                                </div>
                              </Fade>
                            </div>
                          );
                        }
                      )}
                    </div>
                  </div>
                </div>

                <div className="bottom__parts">
                  <div className="dashleft__header--part">
                    <Fade left>
                      <h1
                        dangerouslySetInnerHTML={{
                          __html: isEn
                            ? externals?.title
                            : externals?.["title bn"],
                        }}
                      ></h1>
                    </Fade>
                  </div>
                  <div className="card__sectionMain">
                    <div className="row justify-content-center">
                      {externals?.["Under Segment two"]
                        .slice(currPage * 6 - 6, currPage * 6)
                        .map((external, index) => {
                          return (
                            <div
                              className="col-xl-4 col-lg-4 col-md-6"
                              style={{ display: "flex", flex: "1" }}
                              key={index}
                            >
                              <Fade bottom>
                                <div className="card media__center--cardMain">
                                  <picture>
                                    <source
                                      media="(min-width:650px)"
                                      srcSet={
                                        process.env.IMAGE_URL + external.image
                                      }
                                    />
                                    <source
                                      media="(max-width:649px)"
                                      srcSet={
                                        process.env.IMAGE_URL +
                                        external["image bn"]
                                      }
                                    />
                                    <img
                                      src={
                                        process.env.IMAGE_URL + external.image
                                      }
                                      className="card-img-top"
                                      alt="Media in Ucbl"
                                      style={{
                                        maxHeight: "152px",
                                        objectFit: "fill",
                                      }}
                                    />
                                  </picture>
                                  <div className="card-body newsRoom--card-body">
                                    <h5
                                      className="card-title"
                                      dangerouslySetInnerHTML={{
                                        __html: isEn
                                          ? external?.title
                                          : external?.["title bn"],
                                      }}
                                    ></h5>
                                    <a
                                      href={external?.url}
                                      target="_blank"
                                      className="mediaCenter__card__readBtn"
                                    >
                                      <span>
                                        {isEn
                                          ? ButtonLocale.read_more.en
                                          : ButtonLocale.read_more.bn}
                                      </span>
                                      <svg
                                        width={28}
                                        height={24}
                                        viewBox="0 0 28 24"
                                        fill="none"
                                        xmlns="http://www.w3.org/2000/svg"
                                      >
                                        <path
                                          d="M18.0709 11H4.64844V13H18.0709V16L22.5301 12L18.0709 8V11Z"
                                          fill="#4E4E50"
                                        />
                                      </svg>
                                    </a>
                                  </div>
                                </div>
                              </Fade>
                            </div>
                          );
                        })}
                    </div>
                  </div>
                </div>

                {externals?.["Under Segment two"].length > 0 && (
                  <div className="pagination__part">
                    <Fade bottom>
                      <div className="row">
                        <div className="col-xl-8 mx-auto">
                          <div className="pagination__wrap">
                            <div className="prev__btn--part">
                              <a
                                onClick={() => {
                                  if (currPage > 1) setCurrPage(currPage - 1);
                                }}
                                className="prev__btn"
                                style={{ cursor: "pointer" }}
                              >
                                <svg
                                  width="24"
                                  height="8"
                                  viewBox="0 0 24 8"
                                  fill="none"
                                  xmlns="http://www.w3.org/2000/svg"
                                >
                                  <path
                                    d="M6.51294 5.27583L23.8125 5.27583V3.53264L6.51294 3.53264V0.917844L0.765623 4.40423L6.51294 7.89062V5.27583Z"
                                    fill="#0054A6"
                                  />
                                </svg>{" "}
                                {isEn
                                  ? ButtonLocale.previous.en
                                  : ButtonLocale.previous.bn}
                              </a>
                            </div>

                            <div className="pagination__number">
                              <ul className="list-inline">
                                {pagination().map((item, index) => {
                                  return item;
                                })}
                              </ul>
                            </div>

                            <div className="next__btn--part">
                              <a
                                onClick={() => {
                                  if (currPage < totalPage)
                                    setCurrPage(currPage + 1);
                                }}
                                className="next__btn"
                                style={{ cursor: "pointer" }}
                              >
                                {isEn
                                  ? ButtonLocale.next.en
                                  : ButtonLocale.next.bn}{" "}
                                <svg
                                  width="24"
                                  height="8"
                                  viewBox="0 0 24 8"
                                  fill="none"
                                  xmlns="http://www.w3.org/2000/svg"
                                >
                                  <path
                                    d="M17.4304 3.53276H0.130859V5.27596H17.4304V7.89075L23.1777 4.40436L17.4304 0.917969V3.53276Z"
                                    fill="#0054A6"
                                  />
                                </svg>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </Fade>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>

        <LetUsHelp isEn={isEn} />

        <ModalVideo
          channel="youtube"
          autoplay={1}
          isOpen={isOpen}
          videoId={videoId}
          onClose={() => setOpen(false)}
        />
      </div>
    );
};

export default Newsroom;
