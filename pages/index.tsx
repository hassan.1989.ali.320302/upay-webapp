import React, { useEffect, useState } from "react";
import { GetStaticProps, InferGetStaticPropsType } from "next";
import { Locales } from "../components/Header";
import Fade from "react-reveal/Fade";
import HomeData from "../translations/home.json";
import ButtonText from "../translations/button.json";
import LetUsHelpText from "../translations/letushelp.json";
import Link from "next/link";

import MainSlider from "../components/MainSlider";
import LetsHelpSlider from "../components/LetsHelpSlider";
import { getHomePage, getHomeSlider } from "../server";
import LetUsHelp from "../components/LetUsHelp";

export const getStaticProps: GetStaticProps = async (context) => {
  let page: {
    data: object;
    error: string;
  };
  try {
    const res = await getHomePage();
    const resSlider = await getHomeSlider();

    page = {
      data: {
        home: await res.data,
        slider: await resSlider.data,
      },
      error: "",
    };
  } catch (err) {
    page = {
      data: {},
      error: "Something Went Wrong!",
    };
  }

  return {
    props: {
      page,
    },
    revalidate: 1,
  };
};

const Home: React.FC = ({
  page: { data, error },
}: InferGetStaticPropsType<typeof getStaticProps>): JSX.Element => {
  const [isEn, setIsEn] = useState<boolean>(true);
  const [servicePage, setServicePage] = useState(6);

  useEffect(() => {
    if (typeof window !== "undefined") {
      +localStorage.getItem("locale") === Locales.English && setIsEn(false);

      window.addEventListener("locale", handleLocaleEvent, false);

      return () => {
        window.removeEventListener("locale", handleLocaleEvent);
      };
    }
  }, []);

  const handleLocaleEvent = () => {
    +localStorage.getItem("locale") === Locales.English
      ? setIsEn(false)
      : setIsEn(true);
  };

  const pageData = data?.home?.rows?.Data[0];
  const serviceData = pageData.services[0];
  const findUsData = pageData["find us"][0];

  if (error) {
    <div>
      <div className="mt-5">{error}</div>
    </div>;
  } else
    return (
      <div>
        <MainSlider
          sliders={data.slider.rows["Data"]}
          isEn={isEn}
          isHome={true}
        />
        <section>
          <div className="why__choose__upay--section top__margin">
            <div className="container">
              <div className="row">
                <Fade left>
                  <div className="col-xl-6 col-lg-5">
                    <div className="left__part">
                      <div className="dashleft__header--part desktop__view">
                        <h1
                          dangerouslySetInnerHTML={{
                            __html: isEn
                              ? pageData["segment one title"][0].titile
                              : pageData["segment one title"][0]["titile bn"],
                          }}
                        ></h1>
                        <p>
                          {isEn
                            ? pageData["segment one title"][0].desc
                            : pageData["segment one title"][0]["desc bn"]}
                        </p>
                      </div>
                      <div className="dashmid__wDesc__header--part mobile__view">
                        <h1
                          dangerouslySetInnerHTML={{
                            __html: isEn
                              ? pageData["segment one title"][0].titile
                              : pageData["segment one title"][0]["titile bn"],
                          }}
                        ></h1>
                        <p>
                          {isEn
                            ? pageData["segment one title"][0].desc
                            : pageData["segment one title"][0]["desc bn"]}
                        </p>
                      </div>
                      <div className="mt-5">
                        <video width="100%" height="auto" autoPlay muted loop>
                          <source
                            src={
                              process.env.IMAGE_URL +
                              pageData["animated video"][0].animation
                            }
                            type="video/mp4"
                          />
                        </video>
                      </div>
                    </div>
                  </div>
                </Fade>
                <Fade right>
                  <div className="col-xl-6 col-lg-7 why__choose--rightCol">
                    <div className="right__part">
                      <div className="right__part--cardSec">
                        <div className="row justify-content-center">
                          {pageData.icon.map((item, key) => {
                            return (
                              <div
                                className="col-xl-4 col-lg-4 right__part__custom--col"
                                key={key}
                              >
                                <div className="card why__choose--cardMain">
                                  <div className="top__main--sec">
                                    <picture>
                                      <source
                                        media="(min-width:650px)"
                                        srcSet={
                                          process.env.IMAGE_URL + item?.image
                                        }
                                      />
                                      <source
                                        media="(max-width:649px)"
                                        srcSet={
                                          process.env.IMAGE_URL +
                                          item?.["image sd"]
                                        }
                                      />
                                      <img
                                        src={
                                          process.env.IMAGE_URL + item?.image
                                        }
                                        alt="upay"
                                        // style={{
                                        //   maxWidth: "117px",
                                        //   maxHeight: "113px",
                                        //   objectFit: "fill",
                                        // }}
                                      />
                                    </picture>
                                    <p
                                      dangerouslySetInnerHTML={{
                                        __html: isEn
                                          ? item?.titile
                                          : item?.["title bn"],
                                      }}
                                    ></p>
                                  </div>
                                  <div className="card-body">
                                    <p
                                      className="card-text"
                                      dangerouslySetInnerHTML={{
                                        __html: isEn
                                          ? item?.desc
                                          : item?.["desc bn"],
                                      }}
                                    ></p>
                                    <div className="arr-icon">
                                      <img src="/images/angel.svg" alt="" />
                                    </div>
                                  </div>
                                </div>
                              </div>
                            );
                          })}
                        </div>
                      </div>

                      <div className="bottom__btn--part">
                        {pageData?.["Get app"].map((item, index) => {
                          return (
                            <a
                              href={item.url}
                              target="_blank"
                              key={index}
                              className={
                                index % 2 === 0
                                  ? "btn gStore__btn"
                                  : "btn iStore__btn"
                              }
                            >
                              <picture>
                                <source
                                  media="(min-width:650px)"
                                  srcSet={process.env.IMAGE_URL + item.icon}
                                />
                                <source
                                  media="(max-width:649px)"
                                  srcSet={
                                    process.env.IMAGE_URL + item["icon sd"]
                                  }
                                />
                                <img
                                  src={process.env.IMAGE_URL + item.icon}
                                  alt="upay"
                                  className="app--icon"
                                />
                              </picture>
                            </a>
                          );
                        })}
                      </div>
                    </div>
                  </div>
                </Fade>
              </div>
            </div>
          </div>
        </section>

        <section>
          <div className="our__service--mainSec tb__padding">
            {/* <div className="top__Left--img">
            <img src="/images/our__service--bg.png" alt="" />
          </div> */}
            <div className="container">
              <div className="row">
                <div className="col-xl-12">
                  <Fade bottom cascade>
                    <div className="dashmid__wDesc__header--part bottom__margin">
                      <h1
                        dangerouslySetInnerHTML={{
                          __html: isEn
                            ? serviceData.title
                            : serviceData["title bn"],
                        }}
                      ></h1>
                      <p
                        dangerouslySetInnerHTML={{
                          __html: isEn
                            ? serviceData.desc
                            : serviceData["desc bn"],
                        }}
                      ></p>
                    </div>
                  </Fade>
                  <Fade bottom cascade>
                    <div className="service__card--secMain bottom__margin">
                      <div
                        className="row"
                        style={{ display: "flex", justifyContent: "center" }}
                      >
                        {serviceData["service list"]
                          ?.slice(0, servicePage)
                          .map((service, key) => {
                            return (
                              <div
                                className="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-6 text-center"
                                key={key}
                              >
                                <Link href={service?.["slug url"]}>
                                  <a className="our__service--cardMain">
                                    <div className="img__part">
                                      <picture>
                                        <source
                                          media="(min-width:650px)"
                                          srcSet={
                                            process.env.IMAGE_URL + service.icon
                                          }
                                        />
                                        <source
                                          media="(max-width:649px)"
                                          srcSet={
                                            process.env.IMAGE_URL +
                                            service["icon sd"]
                                          }
                                        />
                                        <img
                                          src={
                                            process.env.IMAGE_URL + service.icon
                                          }
                                          alt="upay"
                                          className="our__service--logo"
                                        />
                                      </picture>
                                    </div>
                                    <p
                                      dangerouslySetInnerHTML={{
                                        __html: isEn
                                          ? service?.["title"]
                                          : service?.["title bn"],
                                      }}
                                    ></p>
                                  </a>
                                </Link>
                              </div>
                            );
                          })}
                      </div>
                    </div>
                  </Fade>
                  {serviceData["service list"]?.length > servicePage && (
                    <div className="bottom__btns--part">
                      <div className="right__arrow--btnPart">
                        <a
                          onClick={() => setServicePage(servicePage + 5)}
                          className="btn right__arrow--btn primary__bg--btn inline__blck--btn"
                        >
                          {isEn
                            ? ButtonText.view_more.en
                            : ButtonText.view_more.bn}
                        </a>
                      </div>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </section>

        <section>
          <div className="whereTo__find--sectionMain">
            <div className="container">
              <div className="row">
                <Fade left>
                  <div
                    className="col-xl-6 col-lg-6"
                    style={{ alignSelf: "center" }}
                  >
                    <div className="left__part">
                      <div className="dashleft__wDesc__header--part">
                        <h1
                          dangerouslySetInnerHTML={{
                            __html: isEn
                              ? findUsData.title
                              : findUsData["title bn"],
                          }}
                        ></h1>
                        <p
                          dangerouslySetInnerHTML={{
                            __html: isEn
                              ? findUsData.desc
                              : findUsData["desc bn"],
                          }}
                        ></p>
                      </div>
                      <div className="find__btn--part">
                        <Link href="/service-location">
                          <a className="btn find__upay--btn secondary__bg--btn">
                            <svg
                              width="36"
                              height="34"
                              viewBox="0 0 36 34"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <path
                                d="M25.2995 21.25H23.7551L23.2077 20.7437C25.1235 18.6063 26.2769 15.8313 26.2769 12.8125C26.2769 6.08125 20.5882 0.625 13.5701 0.625C6.55202 0.625 0.863281 6.08125 0.863281 12.8125C0.863281 19.5438 6.55202 25 13.5701 25C16.7175 25 19.6107 23.8938 21.8393 22.0563L22.3671 22.5813V24.0625L32.1416 33.4188L35.0544 30.625L25.2995 21.25ZM13.5701 21.25C8.70241 21.25 4.77307 17.4813 4.77307 12.8125C4.77307 8.14375 8.70241 4.375 13.5701 4.375C18.4378 4.375 22.3671 8.14375 22.3671 12.8125C22.3671 17.4813 18.4378 21.25 13.5701 21.25Z"
                                fill="#FFD602"
                              />
                            </svg>
                            {isEn
                              ? ButtonText.find_upay.en
                              : ButtonText.find_upay.bn}
                          </a>
                        </Link>
                      </div>
                    </div>
                  </div>
                </Fade>
                <Fade right>
                  <div className="col-xl-6 col-lg-6 ">
                    <div className="right__part">
                      <div className="img__part my-md-3 mbl-space">
                        <picture>
                          <source
                            media="(min-width:650px)"
                            srcSet={process.env.IMAGE_URL + findUsData.image}
                          />
                          <source
                            media="(max-width:649px)"
                            srcSet={
                              process.env.IMAGE_URL + findUsData["image sd"]
                            }
                          />
                          <img
                            src={process.env.IMAGE_URL + findUsData.image}
                            alt="Find Us"
                            style={{ width: "100%" }}
                          />
                        </picture>
                      </div>
                      <div className="find__btn--part">
                        <a
                          href="#!"
                          className="btn find__upay--btn secondary__bg--btn"
                        >
                          <svg
                            width="36"
                            height="34"
                            viewBox="0 0 36 34"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              d="M25.2995 21.25H23.7551L23.2077 20.7437C25.1235 18.6063 26.2769 15.8313 26.2769 12.8125C26.2769 6.08125 20.5882 0.625 13.5701 0.625C6.55202 0.625 0.863281 6.08125 0.863281 12.8125C0.863281 19.5438 6.55202 25 13.5701 25C16.7175 25 19.6107 23.8938 21.8393 22.0563L22.3671 22.5813V24.0625L32.1416 33.4188L35.0544 30.625L25.2995 21.25ZM13.5701 21.25C8.70241 21.25 4.77307 17.4813 4.77307 12.8125C4.77307 8.14375 8.70241 4.375 13.5701 4.375C18.4378 4.375 22.3671 8.14375 22.3671 12.8125C22.3671 17.4813 18.4378 21.25 13.5701 21.25Z"
                              fill="#FFD602"
                            />
                          </svg>
                          {isEn
                            ? ButtonText.find_upay.en
                            : ButtonText.find_upay.bn}
                        </a>
                      </div>
                    </div>
                  </div>
                </Fade>
              </div>
            </div>
          </div>
        </section>

        <LetUsHelp isEn={isEn} />
      </div>
    );
};

export default Home;
