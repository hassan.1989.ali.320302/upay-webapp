import React, { useEffect, useState } from "react";
import { GetStaticProps, InferGetStaticPropsType } from "next";
import { getInnerPage, getPayBillPage } from "../../../server";
import Banner from "../../../components/Banner";
import LetsHelpSlider from "../../../components/LetsHelpSlider";
import { Locales } from "../../../components/Header";
import GetApp from "../../../components/GetApp";
import Link from "next/link";
import Fade from "react-reveal/Fade";
import LetUsHelp from "../../../components/LetUsHelp";

export const getStaticPaths = async () => {
  return {
    paths: [{ params: { slug: "Pay-Bill" } }],
    fallback: true,
  };
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  let page: {
    data: object;
    error: string;
  };
  try {
    const res = await getInnerPage(params.slug);
    page = {
      data: res.data,
      error: "",
    };
  } catch (err) {
    page = {
      data: {},
      error: "Something Went Wrong!",
    };
  }

  return {
    props: {
      page,
    },
    revalidate: 1,
  };
};

const ProductsMain = ({
  page,
}: InferGetStaticPropsType<typeof getStaticProps>): JSX.Element => {
  const [isEn, setIsEn] = useState<boolean>(true);

  useEffect(() => {
    if (typeof window !== "undefined") {
      +localStorage.getItem("locale") === Locales.English && setIsEn(false);

      window.addEventListener("locale", handleLocaleEvent, false);

      return () => {
        window.removeEventListener("locale", handleLocaleEvent);
      };
    }
  }, []);

  const handleLocaleEvent = () => {
    +localStorage.getItem("locale") === Locales.English
      ? setIsEn(false)
      : setIsEn(true);
  };

  const paybillData = page?.data?.rows?.Data[0];
  const error = page?.error;

  if (error) {
    <div>
      <div className="mt-5">{error}</div>
    </div>;
  } else
    return (
      <div>
        <Banner
          name={
            isEn
              ? paybillData?.["cover title"]
              : paybillData?.["cover title bn"]
          }
          cover={{
            image: paybillData?.["background image"],
            imagesd: paybillData?.["background image sd"],
          }}
        />

        <section>
          <div className="paybill">
            <div className="container">
              <div className="row">
                <div className="col-xl-12">
                  <Fade bottom cascade>
                    <div className="paybill__items">
                      <p
                        className="paybill__items-text"
                        dangerouslySetInnerHTML={{
                          __html: isEn
                            ? paybillData?.["desc"]
                            : paybillData?.["desc bn"],
                        }}
                      ></p>
                      <div className="paybill__items-links">
                        <div className="row d-flex justify-content-center">
                          {paybillData?.methods.map((item, key) => {
                            return (
                              <div
                                className="col-xl-3 col-lg-3 col-md-6"
                                key={key}
                              >
                                <Link href={item?.["slug url"]}>
                                  <a
                                    className="paybill__card"
                                    style={{
                                      backgroundImage: `linear-gradient(180deg,rgba(0, 84, 166, 0.4) 0%,rgba(0, 84, 166, 0.8) 100%),url(${
                                        process.env.IMAGE_URL
                                      }${
                                        typeof window !== "undefined" &&
                                        window?.screen?.width > 425
                                          ? item?.image
                                          : item?.["image sd"]
                                      })`,
                                    }}
                                  >
                                    <div className="paybill__card_desc paybill__icon">
                                      <picture>
                                        <source
                                          media="(min-width:650px)"
                                          srcSet={
                                            process.env.IMAGE_URL +
                                            item?.["icon image"]
                                          }
                                        />
                                        <source
                                          media="(max-width:649px)"
                                          srcSet={
                                            process.env.IMAGE_URL +
                                            item?.["icon image sd"]
                                          }
                                        />
                                        <img
                                          src={
                                            process.env.IMAGE_URL +
                                            item?.["icon image"]
                                          }
                                          alt="Pay Bill Ucbl"
                                          // style={{
                                          //   width: "40%",
                                          //   objectFit: "cover",
                                          // }}
                                        />
                                      </picture>
                                      <p
                                        dangerouslySetInnerHTML={{
                                          __html: isEn
                                            ? item?.title
                                            : item?.["title bn"],
                                        }}
                                      ></p>
                                    </div>
                                  </a>
                                </Link>
                              </div>
                            );
                          })}
                        </div>
                      </div>
                    </div>
                  </Fade>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section>
          <GetApp isEn={isEn} />
        </section>

        <LetUsHelp isEn={isEn} />
      </div>
    );
};

export default ProductsMain;
