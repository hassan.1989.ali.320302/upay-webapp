import React, { Fragment, useEffect, useState } from "react";
import {
  GetStaticPaths,
  GetStaticProps,
  InferGetStaticPropsType,
  GetStaticPathsResult,
} from "next";
import Banner from "../../../../components/Banner";
import BillPayCard from "../../../../components/BillPayCard";
import OtherServices from "../../../../components/OtherServices";
import LetsHelpSlider from "../../../../components/LetsHelpSlider";
import { getInnerPage, getPayBillPage } from "../../../../server";
import { useRouter } from "next/router";
import { Locales } from "../../../../components/Header";
import Fade from "react-reveal/Fade";
import Slide from "react-reveal/Slide";
import LetUsHelp from "../../../../components/LetUsHelp";

export const getStaticPaths = async () => {
  return {
    paths: [{ params: { slug: "electricity" } }],
    fallback: true,
  };
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  let page: {
    data: object;
    error: string;
  };
  try {
    const res = await getInnerPage(params.slug);
    page = {
      data: res.data,
      error: "",
    };
  } catch (err) {
    page = {
      data: {},
      error: "Something Went Wrong!",
    };
  }
  console.dir("props", page.data);
  return {
    props: {
      page,
    },
    revalidate: 1,
  };
};

const Utility = ({ page }): JSX.Element => {
  const router = useRouter();

  const [isEn, setIsEn] = useState<boolean>(true);

  useEffect(() => {
    if (typeof window !== "undefined") {
      +localStorage.getItem("locale") === Locales.English && setIsEn(false);

      window.addEventListener("locale", handleLocaleEvent, false);

      return () => {
        window.removeEventListener("locale", handleLocaleEvent);
      };
    }
  }, []);

  const handleLocaleEvent = () => {
    +localStorage.getItem("locale") === Locales.English
      ? setIsEn(false)
      : setIsEn(true);
  };

  const pageData = page?.data?.rows?.Data[0];

  if (router.isFallback) {
    return <div>Loading...</div>;
  } else if (page?.error) {
    return <div className="container">{page?.error}</div>;
  } else
    return (
      <div>
        <Banner
          name={isEn ? pageData.title : pageData["title bn"]}
          cover={{
            image: pageData?.["background image"],
            imagesd: pageData?.["background image sd"],
          }}
        />
        <section>
          <div className="electricity">
            <div className="container">
              <div className="row">
                <div className="col-xl-12">
                  <Fade bottom>
                    <p
                      className="electricity-thead"
                      dangerouslySetInnerHTML={{
                        __html: isEn ? pageData.desc : pageData["desc bn"],
                      }}
                    ></p>
                  </Fade>

                  <Slide left cascade>
                    <div className="bill__pay-cardMain">
                      {pageData["bill payment list"].map((provider, key) => {
                        return (
                          <Fragment key={key}>
                            <BillPayCard data={provider} isEn={isEn} />
                          </Fragment>
                        );
                      })}
                    </div>
                  </Slide>
                </div>
              </div>
            </div>
            <OtherServices isEn={isEn} services={pageData["Other Service"]} />
            <LetUsHelp isEn={isEn} />
          </div>
        </section>
      </div>
    );
};

export default Utility;
