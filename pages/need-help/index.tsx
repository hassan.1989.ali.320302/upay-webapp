import React, { useEffect, useState } from "react";
import { GetStaticProps, InferGetStaticPropsType } from "next";
import Banner from "../../components/Banner";
import LetsHelpSlider from "../../components/LetsHelpSlider";
import { getFAQ, getNeedHelpPage } from "../../server";
import { Locales } from "../../components/Header";
import LetUsHelp from "../../components/LetUsHelp";
import ModalVideo from "react-modal-video";
import Fade from "react-reveal/Fade";
import Buttons from "../../translations/button.json";

export const getStaticProps: GetStaticProps = async (context) => {
  let page: {
    data: object;
    faq: object;
    error: string;
  };
  try {
    const res = await getNeedHelpPage();
    const faq = await getFAQ();
    page = {
      data: res.data,
      faq: faq.data,
      error: "",
    };
  } catch (err) {
    page = {
      data: {},
      faq: {},
      error: "Something Went Wrong!",
    };
  }

  return {
    props: {
      page,
    },
    revalidate: 1,
  };
};

const NeedHelp = ({
  page: { data, faq, error },
}: InferGetStaticPropsType<typeof getStaticProps>): JSX.Element => {
  const pageContent = data?.rows?.Data[0];

  const [isEn, setIsEn] = useState<boolean>(true);
  const [isMobile, setIsMobile] = useState<boolean>(false);
  const [filteredData, setFIlteredData] = useState(null);
  const [displayType, setDisplayType] = useState<boolean>(true);
  const [searchTerm, setSearchTerm] = useState<string>("");
  const [videos, setVideos] = useState({
    main: pageContent?.Tutorial[0]?.["Tutorial list"][0],
    others: pageContent?.Tutorial[0]?.["Tutorial list"].slice(1),
  });
  const [videoId, setVideoId] = useState<String>("L61p2uyiMSo");
  const [isOpen, setOpen] = useState(false);
  const categories = faq?.rows?.Category;
  const faqData = faq?.rows?.FAQ;

  useEffect(() => {
    if (typeof window !== "undefined") {
      +localStorage.getItem("locale") === Locales.English && setIsEn(false);

      window.addEventListener("locale", handleLocaleEvent, false);

      setFIlteredData(
        faqData?.filter((i) => i["category type"] === categories[0].category)
      );

      if (window.screen.width < 991) {
        setIsMobile(true);
      }

      return () => {
        window.removeEventListener("locale", handleLocaleEvent);
      };
    }
  }, []);

  useEffect(() => {
    const elem: HTMLStyleElement = document.querySelector(
      ".help__left--options"
    );
    // console.log(elem);
    if (displayType) {
      elem.style.display = "block";
    } else {
      elem.style.display = "none";
    }
  }, [displayType]);

  useEffect(() => {
    isMobile && setDisplayType(false);
  }, [isMobile]);

  useEffect(() => {
    if (searchTerm) {
      setFIlteredData(
        faqData?.filter((i) => {
          let term =
            i.question.toLowerCase() +
            i["question bn"] +
            i.answer.toLowerCase() +
            i["answer bn"];
          return term.includes(searchTerm);
        })
      );
    } else {
      setFIlteredData(
        faqData?.filter((i) => i["category type"] === categories[0].category)
      );
    }
  }, [searchTerm]);

  const handleFaqs = (cat: string) => {
    setFIlteredData(faqData?.filter((i) => i["category type"] === cat));
  };

  const handleSuffle = (index) => {
    const restVids = videos.others;
    setVideoId(process.env.IMAGE_URL + restVids[index].video);
    setOpen(true);

    setVideos({
      main: restVids[index],
      others: [...restVids.filter((item, idx) => idx !== index), videos.main],
    });
  };

  const handleLocaleEvent = () => {
    +localStorage.getItem("locale") === Locales.English
      ? setIsEn(false)
      : setIsEn(true);
  };

  if (error) {
    <div className="container">{error}</div>;
  } else
    return (
      <div>
        <Banner
          name={
            isEn
              ? pageContent?.["cover title"]
              : pageContent?.["cover title bn"]
          }
          cover={{
            image: pageContent?.["cover image"],
            imagesd: pageContent?.["cover image sd"],
          }}
        />
        <div className="how-to-tutorial">
          <Fade bottom cascade>
            <div className="dashmid__wDesc__header--part how-to-text">
              <h1
                dangerouslySetInnerHTML={{
                  __html: isEn
                    ? pageContent?.Tutorial[0]?.title
                    : pageContent?.Tutorial[0]?.["title bn"],
                }}
              ></h1>
              <p
                dangerouslySetInnerHTML={{
                  __html: isEn
                    ? pageContent?.Tutorial[0]?.desc
                    : pageContent?.Tutorial[0]?.["desc bn"],
                }}
              ></p>
            </div>
          </Fade>
          <section>
            <div className="container">
              <div className="row">
                <Fade left>
                  <div className="col-xl-8 col-lg-8 col-md-12">
                    <div className="tutorial__primary--cardMain">
                      <div className="card tutorial__large-card">
                        <div className="top__video--part">
                          <picture>
                            <source
                              media="(min-width:650px)"
                              srcSet={
                                process.env.IMAGE_URL +
                                videos.main["thumb image"]
                              }
                            />
                            <source
                              media="(max-width:649px)"
                              srcSet={
                                process.env.IMAGE_URL +
                                videos.main["thumb image sd"]
                              }
                            />
                            <img
                              src={
                                process.env.IMAGE_URL +
                                videos.main["thumb image"]
                              }
                              alt="Pay Bill Ucbl Video"
                              className="card-img-top"
                              style={{
                                borderTopLeftRadius: "calc(1.25rem - 1px)",
                                borderTopRightRadius: "calc(1.25rem - 1px)",
                              }}
                            />
                          </picture>
                          <a
                            onClick={() => {
                              setOpen(true);
                              setVideoId(
                                process.env.IMAGE_URL + videos.main.video
                              );
                            }}
                            className="play__icon"
                          >
                            <img src="/images/video-icon.svg" alt="" />
                          </a>
                        </div>
                        <div className="card-body">
                          <h5
                            className="card-title"
                            dangerouslySetInnerHTML={{
                              __html: isEn
                                ? videos?.main.title
                                : videos?.main?.["title bn"],
                            }}
                          ></h5>
                          <p
                            className="card-text"
                            dangerouslySetInnerHTML={{
                              __html: isEn
                                ? videos?.main?.desc
                                : videos?.main?.["desc bn"],
                            }}
                          ></p>
                        </div>
                      </div>
                    </div>
                  </div>
                </Fade>
                <Fade right>
                  <div className="col-xl-4 col-lg-4 col-md-12">
                    <div className="scrolling__selectCard--slide">
                      <div className="row scrolling__selectCard--slide-row">
                        {videos.others.map((vid, key: number) => {
                          return (
                            <div
                              className="col-xl-12 col-lg-12 col-md-5 col-sm-5 col-5 custom__col"
                              key={key}
                            >
                              <div className="small__tutorial-cardMain">
                                <a href="#!" className="video__select-card">
                                  <picture>
                                    <source
                                      media="(min-width:650px)"
                                      srcSet={
                                        process.env.IMAGE_URL +
                                        vid?.["thumb image"]
                                      }
                                      // className="card-img-top"
                                    />
                                    <source
                                      media="(max-width:649px)"
                                      srcSet={
                                        process.env.IMAGE_URL +
                                        vid?.["thumb image sd"]
                                      }
                                      // className="card-img-top"
                                    />
                                    <img
                                      src={
                                        process.env.IMAGE_URL +
                                        vid?.["thumb image"]
                                      }
                                      alt="Pay Bill Ucbl Video"
                                      // className="card-img-top"
                                      // style={{
                                      //   borderTopLeftRadius: "calc(1.25rem - 1px)",
                                      //   borderTopRightRadius: "calc(1.25rem - 1px)",
                                      // }}
                                    />
                                  </picture>
                                  <div className="desc__part vid-text">
                                    <h5
                                      dangerouslySetInnerHTML={{
                                        __html: isEn
                                          ? vid?.title
                                          : vid?.["title bn"],
                                      }}
                                    ></h5>
                                    <p
                                      dangerouslySetInnerHTML={{
                                        __html: isEn
                                          ? vid?.desc
                                          : vid?.["desc bn"],
                                      }}
                                    ></p>
                                  </div>
                                </a>

                                <a
                                  onClick={() => handleSuffle(key)}
                                  className="playicon"
                                >
                                  <img
                                    src="/images/video-icon-small.svg"
                                    alt=""
                                  />
                                </a>
                              </div>
                            </div>
                          );
                        })}
                      </div>
                    </div>
                  </div>
                </Fade>
              </div>
            </div>
          </section>
        </div>
        <div className="how_resetPin--mainPart tb__padding">
          <div className="container">
            <Fade bottom cascade>
              <div className="dashmid__wDesc__header--part">
                <h1>
                  {isEn
                    ? pageContent?.FAQ[0]?.title
                    : pageContent?.FAQ[0]?.["title bn"]}
                </h1>
                <p>
                  {isEn
                    ? pageContent?.FAQ[0]?.desc
                    : pageContent?.FAQ[0]?.["desc bn"]}
                </p>
              </div>
            </Fade>
            <div className="row">
              <div className="col-xl-12">
                <div className="search__barMain">
                  <form className="search__mainForm">
                    <div className="form-group curstom__form-group">
                      <input
                        type="text"
                        className="form-control curstom__form-control"
                        id="search"
                        aria-describedby="search"
                        value={searchTerm}
                        onChange={(e) => setSearchTerm(e.target.value)}
                        placeholder="What are you looking for?"
                      />
                    </div>
                    <button type="submit" className="btn search__go--btn">
                      <svg
                        width="26"
                        height="26"
                        viewBox="0 0 26 26"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M18.6042 16.4167H17.4521L17.0438 16.0229C18.4729 14.3604 19.3333 12.2021 19.3333 9.85417C19.3333 4.61875 15.0896 0.375 9.85417 0.375C4.61875 0.375 0.375 4.61875 0.375 9.85417C0.375 15.0896 4.61875 19.3333 9.85417 19.3333C12.2021 19.3333 14.3604 18.4729 16.0229 17.0438L16.4167 17.4521V18.6042L23.7083 25.8813L25.8813 23.7083L18.6042 16.4167ZM9.85417 16.4167C6.22292 16.4167 3.29167 13.4854 3.29167 9.85417C3.29167 6.22292 6.22292 3.29167 9.85417 3.29167C13.4854 3.29167 16.4167 6.22292 16.4167 9.85417C16.4167 13.4854 13.4854 16.4167 9.85417 16.4167Z"
                          fill="#F6F6F6"
                        />
                      </svg>{" "}
                      Go
                    </button>
                  </form>
                  <div className="options__mobile--sec">
                    <a
                      onClick={() => setDisplayType(!displayType)}
                      className="btn options__btn"
                    >
                      {isEn ? Buttons.option.en : Buttons.option.bn}{" "}
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="19"
                        height="18"
                        viewBox="0 0 19 18"
                        fill="none"
                      >
                        <path
                          d="M19 0.987305H0V17.6123H19V0.987305ZM1.1875 2.1748H17.8125V4.5498H1.1875V2.1748ZM17.8125 16.4248H1.1875V5.7373H17.8125V16.4248ZM5.9375 10.4873V6.9248H2.375V10.4873H5.9375ZM4.75 8.1123V9.2998H3.5625V8.1123H4.75ZM5.9375 15.2373V11.6748H2.375V15.2373H5.9375ZM4.75 12.8623V14.0498H3.5625V12.8623H4.75ZM15.4375 9.2998V8.1123H8.3125V9.2998H15.4375ZM15.4375 14.0498V12.8623H8.3125V14.0498H15.4375Z"
                          fill="black"
                        />
                      </svg>
                    </a>
                  </div>
                </div>
              </div>
              <Fade left>
                <div className="col-xl-4 mb-5">
                  <div
                    className="help__left--options"
                    style={{ display: "block" }}
                  >
                    <h2>{isEn ? Buttons.faqCat.en : Buttons.faqCat.bn}</h2>
                    <div className="list___parts">
                      <ul>
                        {categories?.map((item, idx) => {
                          return (
                            <li key={idx}>
                              <a
                                onClick={() => handleFaqs(item.category)}
                                style={{ cursor: "pointer" }}
                              >
                                {isEn ? item?.category : item?.["category bn"]}
                              </a>
                            </li>
                          );
                        })}
                      </ul>
                    </div>
                  </div>
                </div>
              </Fade>
              <Fade right>
                <div className="col-xl-8">
                  <div className="faq__section">
                    <div className="css__collapse">
                      {filteredData?.map((faq, key) => {
                        return (
                          <div className="tab">
                            <input
                              className="tab__input"
                              id={`tab-${key + 10}`}
                              type="checkbox"
                            />
                            <label
                              className="tab__label"
                              htmlFor={`tab-${key + 10}`}
                            >
                              <span>
                                {isEn ? faq?.question : faq?.["question bn"]}{" "}
                              </span>
                            </label>
                            <div className="tab__content">
                              <div className="content__wrap">
                                <p
                                  dangerouslySetInnerHTML={{
                                    __html: isEn
                                      ? faq?.answer
                                      : faq?.["answer bn"],
                                  }}
                                ></p>
                              </div>
                            </div>
                          </div>
                        );
                      })}
                    </div>
                  </div>
                </div>
              </Fade>
            </div>
          </div>
        </div>

        <LetUsHelp isEn={isEn} />

        <ModalVideo
          channel="custom"
          autoplay
          isOpen={isOpen}
          url={videoId}
          onClose={() => setOpen(false)}
        />
      </div>
    );
};

export default NeedHelp;
