import React, { Fragment, useEffect, useState } from "react";
import Banner from "../../components/Banner";
import LetsHelpSlider from "../../components/LetsHelpSlider";
import { GetStaticProps, InferGetStaticPropsType } from "next";
import { getPartnerWithUsPage, getDivisions, getTypes } from "../../server";
import { Locales } from "../../components/Header";
import Image from "next/image";
import ButtonText from "../../translations/button.json";
import Form from "../../components/Form";
import PartnerOpportunityCard from "../../components/PartnerOpportunityCard";
import LetUsHelp from "../../components/LetUsHelp";
import Link from "next/link";
import OurMarchantPartner from "../../components/OurMerchantPartner";
import OurValueAgentCarousel from "../../components/OurValuedAgents";

type Page = {
  data: any;
  divs: any;
  types: any;
  error: string;
};

type Props = {
  page: Page;
};

type State = {
  file: File | string;
  want: boolean;
  where: boolean;
  isEn: boolean;
};

export const getStaticProps: GetStaticProps = async (context) => {
  let page: Page;
  try {
    const res = await getPartnerWithUsPage();
    const divs = await getDivisions();
    const types = await getTypes();
    page = {
      data: res.data,
      divs: divs.data,
      types: types.data,
      error: "",
    };
  } catch (err) {
    page = {
      data: {},
      divs: [],
      types: [],
      error: "Something Went Wrong!",
    };
  }
  // console.log("here", page);
  return {
    props: {
      page,
    },
    revalidate: 1,
  };
};

const Partner = ({
  page: { data, divs, types, error },
}: InferGetStaticPropsType<typeof getStaticProps>): JSX.Element => {
  // constructor(props: Props) {
  //   super(props);
  //   console.log("p", props);
  //   this.state = {
  //     file: "Upload NID Front",
  //     want: false,
  //     where: false,
  //     isEn: true,
  //   };
  //   // this.handleLocaleEvent = this.handleLocaleEvent.bind(this);
  // }

  const pageData = data?.rows?.Data[0];

  const [isEn, setIsEn] = useState<boolean>(true);

  useEffect(() => {
    if (typeof window !== "undefined") {
      +localStorage.getItem("locale") === Locales.English && setIsEn(false);

      window.addEventListener("locale", handleLocaleEvent, false);

      return () => {
        window.removeEventListener("locale", handleLocaleEvent);
      };
    }
  });

  console.log(divs, types);

  const handleLocaleEvent = () => {
    +localStorage.getItem("locale") === Locales.English
      ? setIsEn(false)
      : setIsEn(true);
  };

  if (error) {
    return <div className="partner">{error}</div>;
  } else
    return (
      <div>
        <Banner
          name={isEn ? pageData?.title : pageData?.["title bn"]}
          cover={{
            image: pageData?.["cover image"],
            imagesd: pageData?.["cover image sd"],
          }}
        />
        <div className="partner">
          <section>
            <div className="our__partner-section">
              <div className="container-fluid">
                <div className="row">
                  <div className="col-xl-6 col-lg-6 custom__col">
                    <div className="left__imgPart">
                      {/* <Image
                        src={
                          process.env.IMAGE_URL +
                          data.rows["Top caption"][0].image
                        }
                        alt="Partner"
                        layout="responsive"
                        height="100"
                        width="100"
                      /> */}
                      <picture>
                        <source
                          media="(min-width:650px)"
                          srcSet={
                            process.env.IMAGE_URL +
                            pageData?.["Data canvas"][0].image
                          }
                        />
                        <source
                          media="(max-width:649px)"
                          srcSet={
                            process.env.IMAGE_URL +
                            pageData?.["Data canvas"][0]?.["image sd"]
                          }
                        />
                        <img
                          src={
                            process.env.IMAGE_URL +
                            pageData?.["Data canvas"][0].image
                          }
                          alt="partners ucbl"
                          // style={{
                          //   width: "40%",
                          //   objectFit: "cover",
                          // }}
                        />
                      </picture>
                    </div>
                  </div>
                  <div className="col-xl-6 col-lg-6 custom__col">
                    <div className="right__part">
                      <h3
                        dangerouslySetInnerHTML={{
                          __html: isEn
                            ? pageData?.["Data canvas"][0]?.title
                            : pageData?.["Data canvas"][0]?.["title bn"],
                        }}
                      ></h3>
                      <p
                        dangerouslySetInnerHTML={{
                          __html: isEn
                            ? pageData?.["Data canvas"][0]?.desc
                            : pageData?.["Data canvas"][0]?.["desc bn"],
                        }}
                      ></p>
                      <div className="right__arrow--btnPart">
                        <Link href="#want-to-join-form">
                          <a className="btn right__arrow--btn primary__bg--btn inline__blck--btn">
                            {isEn
                              ? ButtonText.register.en
                              : ButtonText.register.bn}{" "}
                            <svg
                              width={40}
                              height={24}
                              viewBox="0 0 40 24"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <path
                                d="M26.4575 11H6.60938V13H26.4575V16L33.0515 12L26.4575 8V11Z"
                                fill="black"
                              />
                            </svg>
                          </a>
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <section>
            <div className="advantage tb__padding">
              <div className="container">
                <div className="row">
                  <div className="col-xl-12">
                    <div className="dashleft__wDesc__header--part">
                      <h1
                        dangerouslySetInnerHTML={{
                          __html: isEn
                            ? pageData?.["Advantages"][0]?.title
                            : pageData?.["Advantages"][0]?.["title bn"],
                        }}
                      ></h1>
                      <p
                        dangerouslySetInnerHTML={{
                          __html: isEn
                            ? pageData?.["Advantages"][0]?.desc
                            : pageData?.["Advantages"][0]?.["desc bn"],
                        }}
                      ></p>
                    </div>

                    <div className="advantage__card-sec">
                      <div className="row">
                        {pageData?.["Advantages"][0]?.["advantages list"].map(
                          (adv, key) => {
                            return (
                              <div
                                className="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6"
                                key={key}
                              >
                                <div>
                                  <div className="card how__it__word--secCard">
                                    <picture>
                                      <source
                                        className="card-img-top"
                                        media="(min-width:671px)"
                                        srcSet={
                                          process.env.IMAGE_URL + adv.image
                                        }
                                      />
                                      <source
                                        className="card-img-top"
                                        media="(max-width:670px)"
                                        srcSet={
                                          process.env.IMAGE_URL +
                                          adv["image sd"]
                                        }
                                      />
                                      <img
                                        className="card-img-top"
                                        src={process.env.IMAGE_URL + adv.image}
                                        alt="Partner"
                                        style={{ borderRadius: "50%" }}
                                      />
                                    </picture>
                                    {/* <img
                                    className="card-img-top"
                                    src="/images/how-it-work-sec.svg"
                                    alt="Card image cap"
                                  /> */}
                                    <div className="card-body">
                                      <h5
                                        className="card-title"
                                        dangerouslySetInnerHTML={{
                                          __html: isEn
                                            ? adv.title
                                            : adv["title bn"],
                                        }}
                                      ></h5>
                                      <p
                                        className="card-text"
                                        dangerouslySetInnerHTML={{
                                          __html: isEn
                                            ? adv.desc
                                            : adv["desc bn"],
                                        }}
                                      ></p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            );
                          }
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <section>
            <div className="partner-opportunity tb__margin">
              <div className="container">
                <h1
                  className="po-maintext"
                  dangerouslySetInnerHTML={{
                    __html: isEn
                      ? pageData?.["Opportunities list"][0]?.title
                      : pageData?.["Opportunities list"][0]?.["title bn"],
                  }}
                ></h1>
                <div className="opportunity-list">
                  <div className="row d-flex justify-content-md-between">
                    {pageData?.["Opportunities list"][0]?.["Opportunities"].map(
                      (opp, index) => {
                        return (
                          <Fragment key={index}>
                            <PartnerOpportunityCard
                              img={opp.image}
                              imgsd={opp["image sd"]}
                              title={isEn ? opp.title : opp["title bn"]}
                            />
                          </Fragment>
                        );
                      }
                    )}
                  </div>
                </div>
              </div>
            </div>
          </section>

          {divs && types && (
            <Form
              isEn={isEn}
              headers={pageData?.["Join with us API"][0]}
              divs={divs}
              types={types}
            />
          )}

          <section>
            <div className="agent-partner tb__padding">
              <div className="container">
                <div className="row">
                  <div className="col-xl-12">
                    <div className="agent" id="agent">
                      <div className="dashleft__header--part">
                        <h1
                          dangerouslySetInnerHTML={{
                            __html: isEn
                              ? pageData?.["Valued Agents list"][0]?.title
                              : pageData?.["Valued Agents list"][0]?.[
                                  "title bn"
                                ],
                          }}
                        ></h1>
                      </div>

                      <div className="agent__card-sec">
                        <OurValueAgentCarousel
                          data={
                            pageData?.["Valued Agents list"][0]?.[
                              "Valued Agents"
                            ]
                          }
                          isEn={isEn}
                        />
                        {/* <div className="row">
                          {pageData?.["Valued Agents list"][0]?.[
                            "Valued Agents"
                          ].map((agent, index) => {
                            return (
                              <div className="col-xl-6 col-lg-6" key={index}>
                                <div>
                                  <div className="review__card">
                                    <div className="media review__card-media">
                                      <picture>
                                        <source
                                          media="(min-width:671px)"
                                          srcSet={
                                            process.env.IMAGE_URL + agent.image
                                          }
                                        />
                                        <source
                                          media="(max-width:670px)"
                                          srcSet={
                                            process.env.IMAGE_URL +
                                            agent["image sd"]
                                          }
                                        />
                                        <img
                                          className="align-self-center review__card--img mr-3"
                                          src={
                                            process.env.IMAGE_URL + agent.image
                                          }
                                          alt="Partner"
                                          style={{
                                            borderRadius: "50%",
                                            width: "172px",
                                            height: "172px",
                                          }}
                                        />
                                      </picture>
                                      <div className="media-body review__card-media-body">
                                        <h5 className="mt-0">
                                          <svg
                                            width={47}
                                            height={35}
                                            viewBox="0 0 47 35"
                                            fill="none"
                                            xmlns="http://www.w3.org/2000/svg"
                                          >
                                            <path
                                              d="M20.5261 25.5281C20.5261 20.9076 17.4938 16.6337 12.129 16.6337C10.4963 16.6337 9.2134 17.0957 8.16377 17.6733C8.7469 5.89109 20.5261 8.54785 20.5261 3.46535C20.5261 1.61716 19.1265 0 16.5608 0C12.9454 0 7.69727 3.34983 5.94789 4.96699C2.33251 8.66336 0 12.5908 0 20.4455C0 28.3003 3.38213 35 10.9628 35C16.2109 35 20.5261 30.7261 20.5261 25.5281ZM47 25.5281C47 20.9076 43.9677 16.6337 38.603 16.6337C36.9702 16.6337 35.6873 17.0957 34.6377 17.6733C35.2208 5.89109 47 8.54785 47 3.46535C47 1.61716 45.6005 0 43.0347 0C39.4194 0 34.1712 3.34983 32.4218 4.96699C28.8064 8.66336 26.4739 12.5908 26.4739 20.4455C26.4739 28.3003 29.8561 35 37.4367 35C42.6849 35 47 30.7261 47 25.5281Z"
                                              fill="#FFD602"
                                            />
                                          </svg>
                                          {isEn ? agent.name : agent["name bn"]}
                                        </h5>
                                        <p
                                          dangerouslySetInnerHTML={{
                                            __html: isEn
                                              ? agent.desc
                                              : agent["desc bn"],
                                          }}
                                        ></p>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            );
                          })}
                        </div> */}
                      </div>
                    </div>

                    <div className="partner" id="merchant">
                      <div className="dashleft__header--part">
                        <h1
                          dangerouslySetInnerHTML={{
                            __html: isEn
                              ? pageData?.["Merchant Partners list"][0]?.title
                              : pageData?.["Merchant Partners list"][0]?.[
                                  "title bn"
                                ],
                          }}
                        ></h1>
                      </div>

                      <div className="partner__card-sec">
                        <OurMarchantPartner
                          data={
                            pageData?.["Merchant Partners list"][0]?.[
                              "Merchant Partners"
                            ]
                          }
                        />
                        {/* <div className="row">
                          {pageData?.["Merchant Partners list"][0]?.[
                            "Merchant Partners"
                          ].map((partner, index) => {
                            return (
                              <div
                                className="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6"
                                key={index}
                              >
                                <div>
                                  <div className="partner__card">
                                    <picture>
                                      <source
                                        media="(min-width:671px)"
                                        srcSet={
                                          process.env.IMAGE_URL + partner.image
                                        }
                                      />
                                      <source
                                        media="(max-width:670px)"
                                        srcSet={
                                          process.env.IMAGE_URL +
                                          partner["image sd"]
                                        }
                                      />
                                      <img
                                        src={
                                          process.env.IMAGE_URL + partner.image
                                        }
                                        alt="Marchent Partner"
                                      />
                                    </picture>
                                  </div>
                                </div>
                              </div>
                            );
                          })}
                        </div> */}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <LetUsHelp isEn={isEn} />
        </div>
      </div>
    );
};

export default Partner;
