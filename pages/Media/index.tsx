import React, { useEffect, useState } from "react";
import Banner from "../../components/Banner";
import { GetStaticProps, InferGetStaticPropsType } from "next";
import Link from "next/link";
import LetUsHelp from "../../components/LetUsHelp";
import Fade from "react-reveal/Fade";
import { Locales } from "../../components/Header";
import { getMediaCenter } from "../../server";
import ButtonLocale from "../../translations/button.json";

export const getStaticProps: GetStaticProps = async (context) => {
  let page: {
    data: object;
    error: string;
  };
  try {
    const res = await getMediaCenter();
    // console.log("resss", res);
    page = {
      data: res.data,
      error: "",
    };
  } catch (err) {
    page = {
      data: {},
      error: "Something Went Wrong!",
    };
  }

  return {
    props: {
      page,
    },
    revalidate: 1,
  };
};

const MediaCenter = ({
  page: { data, error },
}: InferGetStaticPropsType<typeof getStaticProps>): JSX.Element => {
  const pageData = data?.rows?.Data[0];
  const [isEn, setIsEn] = useState<boolean>(true);
  const [medias, setMedias] = useState<{ dual; single }>({
    dual: null,
    single: null,
  });

  useEffect(() => {
    if (typeof window !== "undefined") {
      +localStorage.getItem("locale") === Locales.English && setIsEn(false);

      window.addEventListener("locale", handleLocaleEvent, false);

      return () => {
        window.removeEventListener("locale", handleLocaleEvent);
      };
    }
  }, []);

  useEffect(() => {
    if (pageData) {
      setMedias({
        dual: pageData?.["segment one"]?.filter(
          (item) => item.category != "upay-blog"
        ),
        single: pageData?.["segment one"]?.filter(
          (item) => item.category == "upay-blog"
        ),
      });
    }
  }, [pageData]);

  const handleLocaleEvent = () => {
    +localStorage.getItem("locale") === Locales.English
      ? setIsEn(false)
      : setIsEn(true);
  };

  if (error) {
    return <div>{error}</div>;
  } else
    return (
      <div>
        <Banner
          name={isEn ? pageData.title : pageData["title bn"]}
          cover={{
            image: pageData["cover image"],
            imagesd: pageData["cover image sd"],
          }}
        />
        <div className="media__center--mainPart tb__padding">
          <div className="container">
            <div className="row justify-content-center">
              {medias?.dual?.map((item, index) => {
                const slugArr = item["slug url"].split("/");

                const url =
                  slugArr.length > 2
                    ? item["slug url"]
                    : "/" + slugArr[slugArr.length - 1];
                return (
                  <div className="col-xl-6 col-lg-6 col-md-6" key={index}>
                    <Fade left={index % 2 === 0} right={index % 2 !== 0}>
                      <div className="card media__center--cardMain media-card-minh">
                        <picture>
                          <source
                            media="(min-width:650px)"
                            srcSet={process.env.IMAGE_URL + item.image}
                          />
                          <source
                            media="(max-width:649px)"
                            srcSet={process.env.IMAGE_URL + item["image sd"]}
                          />
                          <img
                            src={process.env.IMAGE_URL + item.image}
                            className="card-img-top"
                            alt="Media in Ucbl"
                            // style={{
                            //   width: "40%",
                            //   objectFit: "cover",
                            // }}
                          />
                        </picture>
                        <div className="card-body media__center--card-body">
                          <h5
                            className="card-title"
                            dangerouslySetInnerHTML={{
                              __html: isEn ? item?.title : item?.["title bn"],
                            }}
                          ></h5>
                          <p
                            className="card-text"
                            dangerouslySetInnerHTML={{
                              __html: isEn ? item?.desc : item?.["desc bn"],
                            }}
                          ></p>
                          <Link href={url}>
                            <a className="mediaCenter__card__readBtn">
                              <span>
                                {isEn
                                  ? ButtonLocale.read_more.en
                                  : ButtonLocale.read_more.bn}
                              </span>
                              <svg
                                width={28}
                                height={24}
                                viewBox="0 0 28 24"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                              >
                                <path
                                  d="M18.0709 11H4.64844V13H18.0709V16L22.5301 12L18.0709 8V11Z"
                                  fill="#4E4E50"
                                />
                              </svg>
                            </a>
                          </Link>
                        </div>
                      </div>
                    </Fade>
                  </div>
                );
              })}
              {medias?.single?.map((item, index) => {
                const url = item["slug url"].split("/")[
                  item["slug url"].split("/").length - 1
                ];
                return (
                  <div className="col-xl-12 col-lg-12 col-md-6" key={index}>
                    <Fade bottom>
                      <div className="media__center-cardLarge">
                        <div className="row custom__col-row">
                          <div className="col-xl-6 col-lg-6 custom__col-media">
                            <div className="left__imgPart">
                              <picture>
                                <source
                                  media="(min-width:650px)"
                                  srcSet={process.env.IMAGE_URL + item.image}
                                />
                                <source
                                  media="(max-width:649px)"
                                  srcSet={
                                    process.env.IMAGE_URL + item["image sd"]
                                  }
                                />
                                <img
                                  src={process.env.IMAGE_URL + item.image}
                                  className="card-img-top"
                                  alt="Media in Ucbl"
                                  // style={{
                                  //   width: "40%",
                                  //   objectFit: "cover",
                                  // }}
                                />
                              </picture>
                            </div>
                          </div>
                          <div className="col-xl-6 col-lg-6 custom__col-media">
                            <div className="right__desc">
                              <div className="main__desc">
                                <h3
                                  dangerouslySetInnerHTML={{
                                    __html: isEn
                                      ? item?.title
                                      : item?.["title bn"],
                                  }}
                                ></h3>
                                <p
                                  dangerouslySetInnerHTML={{
                                    __html: isEn
                                      ? item?.desc
                                      : item?.["desc bn"],
                                  }}
                                ></p>
                                <Link href={`/${url}`}>
                                  <a className="mediaCenter__card__readBtn">
                                    <span>
                                      {isEn
                                        ? ButtonLocale.read_more.en
                                        : ButtonLocale.read_more.bn}
                                    </span>
                                    <svg
                                      width={28}
                                      height={24}
                                      viewBox="0 0 28 24"
                                      fill="none"
                                      xmlns="http://www.w3.org/2000/svg"
                                    >
                                      <path
                                        d="M18.0709 11H4.64844V13H18.0709V16L22.5301 12L18.0709 8V11Z"
                                        fill="#4E4E50"
                                      />
                                    </svg>
                                  </a>
                                </Link>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </Fade>
                  </div>
                );
              })}
            </div>
          </div>
        </div>

        {/* <section className="bg__gray">
        <div className="lets__help__you--sectionMain tb__padding">
          <div className="container">
            <div className="dashmid__wDesc__header--part bottom__margin">
              <h1>Let Us Help You</h1>
              <p>
                We are always hear for you, every step of the way. Have a query
                and need help? Our team is ready to answer all your questions.
              </p>
            </div>
            <div className="lets__help__sliderMain">
              <LetsHelpSlider />
            </div>
          </div>
        </div>
      </section> */}
        <LetUsHelp isEn={isEn} />
      </div>
    );
};

export default MediaCenter;
