import React, { useEffect, useState } from "react";
import Banner from "../../components/Banner";
import LetsHelpSlider from "../../components/LetsHelpSlider";
import { GetStaticProps, InferGetStaticPropsType } from "next";
import { getCashInPage, getLatestOfferPage } from "../../server";
import { Locales } from "../../components/Header";
import Link from "next/link";
import ButtonTxt from "../../translations/button.json";
import { Flipper, Flipped } from "react-flip-toolkit";
import Fade from "react-reveal/Fade";
import LetUsHelp from "../../components/LetUsHelp";

export const getStaticProps: GetStaticProps = async (context) => {
  let page: {
    data: object;
    error: string;
  };
  try {
    const res = await getLatestOfferPage();
    page = {
      data: res.data,
      error: "",
    };
  } catch (err) {
    page = {
      data: {},
      error: "Something Went Wrong!",
    };
  }

  return {
    props: {
      page,
    },
    revalidate: 1,
  };
};

const LatestOffer = ({
  page: { data, error },
}: InferGetStaticPropsType<typeof getStaticProps>): JSX.Element => {
  const pageData = data?.rows?.Data[0];
  const allOffers = data?.rows?.Data[0]?.canvas;

  const [isEn, setIsEn] = useState<boolean>(true);
  const [currentOption, setCurrentOption] = useState<String>("All");
  const [filteredOffers, setFilteredOffers] = useState(allOffers);
  const [page, setPage] = useState<number>(6);

  useEffect(() => {
    if (typeof window !== "undefined") {
      +localStorage.getItem("locale") === Locales.English && setIsEn(false);

      window.addEventListener("locale", handleLocaleEvent, false);

      return () => {
        window.removeEventListener("locale", handleLocaleEvent);
      };
    }
  }, []);

  const changeOption = (opt: String) => {
    setCurrentOption(opt);
    setPage(6);
    if (opt === "All") {
      setFilteredOffers(allOffers);
    } else {
      setFilteredOffers([
        ...allOffers.filter((item) => item["option  "] === opt),
      ]);
    }
  };

  const handleLocaleEvent = () => {
    +localStorage.getItem("locale") === Locales.English
      ? setIsEn(false)
      : setIsEn(true);
  };

  if (error) {
    return <div>{error}</div>;
  } else
    return (
      <div>
        <Banner
          name={isEn ? pageData.title : pageData["title bn"]}
          cover={{
            image: pageData["cover image"],
            imagesd: pageData["cover image sd"],
          }}
        />

        <div className="latest-offer">
          <div className="container">
            <div className="row">
              <Fade bottom cascade>
                <div className="col-xl-12">
                  <div className="latest-offer_top-desc">
                    <p
                      dangerouslySetInnerHTML={{
                        __html: isEn ? pageData?.desc : pageData?.["desc bn"],
                      }}
                    ></p>
                  </div>

                  <div className="latest-offer_list">
                    <ul className="list-inline">
                      <li
                        className={`list-inline-item ${
                          currentOption === "All" && "active"
                        }`}
                        style={{ cursor: "pointer" }}
                      >
                        <a
                          onClick={() => changeOption("All")}
                          className="list-inline-link"
                        >
                          {isEn ? "All" : "সবগুলো"}
                        </a>
                      </li>
                      {pageData?.Options.map((option, key) => {
                        return (
                          <li
                            className={`list-inline-item ${
                              currentOption === option?.titile && "active"
                            }`}
                            style={{ cursor: "pointer" }}
                            key={key}
                          >
                            <a
                              onClick={() => changeOption(option.titile)}
                              className="list-inline-link"
                              dangerouslySetInnerHTML={{
                                __html: isEn
                                  ? option?.titile
                                  : option?.["title bn"],
                              }}
                            ></a>
                          </li>
                        );
                      })}

                      {/* <li className="list-inline-item">
                      <a href="#!" className="list-inline-link">
                        Discount
                      </a>
                    </li>

                    <li className="list-inline-item">
                      <a href="#!" className="list-inline-link">
                        commission
                      </a>
                    </li>

                    <li className="list-inline-item">
                      <a href="#!" className="list-inline-link">
                        Gift
                      </a>
                    </li>

                    <li className="list-inline-item">
                      <a href="#!" className="list-inline-link">
                        Other
                      </a>
                    </li> */}
                    </ul>
                  </div>
                  <Flipper
                    flipKey={filteredOffers}
                    spring="gentle"
                    staggerConfig={{
                      default: {
                        reverse: false,
                      },
                    }}
                  >
                    <div className="latest-offer_cardlist">
                      <div className="row">
                        {filteredOffers.slice(0, page).map((offer, key) => {
                          const slugArr = offer["slug url"].split("/");

                          const url =
                            slugArr.length > 2
                              ? offer["slug url"]
                              : "/Latest-offer/" + slugArr[slugArr.length - 1];
                          return (
                            <div
                              className="col-xl-4 col-lg-4 col-md-6"
                              style={{ display: "flex", flex: "1" }}
                              key={key}
                            >
                              <Flipped key={offer} flipId={offer.title}>
                                <div className="card our__blog--cardMain latest_card_custom ">
                                  <picture>
                                    <source
                                      media="(min-width:650px)"
                                      srcSet={
                                        process.env.IMAGE_URL + offer.image
                                      }
                                    />
                                    <source
                                      media="(max-width:649px)"
                                      srcSet={
                                        process.env.IMAGE_URL +
                                        offer["image sd"]
                                      }
                                    />
                                    <img
                                      src={process.env.IMAGE_URL + offer.image}
                                      alt="Pay Bill Ucbl"
                                      className="card-img-top latest_custom_img"
                                    />
                                  </picture>

                                  <div className="card-body our__blog--card-body">
                                    <h5
                                      className="card-title"
                                      dangerouslySetInnerHTML={{
                                        __html: isEn
                                          ? offer?.title
                                          : offer?.["title bn"],
                                      }}
                                    ></h5>
                                    <p
                                      className="card-text"
                                      dangerouslySetInnerHTML={{
                                        __html: isEn
                                          ? offer?.desc
                                          : offer?.["desc bn"],
                                      }}
                                    ></p>
                                    <Link href={url}>
                                      <a className="ourBlog__card__exploreBtn">
                                        <span>
                                          {isEn
                                            ? ButtonTxt.expolre.en
                                            : ButtonTxt.expolre.bn}
                                        </span>
                                        <svg
                                          width={36}
                                          height={22}
                                          viewBox="0 0 36 22"
                                          fill="none"
                                          xmlns="http://www.w3.org/2000/svg"
                                        >
                                          <g clipPath="url(#clip0)">
                                            <path
                                              d="M24.0105 10.2671H6.71094V12.0103H24.0105V14.6251L29.7578 11.1387L24.0105 7.65234V10.2671Z"
                                              fill="#0054A6"
                                            />
                                          </g>
                                          <defs>
                                            <clipPath id="clip0">
                                              <rect
                                                width="34.5703"
                                                height="20.9183"
                                                fill="white"
                                                transform="translate(0.947266 0.679688)"
                                              />
                                            </clipPath>
                                          </defs>
                                        </svg>
                                      </a>
                                    </Link>
                                  </div>
                                </div>
                              </Flipped>
                            </div>
                          );
                        })}
                      </div>
                      {filteredOffers.length > page && (
                        <div className="view__more--btn text-center mt-5">
                          <a
                            onClick={() => setPage(page + 3)}
                            className="btn right__arrow--btn primary__bg--btn inline__blck--btn"
                          >
                            {isEn
                              ? ButtonTxt.view_more.en
                              : ButtonTxt.view_more.bn}{" "}
                          </a>
                        </div>
                      )}
                    </div>
                  </Flipper>
                </div>
              </Fade>
            </div>
          </div>
        </div>
        <LetUsHelp isEn={isEn} />
      </div>
    );
};

export default LatestOffer;
