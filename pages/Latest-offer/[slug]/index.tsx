import React, { useState, useEffect } from "react";
import { GetStaticProps, InferGetStaticPropsType } from "next";
import MainSlider from "../../../components/MainSlider";
import LetsHelpSlider from "../../../components/LetsHelpSlider";
import { Locales } from "../../../components/Header";
import {
  getHomeSlider,
  getInnerPage,
  getLatestOfferPage,
} from "../../../server";
import { useRouter } from "next/router";
import BtnTxt from "../../../translations/button.json";
import Fade from "react-reveal/Fade";
import Link from "next/link";
import LetUsHelp from "../../../components/LetUsHelp";
import Banner from "../../../components/Banner";

export const getStaticPaths = async () => {
  const apiResult = await getLatestOfferPage();

  let urls: String[] = [];

  apiResult.data["rows"]["Data"][0]["canvas"].forEach((item) => {
    const slugArr = item["slug url"].split("/");
    if (item["slug url"] && slugArr.length === 2) {
      urls.push(slugArr[slugArr.length - 1]);
    }
  });

  const paths = urls.map((url) => ({
    params: {
      slug: url,
    },
  }));

  // console.log("paths", paths);

  return {
    paths,
    fallback: true,
  };
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  let pageData: {
    data: object;
    error: string;
  };
  try {
    // console.log("p", params.slug);
    const res = await getInnerPage(params.slug);
    pageData = {
      data: res.data,
      error: "",
    };
  } catch (err) {
    pageData = {
      data: {},
      error: "Something Went Wrong!",
    };
  }

  // console.log("pppp", pageData);

  return {
    props: {
      pageData,
    },
    revalidate: 1,
  };
};

const OfferExpand: React.FC = ({
  pageData,
}: InferGetStaticPropsType<typeof getStaticProps>): JSX.Element => {
  const [isEn, setIsEn] = useState<boolean>(true);
  // const [page, setPage] = useState<number>(5);
  const router = useRouter();

  useEffect(() => {
    if (typeof window !== "undefined") {
      +localStorage.getItem("locale") === Locales.English && setIsEn(false);

      window.addEventListener("locale", handleLocaleEvent, false);

      return () => {
        window.removeEventListener("locale", handleLocaleEvent);
      };
    }
  }, []);

  const handleLocaleEvent = () => {
    +localStorage.getItem("locale") === Locales.English
      ? setIsEn(false)
      : setIsEn(true);
  };

  const mainData = pageData?.data?.rows?.Data[0]?.["Details data"][0];
  const othersData = pageData?.data?.rows?.Data[0]?.["May like"][0];

  if (pageData?.error) {
    <div>
      <div className="mt-5">{pageData?.error}</div>
    </div>;
  } else if (router.isFallback) {
    return <div>Loading....</div>;
  } else
    return (
      <div>
        <Banner
          name={isEn ? pageData?.title : pageData?.["title bn"]}
          cover={{
            image: pageData?.data?.rows?.Data[0]?.image,
            imagesd: pageData?.data?.rows?.Data[0]?.["image sd"],
          }}
        />

        <div className="offer-expand">
          <div className="offer-expand__cashback">
            <div className="container">
              <div className="row">
                <Fade bottom cascade>
                  <div className="col-xl-12">
                    <div className="t_head">
                      <h1
                        dangerouslySetInnerHTML={{
                          __html: isEn
                            ? mainData?.title
                            : mainData?.["title bn"],
                        }}
                      ></h1>
                      <p
                        dangerouslySetInnerHTML={{
                          __html: isEn
                            ? mainData?.["sub title"]
                            : mainData?.["sub title bn"],
                        }}
                      ></p>
                    </div>

                    <div className="offer-expand__cashback__listsec">
                      <p
                        dangerouslySetInnerHTML={{
                          __html: isEn
                            ? mainData?.["desc"]
                            : mainData?.["desc bn"],
                        }}
                      ></p>

                      <div className="listing__component-part">
                        <ul>
                          {mainData?.["key value"].map((val, key) => {
                            return (
                              <li
                                key={key}
                                dangerouslySetInnerHTML={{
                                  __html: isEn ? val?.title : val?.["title bn"],
                                }}
                              ></li>
                            );
                          })}
                        </ul>
                      </div>
                      {/* {mainData?.["key value"]?.length > page && (
                        <div className="view__more--btn text-center">
                          <a
                            onClick={() => setPage((prev) => prev + 5)}
                            className="btn right__arrow--btn primary__bg--btn inline__blck--btn"
                          >
                            {isEn ? BtnTxt.view_more.en : BtnTxt.view_more.bn}{" "}
                          </a>
                        </div>
                      )} */}
                    </div>
                  </div>
                </Fade>
              </div>
            </div>
          </div>

          {othersData?.["Under May like"].length > 0 && (
            <div className="offer-expand__liked-offer">
              <div className="container">
                <div className="row">
                  <Fade bottom cascade>
                    <div className="col-xl-12">
                      <div className="t_head">
                        <h1
                          dangerouslySetInnerHTML={{
                            __html: isEn
                              ? othersData.title
                              : othersData["title bn"],
                          }}
                        ></h1>
                        <p
                          dangerouslySetInnerHTML={{
                            __html: isEn
                              ? othersData.desc
                              : othersData["desc bn"],
                          }}
                        ></p>
                      </div>

                      <div className="offer-expand__liked-offer-list">
                        <div className="row justify-content-center c__row">
                          {othersData?.["Under May like"].map((other, key) => {
                            const slugArr = other["slug url"].split("/");

                            const url =
                              slugArr.length > 2
                                ? other["slug url"]
                                : "/" + slugArr[slugArr.length - 1];
                            return (
                              <div
                                className="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12"
                                key={key}
                              >
                                <div className="card our__blog--cardMain latest_other_card_custom">
                                  <picture>
                                    <source
                                      media="(min-width:650px)"
                                      srcSet={
                                        process.env.IMAGE_URL + other.image
                                      }
                                    />
                                    <source
                                      media="(max-width:649px)"
                                      srcSet={
                                        process.env.IMAGE_URL +
                                        other["image sd"]
                                      }
                                    />
                                    <img
                                      src={process.env.IMAGE_URL + other.image}
                                      alt="Latest offer Ucbl"
                                      className="card-img-top latest_other_custom_img"
                                    />
                                  </picture>
                                  <div className="card-body our__blog--card-body">
                                    <h5
                                      className="card-title"
                                      dangerouslySetInnerHTML={{
                                        __html: isEn
                                          ? other?.title
                                          : other?.["title bn"],
                                      }}
                                    ></h5>
                                    <p
                                      className="card-text"
                                      dangerouslySetInnerHTML={{
                                        __html: isEn
                                          ? other?.desc
                                          : other?.["desc bn"],
                                      }}
                                    ></p>
                                    <Link href={url}>
                                      <a className="ourBlog__card__exploreBtn">
                                        <span>
                                          {isEn
                                            ? BtnTxt.expolre.en
                                            : BtnTxt.expolre.bn}
                                        </span>
                                        <svg
                                          width={36}
                                          height={22}
                                          viewBox="0 0 36 22"
                                          fill="none"
                                          xmlns="http://www.w3.org/2000/svg"
                                        >
                                          <g clipPath="url(#clip0)">
                                            <path
                                              d="M24.0105 10.2671H6.71094V12.0103H24.0105V14.6251L29.7578 11.1387L24.0105 7.65234V10.2671Z"
                                              fill="#0054A6"
                                            />
                                          </g>
                                          <defs>
                                            <clipPath id="clip0">
                                              <rect
                                                width="34.5703"
                                                height="20.9183"
                                                fill="white"
                                                transform="translate(0.947266 0.679688)"
                                              />
                                            </clipPath>
                                          </defs>
                                        </svg>
                                      </a>
                                    </Link>
                                  </div>
                                </div>
                              </div>
                            );
                          })}
                        </div>
                        <div className="view__more--btn text-center">
                          <Link href="/Latest-offer">
                            <a className="btn right__arrow--btn primary__bg--btn inline__blck--btn">
                              {isEn
                                ? BtnTxt["view_all_offers"].en
                                : BtnTxt["view_all_offers"].bn}{" "}
                              <svg
                                width={40}
                                height={24}
                                viewBox="0 0 40 24"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                              >
                                <path
                                  d="M26.4575 11H6.60938V13H26.4575V16L33.0515 12L26.4575 8V11Z"
                                  fill="black"
                                />
                              </svg>
                            </a>
                          </Link>
                        </div>
                      </div>
                    </div>
                  </Fade>
                </div>
              </div>
            </div>
          )}
        </div>
        <LetUsHelp isEn={isEn} />
      </div>
    );
};

export default OfferExpand;
