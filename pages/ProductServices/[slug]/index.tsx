import React, { Fragment, useEffect, useState } from "react";
import {
  GetStaticPaths,
  GetStaticProps,
  InferGetStaticPropsType,
  GetStaticPathsResult,
} from "next";
import Banner from "../../../components/Banner";
import BillPayCard from "../../../components/BillPayCard";
import OtherServices from "../../../components/OtherServices";
import LetsHelpSlider from "../../../components/LetsHelpSlider";
import { getDropDown, getInnerPage, getPayBillPage } from "../../../server";
import { useRouter } from "next/router";
import { Locales } from "../../../components/Header";
import Fade from "react-reveal/Fade";
import Slide from "react-reveal/Slide";
import LetUsHelp from "../../../components/LetUsHelp";

export const getStaticPaths = async () => {
  return {
    paths: [{ params: { slug: "privacy-policy" } }],
    fallback: true,
  };
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  let page: {
    data: object;
    error: string;
  };
  try {
    const res = await getInnerPage(params.slug);
    page = {
      data: res.data,
      error: "",
    };
  } catch (err) {
    page = {
      data: {},
      error: "Something Went Wrong!",
    };
  }
  console.dir("props", page.data);
  return {
    props: {
      page,
    },
    revalidate: 1,
  };
};

const PolicyMain = ({ page }): JSX.Element => {
  const router = useRouter();

  const [isEn, setIsEn] = useState<boolean>(true);

  useEffect(() => {
    if (typeof window !== "undefined") {
      +localStorage.getItem("locale") === Locales.English && setIsEn(false);

      window.addEventListener("locale", handleLocaleEvent, false);

      return () => {
        window.removeEventListener("locale", handleLocaleEvent);
      };
    }
  }, []);

  const handleLocaleEvent = () => {
    +localStorage.getItem("locale") === Locales.English
      ? setIsEn(false)
      : setIsEn(true);
  };

  const pageData = page?.data?.rows?.Data[0];

  if (router.isFallback) {
    return <div>Loading...</div>;
  } else if (page?.error) {
    return <div className="container">{page?.error}</div>;
  } else
    return (
      <div>
        <Banner
          name={isEn ? pageData?.["cover title"] : pageData["cover title bn"]}
          cover={{
            image: pageData?.["cover image"],
            imagesd: pageData?.["cover image sd"],
          }}
        />
        <section>
          <div className="container">
            <div
              dangerouslySetInnerHTML={{
                __html: isEn ? pageData?.desc : pageData?.["desc bn"],
              }}
            />
          </div>
        </section>
        <LetUsHelp isEn={isEn} />
      </div>
    );
};

export default PolicyMain;
