import React, { useEffect, useState } from "react";
import Banner from "../../components/Banner";
import { GetStaticProps, InferGetStaticPropsType } from "next";
import Link from "next/link";
import Fade from "react-reveal/Fade";
import { Locales } from "../../components/Header";
import { getBlogPage } from "../../server";
import ButtonLocale from "../../translations/button.json";
import LetUsHelp from "../../components/LetUsHelp";

export const getStaticProps: GetStaticProps = async (context) => {
  let page: {
    data: object;
    error: string;
  };
  try {
    const res = await getBlogPage();
    // console.log("resss", res);
    page = {
      data: res.data,
      error: "",
    };
  } catch (err) {
    page = {
      data: {},
      error: "Something Went Wrong!",
    };
  }

  return {
    props: {
      page,
    },
    revalidate: 1,
  };
};

const Blog = ({
  page: { data, error },
}: InferGetStaticPropsType<typeof getStaticProps>): JSX.Element => {
  const pageData = data?.rows?.Data[0];
  const blogs = pageData?.["segment one"];

  const [isEn, setIsEn] = useState<boolean>(true);

  const [currPage, setCurrPage] = useState<number>(1);
  const [totalPage, setTotalPage] = useState<number>(1);

  useEffect(() => {
    if (typeof window !== "undefined") {
      +localStorage.getItem("locale") === Locales.English && setIsEn(false);

      window.addEventListener("locale", handleLocaleEvent, false);

      return () => {
        window.removeEventListener("locale", handleLocaleEvent);
      };
    }
  }, []);

  useEffect(() => {
    if (blogs) {
      setTotalPage(Math.ceil(blogs.length / 9));
    }
  }, [blogs]);

  const handleLocaleEvent = () => {
    +localStorage.getItem("locale") === Locales.English
      ? setIsEn(false)
      : setIsEn(true);
  };

  const pagination = () => {
    let pages = [];
    for (let i = 1; i <= totalPage; i++) {
      pages.push(
        <li
          className={`list-inline-item ${currPage === i && "active"}`}
          style={{ cursor: "pointer" }}
          key={i}
        >
          <a onClick={() => setCurrPage(i)} className="list-inline-link">
            {i}
          </a>
        </li>
      );
    }
    return pages;
  };

  if (error) {
    return <div>{error}</div>;
  } else
    return (
      <div>
        <Banner
          name={isEn ? pageData.title : pageData["title bn"]}
          cover={{
            image: pageData["cover image"],
            imagesd: pageData["cover image sd"],
          }}
        />
        <div className="our__blog--cardPart-main tb__padding">
          <div className="container">
            <div className="row justify-content-center">
              {blogs
                ?.slice(currPage * 9 - 9, currPage * 9)
                .map((blog, index) => {
                  const slugArr = blog["slug url"].split("/");

                  const url =
                    slugArr.length > 2
                      ? blog["slug url"]
                      : "/Blog/" + slugArr[slugArr.length - 1];
                  return (
                    <div className="col-xl-4 col-lg-4 col-md-6" key={index}>
                      <Fade bottom>
                        <div className="card our__blog--cardMain blog-card-minh">
                          {blog?.image && (
                            <picture>
                              <source
                                media="(min-width:650px)"
                                srcSet={process.env.IMAGE_URL + blog?.image}
                              />
                              <source
                                media="(max-width:649px)"
                                srcSet={
                                  process.env.IMAGE_URL + blog?.["image sd"]
                                }
                              />
                              <img
                                src={process.env.IMAGE_URL + blog?.image}
                                className="card-img-top"
                                alt="Blog in Ucbl"
                                // style={{
                                //   width: "40%",
                                //   objectFit: "cover",
                                // }}
                              />
                            </picture>
                          )}

                          <div className="card-body our__blog--card-body">
                            <h5
                              className="card-title"
                              dangerouslySetInnerHTML={{
                                __html: isEn ? blog?.title : blog?.["title bn"],
                              }}
                            />
                            <p
                              className="card-text"
                              dangerouslySetInnerHTML={{
                                __html: isEn
                                  ? blog?.desc.substring(0, 300) + "..."
                                  : blog?.["desc bn"].substring(0, 300) + "...",
                              }}
                            />

                            <Link href={url}>
                              <a className="ourBlog__card__exploreBtn">
                                <span>
                                  {isEn
                                    ? ButtonLocale.expolre.en
                                    : ButtonLocale.expolre.bn}
                                </span>
                                <svg
                                  width={36}
                                  height={22}
                                  viewBox="0 0 36 22"
                                  fill="none"
                                  xmlns="http://www.w3.org/2000/svg"
                                >
                                  <g clipPath="url(#clip0)">
                                    <path
                                      d="M24.0105 10.2671H6.71094V12.0103H24.0105V14.6251L29.7578 11.1387L24.0105 7.65234V10.2671Z"
                                      fill="#0054A6"
                                    />
                                  </g>
                                  <defs>
                                    <clipPath id="clip0">
                                      <rect
                                        width="34.5703"
                                        height="20.9183"
                                        fill="white"
                                        transform="translate(0.947266 0.679688)"
                                      />
                                    </clipPath>
                                  </defs>
                                </svg>
                              </a>
                            </Link>
                          </div>
                        </div>
                      </Fade>
                    </div>
                  );
                })}
            </div>

            {blogs.length > 0 && (
              <div className="pagination__part">
                <Fade bottom>
                  <div className="row">
                    <div className="col-xl-8 mx-auto">
                      <div className="pagination__wrap">
                        <div className="prev__btn--part">
                          <a
                            onClick={() => {
                              if (currPage > 1) setCurrPage(currPage - 1);
                            }}
                            className="prev__btn"
                            style={{ cursor: "pointer" }}
                          >
                            <svg
                              width="24"
                              height="8"
                              viewBox="0 0 24 8"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <path
                                d="M6.51294 5.27583L23.8125 5.27583V3.53264L6.51294 3.53264V0.917844L0.765623 4.40423L6.51294 7.89062V5.27583Z"
                                fill="#0054A6"
                              />
                            </svg>{" "}
                            {isEn
                              ? ButtonLocale.previous.en
                              : ButtonLocale.previous.bn}
                          </a>
                        </div>

                        <div className="pagination__number">
                          <ul className="list-inline">
                            {pagination().map((item, index) => {
                              return item;
                            })}
                          </ul>
                        </div>

                        <div className="next__btn--part">
                          <a
                            onClick={() => {
                              if (currPage < totalPage)
                                setCurrPage(currPage + 1);
                            }}
                            className="next__btn"
                            style={{ cursor: "pointer" }}
                          >
                            {isEn ? ButtonLocale.next.en : ButtonLocale.next.bn}{" "}
                            <svg
                              width="24"
                              height="8"
                              viewBox="0 0 24 8"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <path
                                d="M17.4304 3.53276H0.130859V5.27596H17.4304V7.89075L23.1777 4.40436L17.4304 0.917969V3.53276Z"
                                fill="#0054A6"
                              />
                            </svg>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </Fade>
              </div>
            )}
          </div>
        </div>

        <LetUsHelp isEn={isEn} />
      </div>
    );
};

export default Blog;
