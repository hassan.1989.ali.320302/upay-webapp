import React, { useEffect, useState } from "react";
import Banner from "../../../components/Banner";
import { GetStaticProps, InferGetStaticPropsType } from "next";
import BlogPostBasicSlider from "../../../components/BlogPostBasicSlider";
import UserPostSlider from "../../../components/UserPostSlider";
import LetUsHelp from "../../../components/LetUsHelp";
import Fade from "react-reveal/Fade";
import { getBlogPage, getInnerPage } from "../../../server";
import { Locales } from "../../../components/Header";
import Link from "next/link";
import LocaleButton from "../../../translations/button.json";
import {
  FacebookShareButton,
  LinkedinShareButton,
  FacebookIcon,
  LinkedinIcon,
} from "react-share";
import { descSeperator } from "../../../utils/BlogDescSeperator";
import { useRouter } from "next/router";

export const getStaticPaths = async () => {
  const apiResult = await getBlogPage();

  const urls = apiResult.data["rows"]["Data"][0]["segment one"].map((item) => {
    const slugArr = item["slug url"].split("/");
    if (item["slug url"] && slugArr.length === 2) {
      return slugArr[slugArr.length - 1];
    }
  });

  const paths = urls.map((url) => ({
    params: {
      slug: url,
    },
  }));

  // console.log("paths", paths);

  return {
    paths,
    fallback: true,
  };
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  let page: {
    data: object;
    blogs: any;
    error: string;
  };
  try {
    // console.log("p", params.slug);
    const blogs = await getBlogPage();
    const res = await getInnerPage(params.slug);
    page = {
      data: res.data,
      blogs: blogs.data,
      error: "",
    };
  } catch (err) {
    page = {
      data: {},
      blogs: [],
      error: "Something Went Wrong!",
    };
  }

  // console.log("pppp", page);

  return {
    props: {
      page,
    },
    revalidate: 1,
  };
};

const BlogPost = ({
  page,
}: InferGetStaticPropsType<typeof getStaticProps>): JSX.Element => {
  const router = useRouter();
  const pageData = page?.data?.rows?.Data[0];
  const content = pageData?.["segment one"][0];
  const error = page?.error;
  const relatedBlogs = page?.blogs?.rows?.Data[0]?.["segment one"]
    .filter((item) => item["slug url"] !== page?.data.slug)
    .slice(0, 4);
  // console.log(relatedBlogs);

  const [isEn, setIsEn] = useState<boolean>(true);
  const [currHref, setCurrHref] = useState<string>();

  useEffect(() => {
    if (typeof window !== "undefined") {
      +localStorage.getItem("locale") === Locales.English && setIsEn(false);

      window.addEventListener("locale", handleLocaleEvent, false);

      setCurrHref(window.location.href);

      return () => {
        window.removeEventListener("locale", handleLocaleEvent);
      };
    }
  }, []);

  const handleLocaleEvent = () => {
    +localStorage.getItem("locale") === Locales.English
      ? setIsEn(false)
      : setIsEn(true);
  };

  if (router.isFallback) {
    return <div>Loading...</div>;
  } else if (error) {
    return <div>{error}</div>;
  } else
    return (
      <div>
        <Banner
          name={isEn ? pageData?.title : pageData?.["title bn"]}
          cover={{
            image: pageData?.["cover image"],
            imagesd: pageData?.["cover image sd"],
          }}
        />

        <div className="blog__post--wrapper--main tb__padding">
          <div className="container">
            <div className="row">
              <div className="col-xl-12">
                <Fade left cascade>
                  <div className="blog__post-topDesc">
                    <h2
                      dangerouslySetInnerHTML={{
                        __html: isEn ? content?.title : content?.["title bn"],
                      }}
                    />
                    {content && (
                      <p
                        dangerouslySetInnerHTML={{
                          __html: isEn
                            ? descSeperator(content).first
                            : descSeperator(content, "bn").first,
                        }}
                      />
                    )}

                    <div className="large--imgPart">
                      {content?.image && (
                        <picture>
                          <source
                            media="(min-width:650px)"
                            srcSet={process.env.IMAGE_URL + content?.image}
                          />
                          <source
                            media="(max-width:649px)"
                            srcSet={
                              process.env.IMAGE_URL + content?.["image sd"]
                            }
                          />
                          <img
                            src={process.env.IMAGE_URL + content?.image}
                            alt="Blog Ucbl"
                          />
                        </picture>
                      )}
                    </div>
                    {content && (
                      <p
                        dangerouslySetInnerHTML={{
                          __html: isEn
                            ? descSeperator(content).last
                            : descSeperator(content, "bn").last,
                        }}
                      />
                    )}

                    <div className="blog__post-sidePart">
                      <div className="row">
                        <div className="col-xl-6 col-lg-6 blog__post-sidePart-leftCol">
                          <div className="left-description">
                            <h3
                              dangerouslySetInnerHTML={{
                                __html: isEn
                                  ? content?.["sub title"]
                                  : content?.["sub title bn"],
                              }}
                            />
                            <p
                              dangerouslySetInnerHTML={{
                                __html: isEn
                                  ? content?.["sub desc"]
                                  : content?.["sub desc bn"],
                              }}
                            />
                          </div>
                        </div>
                        <div className="col-xl-6 col-lg-6">
                          <div className="right--imgPart">
                            {content?.["sub image"] && (
                              <picture>
                                <source
                                  media="(min-width:650px)"
                                  srcSet={
                                    process.env.IMAGE_URL +
                                    content?.["sub image"]
                                  }
                                />
                                <source
                                  media="(max-width:649px)"
                                  srcSet={
                                    process.env.IMAGE_URL +
                                    content?.["sub image sd"]
                                  }
                                />
                                <img
                                  src={
                                    process.env.IMAGE_URL +
                                    content?.["sub image"]
                                  }
                                  alt="Blog Ucbl"
                                />
                              </picture>
                            )}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </Fade>
              </div>
            </div>

            <div className="blog__post-simpleImg-slider">
              <div className="row">
                <div className="col-xl-10 col-sm-10 col-10 mx-auto">
                  {pageData?.["Segment two"].length > 0 && (
                    <BlogPostBasicSlider sliders={pageData?.["Segment two"]} />
                  )}
                </div>
              </div>
            </div>

            <div className="blog__post--bottomPart">
              <div className="row">
                {/* <div className="col-xl-8 col-lg-7 blog__post--bottomPart-leftCol">
                  <div className="left__form--part">
                    <h4>Post a Comment</h4>
                    <form>
                      <div className="form-row">
                        <div className="form-group col-md-6 custom__input-form-group">
                          <input
                            type="email"
                            className="form-control custom-form-control"
                            id="exampleInputEmail1"
                            aria-describedby="emailHelp"
                            placeholder="Enter email"
                          />
                        </div>
                        <div className="form-group col-md-6 custom__input-form-group">
                          <input
                            type="email"
                            className="form-control custom-form-control"
                            id="exampleInputEmail1"
                            aria-describedby="emailHelp"
                            placeholder="Enter email"
                          />
                        </div>
                        <div className="form-group col-md-12 custom__textarea-form-group">
                          <textarea
                            className="form-control custom-form-control"
                            id="exampleFormControlTextarea1"
                            rows={5}
                            defaultValue={""}
                          />
                        </div>
                      </div>
                      <div className="full__width--btnPart">
                        <button
                          type="submit"
                          className="btn custom__submit__btn blue__bg--btn fullWidth__btn"
                        >
                          Submit
                        </button>
                      </div>
                    </form>
                  </div>
                </div> */}
                <div className="col-xl-4 col-lg-5 blog__post--bottomPart-rightCol">
                  <div className="right__part__userPost">
                    <div className="slider__Wrap">
                      <Fade bottom cascade>
                        <div className="top__part">
                          <p
                            dangerouslySetInnerHTML={{
                              __html: isEn
                                ? pageData?.["Segment three"][0]?.title
                                : pageData?.["Segment three"][0]?.["title bn"],
                            }}
                          ></p>
                          <div className="social__icon">
                            <FacebookShareButton
                              title="Facebook"
                              url={currHref}
                            >
                              <FacebookIcon size={24} round={true} />
                            </FacebookShareButton>
                            <LinkedinShareButton
                              title="LinkedIn"
                              url={currHref}
                            >
                              <LinkedinIcon size={24} round={true} />
                            </LinkedinShareButton>
                          </div>
                        </div>
                      </Fade>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <section>
          <div className="related__post--mainPart tb__padding">
            <div className="container">
              <div className="row">
                <Fade bottom>
                  <div className="col-xl-12">
                    <div className="dashmid__wDesc__header--part">
                      <h1
                        dangerouslySetInnerHTML={{
                          __html: isEn
                            ? pageData?.["Segment four"][0]?.title
                            : pageData?.["Segment four"][0]?.["title bn"],
                        }}
                      />
                      <p
                        dangerouslySetInnerHTML={{
                          __html: isEn
                            ? pageData?.["Segment four"][0]?.desc
                            : pageData?.["Segment four"][0]?.["desc bn"],
                        }}
                      />
                    </div>

                    <div className="related__card--section">
                      <div className="row related__card--section-row">
                        {relatedBlogs?.map((related, index) => {
                          const url = related["slug url"].split("/")[
                            related["slug url"].split("/").length - 1
                          ];
                          return (
                            <div
                              className="col-xl-3 col-lg-3 col-md-6 col-sm-7 col-8 custom__col"
                              key={index}
                            >
                              <div className="card our__blog--cardMain related-custom-size">
                                {related?.image && (
                                  <picture>
                                    <source
                                      media="(min-width:650px)"
                                      srcSet={
                                        process.env.IMAGE_URL + related?.image
                                      }
                                    />
                                    <source
                                      media="(max-width:649px)"
                                      srcSet={
                                        process.env.IMAGE_URL +
                                        related?.["image sd"]
                                      }
                                    />
                                    <img
                                      src={
                                        process.env.IMAGE_URL + related?.image
                                      }
                                      alt="Blog Ucbl"
                                      className="card-img-top"
                                    />
                                  </picture>
                                )}

                                <div className="card-body related__post--card-body">
                                  <h5
                                    className="card-title"
                                    dangerouslySetInnerHTML={{
                                      __html: isEn
                                        ? related?.title
                                        : related?.["title bn"],
                                    }}
                                  />
                                  <p
                                    className="card-text"
                                    dangerouslySetInnerHTML={{
                                      __html: isEn
                                        ? related?.desc
                                        : related?.["desc bn"],
                                    }}
                                  />
                                  <Link href={`/Blog/${url}`}>
                                    <a className="ourBlog__card__exploreBtn">
                                      <span>
                                        {isEn
                                          ? LocaleButton.expolre.en
                                          : LocaleButton.expolre.bn}
                                      </span>
                                      <svg
                                        width="17"
                                        height="6"
                                        viewBox="0 0 17 6"
                                        fill="none"
                                        xmlns="http://www.w3.org/2000/svg"
                                      >
                                        <path
                                          d="M12.6784 1.93292H0.167969V3.19353H12.6784V5.08446L16.8346 2.56322L12.6784 0.0419922V1.93292Z"
                                          fill="#0054A6"
                                        />
                                      </svg>
                                    </a>
                                  </Link>
                                </div>
                              </div>
                            </div>
                          );
                        })}
                      </div>
                    </div>

                    <div className="right__arrow--btnPart">
                      <Link href="/Blog">
                        <a className="btn right__arrow--btn secondaryD__bg--btn inline__blck--btn">
                          {isEn
                            ? LocaleButton.all_post.en
                            : LocaleButton.all_post.bn}{" "}
                          <svg
                            width="28"
                            height="8"
                            viewBox="0 0 28 8"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              d="M20.4594 3H0.611328V5H20.4594V8L27.0534 4L20.4594 0V3Z"
                              fill="white"
                            />
                          </svg>
                        </a>
                      </Link>
                    </div>
                  </div>
                </Fade>
              </div>
            </div>
          </div>
        </section>

        <LetUsHelp isEn={isEn} />
      </div>
    );
};

export default BlogPost;
