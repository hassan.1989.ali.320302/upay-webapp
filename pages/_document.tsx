import Document, {
  Html,
  Head,
  Main,
  NextScript,
  DocumentContext,
} from "next/document";

class MyDocument extends Document {
  static async getInitialProps(ctx: DocumentContext) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }

  render() {
    return (
      <Html>
        <Head>
          <script
            async
            dangerouslySetInnerHTML={{
              __html: `
                (function(w,d,s,l,i){
                      w[l]=w[l]||[];
                      w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});
                      var f=d.getElementsByTagName(s)[0],

                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

                })(window,document,'script','dataLayer','GTM-PQ9R53T');
                `,
            }}
          />
        </Head>
        <body>
          <noscript
            dangerouslySetInnerHTML={{
              __html: `
              <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PQ9R53T"

              height="0" width="0" style="display:none;visibility:hidden"></iframe>
`,
            }}
          />
          <Main />
          <NextScript />
          <script
            dangerouslySetInnerHTML={{
              __html: `
                window.ZEROCIUM_WEBCHAT_ID =
                  "ce8edd35-41b1-4fde-b7b8-78f4c8cbbd2e";
                (function () {
                  let d = document;
                  let s = d.createElement("script");
                  s.src = "https://webchat.zerocium.com/js/zcwebchat.js";
                  s.async = 1;
                  d.getElementsByTagName("body")[0].appendChild(s);
                })();
            `,
            }}
          />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
