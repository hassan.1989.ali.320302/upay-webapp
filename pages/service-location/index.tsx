import React, { useEffect, useRef, useState } from "react";
import Banner from "../../components/Banner";
import LetsHelpSlider from "../../components/LetsHelpSlider";
import { GetStaticProps, InferGetStaticPropsType } from "next";
import { getServiceLocationPage } from "../../server";
import { Locales } from "../../components/Header";
import Map from "../../components/Map";
import LetUsHelp from "../../components/LetUsHelp";
import Image from "next/image";

export const getStaticProps: GetStaticProps = async (context) => {
  let page: {
    data: object;
    error: string;
  };
  try {
    const res = await getServiceLocationPage();
    page = {
      data: await res.data,
      error: "",
    };
  } catch (err) {
    page = {
      data: {},
      error: "Something Went Wrong!",
    };
  }

  return {
    props: {
      page,
    },
    revalidate: 1,
  };
};

const Location = ({
  page: { data, error },
}: InferGetStaticPropsType<typeof getStaticProps>): JSX.Element => {
  // const pageData = data.rows.Data[0];
  // const atm = pageData["ATM"];
  // const merchant = pageData["Merchant"];
  // const agent = pageData["Agent"];

  // const [isListView, setIsListView] = useState<boolean | null>(null);
  // const [isEn, setIsEn] = useState<boolean>(true);
  // const [selected, setSelected] = useState<string>("atm");
  // const [selectList, setSelectList] = useState(atm);
  // const [selectedLocation, setSelectedLocation] = useState<Number>(0);
  // const [searchTerm, setSearchTerm] = useState<string>("");

  // let child = useRef(null);

  // useEffect(() => {
  //   if (typeof window !== "undefined") {
  //     const screen = window.screen.width;
  //     if (screen < 576) {
  //       setIsListView(true);
  //     }

  //     +localStorage.getItem("locale") === Locales.English && setIsEn(false);

  //     window.addEventListener("locale", handleLocaleEvent, false);

  //     return () => {
  //       window.removeEventListener("locale", handleLocaleEvent);
  //     };
  //   }
  // }, []);

  // useEffect(() => {
  //   if (typeof window !== "undefined") {
  //     if (isListView === true) {
  //       let map: any = document.querySelector("#mob_map");
  //       map.style.display = "none";
  //       let list: any = document.querySelector("#mob_list");
  //       list.style.display = "block";
  //     }
  //     if (isListView === false) {
  //       let list: any = document.querySelector("#mob_list");
  //       list.style.display = "none";
  //       let map: any = document.querySelector("#mob_map");
  //       map.style.display = "block";
  //     }
  //   }
  // }, [isListView]);

  // useEffect(() => {
  //   if (searchTerm.length > 0) {
  //     let res: [];
  //     if (selected === "atm") {
  //       res = atm.filter(
  //         (item) =>
  //           item.title.toLowerCase().includes(searchTerm.toLowerCase()) ||
  //           item.location.toLowerCase().includes(searchTerm.toLowerCase())
  //       );
  //     } else if (selected === "merchant") {
  //       res = merchant.filter(
  //         (item) =>
  //           item.title.toLowerCase().includes(searchTerm.toLowerCase()) ||
  //           item.location.toLowerCase().includes(searchTerm.toLowerCase())
  //       );
  //     } else {
  //       res = agent.filter(
  //         (item) =>
  //           item.title.toLowerCase().includes(searchTerm.toLowerCase()) ||
  //           item.location.toLowerCase().includes(searchTerm.toLowerCase())
  //       );
  //     }
  //     if (res.length > 0) {
  //       setSelectList(res);
  //     }
  //   } else {
  //     selected === "atm"
  //       ? setSelectList(atm)
  //       : selected === "merchant"
  //       ? setSelectList(merchant)
  //       : setSelectList(agent);
  //   }
  // }, [searchTerm]);

  // const handleLocaleEvent = () => {
  //   +localStorage.getItem("locale") === Locales.English
  //     ? setIsEn(false)
  //     : setIsEn(true);
  // };

  // const handleSelection = (option) => {
  //   setSelected(option);
  //   option === "atm"
  //     ? setSelectList(atm)
  //     : option === "merchant"
  //     ? setSelectList(merchant)
  //     : setSelectList(agent);
  // };

  // if (error) {
  //   return <div>{error}</div>;
  // } else
  return (
    <div>
      {/* <Banner
          name={isEn ? pageData.title : pageData["title bn"]}
          cover={{
            image: pageData["cover image"],
            imagesd: pageData["cover image sd"],
          }}
        /> */}

      {/* <div className="location">
          <div className="container-fluid">
            <div className="row">
              <div className="col-xl-3 p-0">
                <div className="location__selection">
                  <h3>LOCATE US</h3>
                  <div className="location__selection-list">
                    <ul className="list-lnline">
                      <li
                        className={
                          selected === "atm"
                            ? "list-inline-item active"
                            : "list-inline-item"
                        }
                      >
                        <a
                          onClick={() => handleSelection("atm")}
                          className="list-inline-link"
                          style={{ cursor: "pointer" }}
                        >
                          ATM
                        </a>
                      </li>
                      <li
                        className={
                          selected === "merchant"
                            ? "list-inline-item active"
                            : "list-inline-item"
                        }
                      >
                        <a
                          onClick={() => handleSelection("merchant")}
                          className="list-inline-link"
                          style={{ cursor: "pointer" }}
                        >
                          MERCHANT
                        </a>
                      </li>
                      <li
                        className={
                          selected === "agent"
                            ? "list-inline-item active"
                            : "list-inline-item"
                        }
                      >
                        <a
                          onClick={() => handleSelection("agent")}
                          className="list-inline-link"
                          style={{ cursor: "pointer" }}
                        >
                          AGENT
                        </a>
                      </li>
                    </ul>

                    {(isListView === null || isListView === true) && (
                      <div className="form-group">
                        <div className="input-group">
                          <input
                            className="form-control"
                            placeholder="Search"
                            name="Search"
                            value={searchTerm}
                            //   autoComplete="on"
                            //   defaultValue
                            onChange={(e) => setSearchTerm(e.target.value)}
                          />

                          <a className="input-group-append">
                            <span className="input-group-text">
                              <svg
                                width={13}
                                height={13}
                                viewBox="0 0 14 14"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                              >
                                <path
                                  fillRule="evenodd"
                                  clipRule="evenodd"
                                  d="M13.5512 0.448971C13.8832 0.780917 13.8832 1.31911 13.5512 1.65105L1.65124 13.5511C1.31929 13.883 0.7811 13.883 0.449155 13.5511C0.117209 13.2191 0.117209 12.6809 0.449155 12.349L12.3492 0.448971C12.6811 0.117026 13.2193 0.117026 13.5512 0.448971Z"
                                  fill="black"
                                  fillOpacity="0.4"
                                />
                                <path
                                  fillRule="evenodd"
                                  clipRule="evenodd"
                                  d="M0.449155 0.448971C0.7811 0.117026 1.31929 0.117026 1.65124 0.448971L13.5512 12.349C13.8832 12.6809 13.8832 13.2191 13.5512 13.5511C13.2193 13.883 12.6811 13.883 12.3492 13.5511L0.449155 1.65105C0.117209 1.31911 0.117209 0.780917 0.449155 0.448971Z"
                                  fill="black"
                                  fillOpacity="0.4"
                                />
                              </svg>
                            </span>
                          </a>

                          <a className="input-group-append">
                            <span
                              className="input-group-text"
                              id="basic-addon1"
                            >
                              <svg
                                width={21}
                                height={21}
                                viewBox="0 0 21 21"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                              >
                                <path
                                  d="M19.7142 18.2942L16.0342 14.6142C17.4689 12.8167 18.161 10.5381 17.9684 8.24625C17.7758 5.95443 16.7131 3.8233 14.9985 2.29043C13.2839 0.757552 11.0475 -0.0607544 8.74849 0.00351862C6.44949 0.0677916 4.26232 1.00977 2.63604 2.63604C1.00977 4.26232 0.0677916 6.44949 0.00351862 8.74849C-0.0607544 11.0475 0.757552 13.2839 2.29043 14.9985C3.8233 16.7131 5.95443 17.7758 8.24625 17.9684C10.5381 18.161 12.8167 17.4689 14.6142 16.0342L18.2942 19.7142C18.3872 19.808 18.4978 19.8824 18.6197 19.9331C18.7415 19.9839 18.8722 20.01 19.0042 20.01C19.1362 20.01 19.267 19.9839 19.3888 19.9331C19.5107 19.8824 19.6213 19.808 19.7142 19.7142C19.808 19.6213 19.8824 19.5107 19.9331 19.3888C19.9839 19.267 20.01 19.1362 20.01 19.0042C20.01 18.8722 19.9839 18.7415 19.9331 18.6197C19.8824 18.4978 19.808 18.3872 19.7142 18.2942ZM2.00423 9.00424C2.00933 7.85743 2.2961 6.72944 2.83935 5.71945C3.38259 4.70947 4.16566 3.84842 5.11969 3.21202C6.07373 2.57562 7.16952 2.18337 8.31069 2.06976C9.45186 1.95615 10.6035 2.12465 11.6643 2.56046C12.725 2.99626 13.6625 3.68602 14.3942 4.56908C15.1259 5.45213 15.6295 6.50144 15.8606 7.62473C16.0917 8.74802 16.0432 9.91088 15.7196 11.0111C15.3959 12.1113 14.8068 13.1151 14.0042 13.9342C13.0212 14.9095 11.771 15.5713 10.4118 15.8359C9.05265 16.1005 7.6455 15.956 6.36842 15.4208C5.09133 14.8856 4.00168 13.9836 3.2373 12.829C2.47293 11.6743 2.06817 10.3189 2.07423 8.93424L2.00423 9.00424Z"
                                  fill="black"
                                />
                              </svg>
                            </span>
                          </a>
                        </div>
                      </div>
                    )}
                  </div>

                  <div id="mob_list">
                    <div className="location__selection-options">
                      <ul>
                        {selectList.map((item, index) => {
                          return (
                            <li key={index}>
                              <a
                                onClick={() => {
                                  setSelectedLocation(index);
                                  // console.log("df", child);
                                  child.current.handle(index);
                                }}
                                className="location__link-card"
                                style={{ cursor: "pointer" }}
                              >
                                <div className="media">
                                  <div
                                    style={{ width: "70px", height: "70px" }}
                                    className="d-flex flex-row align-items-center"
                                  >
                                    <picture>
                                      <source
                                        media="(min-width:650px)"
                                        srcSet={
                                          process.env.IMAGE_URL + item?.icon
                                        }
                                      />
                                      <source
                                        media="(max-width:649px)"
                                        srcSet={
                                          process.env.IMAGE_URL +
                                          item?.["icon sd"]
                                        }
                                      />
                                      <img
                                        src={process.env.IMAGE_URL + item?.icon}
                                        alt="location"
                                        className="align-self-center mr-3"
                                        style={{
                                          width: "70%",
                                        }}
                                      />
                                    </picture>
                                  </div>
                                  <div className="media-body">
                                    <div className="area_desc">
                                      <h5
                                        className="mt-0"
                                        dangerouslySetInnerHTML={{
                                          __html: isEn
                                            ? item?.title
                                            : item?.["title bn"],
                                        }}
                                      ></h5>
                                      <p
                                        className="loct"
                                        dangerouslySetInnerHTML={{
                                          __html: isEn
                                            ? item?.location
                                            : item?.["location bn"],
                                        }}
                                      ></p>
                                      <p
                                        className="time"
                                        dangerouslySetInnerHTML={{
                                          __html: isEn
                                            ? item?.date
                                            : item?.["date bn"],
                                        }}
                                      ></p>
                                    </div>
                                    <div className="icon_part">
                                      <svg
                                        width="20"
                                        height="20"
                                        viewBox="0 0 20 20"
                                        fill="none"
                                        xmlns="http://www.w3.org/2000/svg"
                                      >
                                        <g clipPath="url(#clip0)">
                                          <path
                                            d="M7.12969 19.2419L15.9905 11.0105L7.1297 2.77896"
                                            stroke="#4E4E50"
                                            strokeWidth="2.80909"
                                          />
                                        </g>
                                        <defs>
                                          <clipPath id="clip0">
                                            <rect
                                              width="20"
                                              height="20"
                                              fill="white"
                                            />
                                          </clipPath>
                                        </defs>
                                      </svg>
                                    </div>
                                  </div>
                                </div>
                              </a>
                            </li>
                          );
                        })}
                      </ul>
                    </div>

                    <div className="br__gray">
                      <div className="location__selection__btn--part mbl__vsbl">
                        <ul className="list-inline">
                          <li className="list-inline-item active">
                            <a
                              onClick={() => setIsListView(true)}
                              className="list-inline-link"
                            >
                              List View
                            </a>
                          </li>
                          <li className="list-inline-item">
                            <a
                              onClick={() => setIsListView(false)}
                              className="list-inline-link"
                            >
                              Map View
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-xl-9 p-0" id="mob_map">
                <div className="location__map">
                  {selectList.length > 0 && (
                    <Map
                      data={selectList}
                      selected={selectedLocation}
                      onRef={(ref) => (child.current = ref)}
                    />
                  )}
                  <div className="location__selection__btn--part mbl__vsbl">
                    <ul className="list-inline">
                      <li className="list-inline-item ">
                        <a
                          onClick={() => setIsListView(true)}
                          className="list-inline-link"
                        >
                          List View
                        </a>
                      </li>
                      <li className="list-inline-item active">
                        <a
                          onClick={() => setIsListView(false)}
                          className="list-inline-link"
                        >
                          Map View
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div> */}

      {/* <LetUsHelp isEn={isEn} /> */}
      <div className="text-center mt-5 mb-5">
        <Image src="/images/soon1.png" width={800} height={"auto"} />
      </div>
    </div>
  );
};

export default Location;
