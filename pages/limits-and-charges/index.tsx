import React, { useEffect, useState } from "react";
import Banner from "../../components/Banner";
import { GetStaticProps, InferGetStaticPropsType } from "next";
import { Locales } from "../../components/Header";
import { getCalc, getChargesPage, getChargesTable } from "../../server";
import LetUsHelp from "../../components/LetUsHelp";
import { calculate, noChargeCheck } from "../../utils/Calculation";
import Fade from "react-reveal/Fade";

export const getStaticProps: GetStaticProps = async () => {
  let page: {
    data: object;
    calc: [];
    table: object;
    error: string;
  };
  try {
    const res = await getChargesPage();
    const calc = await getCalc();
    const table = await getChargesTable();
    page = {
      data: res.data,
      calc: calc.data,
      table: table.data,
      error: "",
    };
  } catch (err) {
    page = {
      data: {},
      calc: [],
      table: {},
      error: "Something Went Wrong!",
    };
  }

  // console.log("pppp", page);

  return {
    props: {
      page,
    },
    revalidate: 1,
  };
};

const RateCharge = ({
  page: { data, calc, table, error },
}: InferGetStaticPropsType<typeof getStaticProps>): JSX.Element => {
  const pageData = data?.rows?.Data[0];
  const [isEn, setIsEn] = useState<boolean>(true);
  const [services, setServices] = useState<string[]>(null);
  const [selectedService, setSelectedService] = useState<string | null>(null);
  const [transTypes, setTransTypes] = useState<string[] | null>(null);
  const [selectedTrans, setSelectedTrans] = useState<string | null>(null);
  const [amount, setAmount] = useState<number>(0);
  const [charge, setCharge] = useState<number>(0);
  const [total, setTotal] = useState<number>(0);
  const [noCharge, setNoCharge] = useState<boolean>(false);
  //trigger to calculate when same transaction type ("")
  const [trigger, setTrigger] = useState<boolean>(false);

  useEffect(() => {
    if (typeof window !== "undefined") {
      +localStorage.getItem("locale") === Locales.English && setIsEn(false);

      window.addEventListener("locale", handleLocaleEvent, false);

      return () => {
        window.removeEventListener("locale", handleLocaleEvent);
      };
    }
  }, []);

  useEffect(() => {
    if (calc) {
      let sevs = calc?.map((it) => it["service_name"]);
      let uniqueSevs: string[] = [...Array.from(new Set<string>(sevs))];
      // console.log("uqe", uniqueSevs);
      setServices(uniqueSevs);
      setSelectedService(uniqueSevs[0]);
    }
  }, []);

  useEffect(() => {
    let trans = calc
      ?.filter((it) => it["service_name"] === selectedService)
      .map((it) => it["transaction_type"]);
    // console.log("trans", trans);
    let uniqueTrans: string[] = [...Array.from(new Set<string>(trans))];
    setTransTypes(uniqueTrans);
    setSelectedTrans(uniqueTrans[0]);
    if (uniqueTrans[0] === "") {
      setTrigger(!trigger);
    }
    setCharge(0);
    // setTotal(0);
  }, [selectedService]);

  useEffect(() => {
    // console.log("hereeeeeeeeeeeeeeeeeee");
    setCharge(0);
    // setTotal(0);
    if (selectedService) {
      const res = noChargeCheck(calc, selectedService, selectedTrans);
      setNoCharge(res);
      if (!res && amount > 0) {
        setTimeout(() => {
          const { charge, total } = calculate(
            calc,
            selectedService,
            selectedTrans,
            amount
          );
          setCharge(charge);
          setTotal(total);
        }, 0);
      }
      if (res) {
        setTotal(amount);
      }
    }
  }, [amount, selectedTrans, trigger]);

  useEffect(() => {
    console.log(selectedTrans);
  }, [selectedTrans]);

  const handleLocaleEvent = () => {
    +localStorage.getItem("locale") === Locales.English
      ? setIsEn(false)
      : setIsEn(true);
  };

  if (error) {
    return <div>{error}</div>;
  } else
    return (
      <div>
        <Banner
          name={isEn ? pageData?.title : pageData?.["title bn"]}
          cover={{
            image: pageData?.["cover image"],
            imagesd: pageData?.["cover image sd"],
          }}
        />
        <div className="limit-frequency">
          <Fade bottom>
            <div className="t_head">
              <h1
                dangerouslySetInnerHTML={{
                  __html: isEn
                    ? pageData?.["segment one"][0]?.title
                    : pageData?.["segment one"][0]?.["title bn"],
                }}
              />
              <p
                dangerouslySetInnerHTML={{
                  __html: isEn
                    ? pageData?.["segment one"][0]?.desc
                    : pageData?.["segment one"][0]?.["desc bn"],
                }}
              />
            </div>
          </Fade>
          <div className="limit_part-section">
            <div className="container">
              <div className="row">
                <div className="col-xl-10 mx-auto">
                  <form>
                    <div className="form-row">
                      <Fade left>
                        <div className="form-group col-md-6 select_t_default">
                          <label htmlFor="select_t_default">
                            What Services you looking for?
                          </label>
                          <div className="custom_select_wrap">
                            <select
                              className="form-control"
                              id="select_t_default"
                              onChange={(e) =>
                                setSelectedService(e.target.value)
                              }
                            >
                              {services?.map((service, index) => (
                                <option
                                  key={index}
                                  value={service}
                                  // selected={selectedService === service}
                                  dangerouslySetInnerHTML={{ __html: service }}
                                ></option>
                              ))}
                            </select>
                            <span className="custom_arrow" />
                          </div>
                        </div>
                      </Fade>
                      <Fade right>
                        <div className="form-group col-md-6 select_t_default">
                          <label htmlFor="select_t_default">
                            Select Services
                          </label>
                          <div className="custom_select_wrap">
                            <select
                              className="form-control"
                              id="select_t_default"
                              onChange={(e) => setSelectedTrans(e.target.value)}
                            >
                              {transTypes?.map((trans, index) => (
                                <option
                                  key={index}
                                  value={trans}
                                  selected={selectedTrans === trans}
                                  dangerouslySetInnerHTML={{ __html: trans }}
                                ></option>
                              ))}
                            </select>
                            <span className="custom_arrow" />
                          </div>
                        </div>
                      </Fade>
                      <Fade left>
                        <div className="form-group col-md-6 select_t_default">
                          <label htmlFor="select_t_default">Amount (BDT)</label>
                          <div className="input-group custom__input-group">
                            <input
                              type="number"
                              className="form-control c__form-control"
                              placeholder="0"
                              defaultValue={amount}
                              disabled={noCharge}
                              aria-label="Amount"
                              aria-describedby="basic-addon1"
                              onChange={(e) => setAmount(+e.target.value)}
                            />
                            <div className="input-group-prepend">
                              <span
                                className="input-group-text"
                                id="basic-addon1"
                              >
                                TAKA
                              </span>
                            </div>
                          </div>
                        </div>
                      </Fade>
                      <Fade right>
                        <div className="form-group col-md-6 select_t_default">
                          <label htmlFor="select_t_default">Charge (BDT)</label>
                          <div className="input-group custom__input-group">
                            <input
                              type="text"
                              className="form-control c__form-control"
                              placeholder="Charge"
                              value={charge}
                              readOnly
                              aria-label="Charge"
                              aria-describedby="basic-addon1"
                            />
                            <div className="input-group-prepend">
                              <span
                                className="input-group-text"
                                id="basic-addon1"
                              >
                                TAKA
                              </span>
                            </div>
                          </div>
                        </div>
                      </Fade>
                    </div>
                  </form>
                  <div className="row">
                    <div className="col-xl-6 mx-auto">
                      <div className="main__amount__show">
                        <div className="top__part">
                          <p>Total (including charge)</p>
                        </div>
                        <div className="bottom__part">
                          <p>
                            <span>{total?.toLocaleString()}</span> Taka
                          </p>
                        </div>
                        {noCharge && (
                          <div className="text-danger">
                            *No Charges Applicable
                          </div>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="charge__sec">
            <div className="container">
              <div className="row">
                <div className="col-xl-12">
                  <Fade bottom>
                    <div className="t_mid">
                      <h1
                        dangerouslySetInnerHTML={{
                          __html: isEn
                            ? pageData?.["segment two"][0]?.title
                            : pageData?.["segment two"][0]?.["title bn"],
                        }}
                      />
                      <p
                        dangerouslySetInnerHTML={{
                          __html: isEn
                            ? pageData?.["segment two"][0]?.desc
                            : pageData?.["segment two"][0]?.["desc bn"],
                        }}
                      />
                    </div>
                  </Fade>

                  {/* <div className="charge__table">
                    <div className="bill__pay--table--part">
                      <div className="table-responsive">
                        <table className="table table-bordered bill__pay-table">
                          <thead>
                            <tr>
                              <th
                                rowSpan={2}
                                scope="col"
                                className="start_t-head"
                              >
                                Service Type
                              </th>
                              <th scope="col" colSpan={2}>
                                Per Transaction Limit (Amount in BDT)
                              </th>
                              <th scope="col" colSpan={2}>
                                Daily Transaction Limit (Amount in BDT)
                              </th>
                              <th scope="col" colSpan={2}>
                                Monthly Transaction Limit (Amount in BDT)
                              </th>
                            </tr>
                            <tr>
                              <th scope="col">Min</th>
                              <th scope="col">Max</th>
                              <th scope="col">Count</th>
                              <th scope="col">Amount</th>
                              <th scope="col">Count</th>
                              <th scope="col">Amount</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>Add Money from Bank</td>
                              <td className="text-center" rowSpan={3}>
                                50
                              </td>
                              <td className="text-center" rowSpan={3}>
                                30000
                              </td>
                              <td className="text-center" rowSpan={3}>
                                5
                              </td>
                              <td className="text-center" rowSpan={3}>
                                30000
                              </td>
                              <td className="text-center" rowSpan={3}>
                                5
                              </td>
                              <td className="text-center" rowSpan={3}>
                                30000
                              </td>
                            </tr>
                            <tr>
                              <td>Add Money from Credit Card</td>
                            </tr>
                            <tr>
                              <td>Cash In from Agent</td>
                            </tr>
                            <tr>
                              <td>Cash Out from Agent</td>
                              <td className="text-center">50</td>
                              <td className="text-center" rowSpan={2}>
                                25000
                              </td>
                              <td className="text-center" rowSpan={2}>
                                50
                              </td>
                              <td className="text-center" rowSpan={2}>
                                25000
                              </td>
                              <td className="text-center" rowSpan={2}>
                                50
                              </td>
                              <td className="text-center" rowSpan={2}>
                                25000
                              </td>
                            </tr>
                            <tr>
                              <td>Cash Out from ATM</td>
                              <td className="text-center">2000</td>
                            </tr>
                            <tr>
                              <td>Add Money from Bank</td>
                              <td className="text-center" rowSpan={2}>
                                50
                              </td>
                              <td className="text-center" rowSpan={2}>
                                30000
                              </td>
                              <td className="text-center" rowSpan={2}>
                                5
                              </td>
                              <td className="text-center" rowSpan={2}>
                                30000
                              </td>
                              <td className="text-center" rowSpan={2}>
                                5
                              </td>
                              <td className="text-center" rowSpan={2}>
                                30000
                              </td>
                            </tr>
                            <tr>
                              <td>Transfer Money to Bank Account</td>
                            </tr>
                            <tr>
                              <td>Merchant Payment</td>
                              <td className="text-center">10</td>
                              <td className="text-center" colSpan={5}>
                                No Limit
                              </td>
                            </tr>
                            <tr>
                              <td>Payment</td>
                              <td className="text-center">10</td>
                              <td className="text-center" colSpan={5}>
                                No Limit
                              </td>
                            </tr>
                            <tr>
                              <td>Pay Bill</td>
                              <td className="text-center" colSpan={6}>
                                No Limit
                              </td>
                            </tr>
                            <tr>
                              <td>Mobile Recharge*</td>
                              <td className="text-center">50</td>
                              <td className="text-center">30000</td>
                              <td className="text-center">5</td>
                              <td className="text-center">30000</td>
                              <td className="text-center">5</td>
                              <td className="text-center">30000</td>
                            </tr>
                            <tr>
                              <td>Mobile Recharge*</td>
                              <td className="text-center">50</td>
                              <td className="text-center">30000</td>
                              <td className="text-center">5</td>
                              <td className="text-center">30000</td>
                              <td className="text-center">5</td>
                              <td className="text-center">30000</td>
                            </tr>
                            <tr>
                              <td>Mobile Recharge*</td>
                              <td className="text-center">50</td>
                              <td className="text-center">30000</td>
                              <td className="text-center">5</td>
                              <td className="text-center">30000</td>
                              <td className="text-center">5</td>
                              <td className="text-center">30000</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div> */}
                  <Fade bottom>
                    <div
                      dangerouslySetInnerHTML={{
                        __html:
                          table?.rows?.Data[0]?.["table data"][0]?.[
                            "charge table"
                          ],
                      }}
                    />
                  </Fade>
                  <Fade bottom cascade>
                    {table?.rows?.Data[0]?.["table data sd"].map((tbl, idx) => {
                      return (
                        <div
                          key={idx}
                          dangerouslySetInnerHTML={{
                            __html: tbl["charge table"],
                          }}
                        />
                      );
                    })}
                  </Fade>
                </div>
              </div>
            </div>
          </div>
        </div>

        <LetUsHelp isEn={isEn} />
      </div>
    );
};

export default RateCharge;
