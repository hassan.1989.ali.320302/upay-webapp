import { Flipper, Flipped } from "react-flip-toolkit";
import Head from "next/head";
import "react-toastify/dist/ReactToastify.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "react-modal-video/scss/modal-video.scss";
import "../styles/App.scss";
import { AppProps } from "next/app";
import Router from "next/router";
import Layout from "../components/Layout";
import config from "react-reveal/globals";
import { useEffect, useState } from "react";
import { GTMPageView } from "../utils/gtm";

config({ ssrFadeout: true });

function MyApp({ Component, pageProps, router }: AppProps): JSX.Element {
  useEffect(() => {
    const handleRouteChange = (url: string) => GTMPageView(url);
    Router.events.on("routeChangeComplete", handleRouteChange);
    return () => {
      Router.events.off("routeChangeComplete", handleRouteChange);
    };
  }, []);

  useEffect(() => {
    import("react-facebook-pixel")
      .then((x) => x.default)
      .then((ReactPixel) => {
        ReactPixel.init(process.env.FB_PXL_ID);
        ReactPixel.pageView();

        Router.events.on("routeChangeComplete", () => {
          ReactPixel.pageView();
        });
      });
  }, []);

  return (
    <Flipper
      flipKey={router.asPath}
      spring={{ stiffness: 130, damping: 17 }}
      staggerConfig={{ default: { reverse: true } }}
    >
      <Flipped flipId="page">
        <div className="App">
          <Head>
            <title>Upay</title>
          </Head>
          <Layout>
            <Component {...pageProps} />
          </Layout>
        </div>
      </Flipped>
    </Flipper>
  );
}

export default MyApp;
