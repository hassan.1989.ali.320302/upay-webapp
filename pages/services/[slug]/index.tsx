import React, { Component, useEffect, useState } from "react";
import Banner from "../../../components/Banner";
import { GetStaticProps, InferGetStaticPropsType } from "next";
import { getInnerPage } from "../../../server";
import { Locales } from "../../../components/Header";
import GetApp from "../../../components/GetApp";
import Link from "next/link";
import Fade from "react-reveal/Fade";
import LetUsHelp from "../../../components/LetUsHelp";
import { useRouter } from "next/router";

export const getStaticPaths = async () => {
  return {
    paths: [{ params: { slug: "remittance" } }],
    fallback: true,
  };
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  let page: {
    data: object;
    error: string;
  };
  try {
    const res = await getInnerPage(params.slug);
    // console.log("resss", res);
    page = {
      data: res.data,
      error: "",
    };
  } catch (err) {
    page = {
      data: {},
      error: "Something Went Wrong!",
    };
  }

  return {
    props: {
      page,
    },
    revalidate: 1,
  };
};

const UCBLServices = ({
  page,
}: InferGetStaticPropsType<typeof getStaticProps>): JSX.Element => {
  const [isEn, setIsEn] = useState<boolean>(true);
  const router = useRouter();

  useEffect(() => {
    if (typeof window !== "undefined") {
      +localStorage.getItem("locale") === Locales.English && setIsEn(false);

      window.addEventListener("locale", handleLocaleEvent, false);

      return () => {
        window.removeEventListener("locale", handleLocaleEvent);
      };
    }
  }, []);

  const handleLocaleEvent = () => {
    +localStorage.getItem("locale") === Locales.English
      ? setIsEn(false)
      : setIsEn(true);
  };

  const pageData = page?.data?.rows?.Data[0];
  const error = page?.error;

  if (router.isFallback) {
    return <div>Loading...</div>;
  } else if (error) {
    return <div>{error}</div>;
  } else
    return (
      <div>
        <Banner
          name={isEn ? pageData?.title : pageData?.["title bn"]}
          cover={{
            image: pageData?.["cover image"],
            imagesd: pageData?.["cover image sd"],
          }}
        />

        <section>
          <div className="paybill">
            <div className="container">
              <div className="row">
                <div className="col-xl-12">
                  <div className="paybill__items">
                    <Fade bottom>
                      <p
                        className="paybill__items-text"
                        dangerouslySetInnerHTML={{
                          __html: isEn ? pageData?.desc : pageData?.["desc bn"],
                        }}
                      ></p>
                    </Fade>
                    <div className="paybill__items-links">
                      <div className="row">
                        <div className="col-xl-6 col-lg-6 col-md-12">
                          <Fade left cascade>
                            <div className="paybill__items-links-desc">
                              {pageData?.["segment list"].map((item, key) => {
                                return (
                                  <p
                                    key={key}
                                    dangerouslySetInnerHTML={{
                                      __html: isEn
                                        ? item?.list
                                        : item?.["list bn"],
                                    }}
                                  ></p>
                                );
                              })}
                            </div>
                          </Fade>
                        </div>

                        <Fade right>
                          {pageData?.["segment one"].map((item, key) => {
                            const url = item["slug url"].split("/")[
                              item["slug url"].split("/").length - 1
                            ];
                            return (
                              <div
                                className="col-xl-3 col-lg-3 col-md-6 "
                                key={key}
                              >
                                <Link href={`/services/details/${url}`}>
                                  <a
                                    className="paybill__card"
                                    style={{
                                      backgroundImage: `linear-gradient(180deg,rgba(0, 84, 166, 0.4) 0%,rgba(0, 84, 166, 0.8) 100%),url(${
                                        process.env.IMAGE_URL
                                      }${
                                        typeof window !== "undefined" &&
                                        window?.screen?.width > 425
                                          ? item.image
                                          : item["image sd"]
                                      })`,
                                    }}
                                  >
                                    <div className="paybill__card_desc paybill__icon">
                                      <picture>
                                        <source
                                          media="(min-width:650px)"
                                          srcSet={
                                            process.env.IMAGE_URL + item?.icon
                                          }
                                        />
                                        <source
                                          media="(max-width:649px)"
                                          srcSet={
                                            process.env.IMAGE_URL +
                                            item?.["icon sd"]
                                          }
                                        />
                                        <img
                                          src={
                                            process.env.IMAGE_URL + item?.icon
                                          }
                                          alt="Cash in Ucbl"
                                        />
                                      </picture>
                                      <p
                                        dangerouslySetInnerHTML={{
                                          __html: isEn
                                            ? item?.title
                                            : item?.["title bn"],
                                        }}
                                      ></p>
                                    </div>
                                  </a>
                                </Link>
                              </div>
                            );
                          })}
                        </Fade>
                        {/* <div className="col-xl-3 col-lg-3 col-md-6">
                        <a href="#!" className="paybill__card">
                          <div className="paybill__card_desc">
                            <img src="/images/electricity.svg" alt="" />
                            <p>Upay Agent</p>
                          </div>
                        </a>
                      </div>

                      <div className="col-xl-3 col-lg-3 col-md-6">
                        <a href="#!" className="paybill__card">
                          <div className="paybill__card_desc">
                            <img src="/images/electricity.svg" alt="" />
                            <p>Debit / Credit Cards</p>
                          </div>
                        </a>
                      </div> */}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        {/* <section>
          <div>
            <div className="get__app-mainPart">
              <div className="container">
                <div className="row">
                  <div className="col-xl-5 col-lg-5 col-md-6 custom__col">
                    <Fade left cascade>
                      <div className="left__part">
                        <h2>{isEn ? GetApp.get_app : GetApp.get_app_bn}</h2>
                        <p
                          dangerouslySetInnerHTML={{
                            __html: isEn
                              ? GetApp.get_app_desc
                              : GetApp.get_app_desc_bn,
                          }}
                        ></p>
                      </div>
                    </Fade>
                  </div>
                  <div className="col-xl-7 col-lg-7 col-md-6 custom__col">
                    <Fade right cascade>
                      <div className="right__part row">
                        {pageData["Segment two"][0]["Under Segment two"].map(
                          (item, key) => {
                            return (
                              <a href="#!" key={key} className="play_store-btn">
                                <picture>
                                  <source
                                    media="(min-width:650px)"
                                    srcSet={process.env.IMAGE_URL + item.image}
                                  />
                                  <source
                                    media="(max-width:649px)"
                                    srcSet={
                                      process.env.IMAGE_URL + item["image sd"]
                                    }
                                  />
                                  <img
                                    src={process.env.IMAGE_URL + item.image}
                                    alt="Cash in Ucbl"
                                    style={{
                                      width: "100%",
                                    }}
                                  />
                                </picture>
                              </a>
                            );
                          }
                        )}
                        <a href="#!" className="play_store-btn">
                          <img
                            src="/images/Google-Play-btn-getAPP.svg"
                            alt=""
                          />
                        </a>
                        <a href="#!" className="apple_store-btn">
                          <img src="/images/Apple-App-btn-getAPP.svg" alt="" />
                        </a>
                      </div>
                    </Fade>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section> */}
        <GetApp isEn={isEn} />

        <LetUsHelp isEn={isEn} />
      </div>
    );
};

export default UCBLServices;
