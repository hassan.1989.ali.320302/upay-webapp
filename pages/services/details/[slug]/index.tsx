import React, { useEffect, useState } from "react";
import Banner from "../../../../components/Banner";
import { GetStaticProps, InferGetStaticPropsType } from "next";
import { getCashInPage, getInnerPage } from "../../../../server";
import { useRouter } from "next/router";
import { Locales } from "../../../../components/Header";
import Fade from "react-reveal/Fade";
import LetUsHelp from "../../../../components/LetUsHelp";
import ModalVideo from "react-modal-video";

export const getStaticPaths = async () => {
  return {
    paths: [
      { params: { slug: "upay-agent" } },
      { params: { slug: "Desco-prepaid" } },
    ],
    fallback: true,
  };
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  let page: {
    data: object;
    error: string;
  };
  try {
    // console.log("p", params.slug);
    const res = await getInnerPage(params.slug);
    page = {
      data: res.data,
      error: "",
    };
  } catch (err) {
    page = {
      data: {},
      error: "Something Went Wrong!",
    };
  }

  // console.log("pppp", page);

  return {
    props: {
      page,
    },
    revalidate: 1,
  };
};

function CashInDetails({
  page,
}: InferGetStaticPropsType<typeof getStaticProps>): JSX.Element {
  const router = useRouter();
  const pageData = page?.data?.rows?.data[0];
  // const page_tutorial = pageData?.["Tutorial Methods"][0];

  const [isEn, setIsEn] = useState<boolean>(true);
  const [isApp, setIsApp] = useState<boolean>(true);
  // const [videos, setVideos] = useState({
  //   main: page_tutorial?.["Tutorial"][0],
  //   others: page_tutorial?.["Tutorial"].slice(1),
  // });
  // const [videoId, setVideoId] = useState<String>("");
  // const [isOpen, setOpen] = useState(false);

  useEffect(() => {
    if (typeof window !== "undefined") {
      +localStorage.getItem("locale") === Locales.English && setIsEn(false);

      window.addEventListener("locale", handleLocaleEvent, false);

      return () => {
        window.removeEventListener("locale", handleLocaleEvent);
      };
    }
  }, []);

  const handleLocaleEvent = () => {
    +localStorage.getItem("locale") === Locales.English
      ? setIsEn(false)
      : setIsEn(true);
  };

  // const handleSuffle = (index) => {
  //   const restVids = videos.others;
  //   setVideoId(process.env.IMAGE_URL + restVids[index].video);
  //   setOpen(true);

  //   setVideos({
  //     main: restVids[index],
  //     others: [...restVids.filter((item, idx) => idx !== index), videos.main],
  //   });
  // };

  if (router.isFallback) {
    return <div>Loading...</div>;
  } else if (page?.error) {
    return <div className="container">{page?.error}</div>;
  } else
    return (
      <div>
        <Banner
          name={isEn ? pageData?.tiitle : pageData?.["title bn"]}
          cover={{
            image: pageData?.["background image"],
            imagesd: pageData?.["background image sd"],
          }}
        />

        <div className="prepaid__page--mainSection">
          <div className="top__part">
            <div className="container">
              <div className="row">
                <Fade left cascade>
                  <div className="col-xl-12">
                    <h2
                      dangerouslySetInnerHTML={{
                        __html: isEn
                          ? pageData?.heading
                          : pageData?.["heading bn"],
                      }}
                    ></h2>
                    <p
                      dangerouslySetInnerHTML={{
                        __html: isEn ? pageData?.desc : pageData?.["desc bn"],
                      }}
                    ></p>
                  </div>
                </Fade>
              </div>
            </div>
          </div>

          <div className="mid__prepaid__billApp--use">
            <div className="container">
              <div className="row">
                <div className="col-xl-12">
                  <Fade bottom cascade>
                    <div className="dashmid__wDesc__header--part">
                      <h1
                        dangerouslySetInnerHTML={{
                          __html: isEn
                            ? pageData?.Methods[0]?.title
                            : pageData?.Methods[0]?.["title bn"],
                        }}
                      ></h1>
                      <p
                        dangerouslySetInnerHTML={{
                          __html: isEn
                            ? pageData?.Methods[0]?.desc
                            : pageData?.Methods[0]?.["desc bn"],
                        }}
                      ></p>
                    </div>
                  </Fade>
                  <Fade bottom cascade>
                    {isApp ? (
                      <div className="app__use--imgSec">
                        <div className="row justify-content-center prepaid__app--row">
                          {pageData?.Methods[0]?.["Try onApp"].map(
                            (appImg, key) => {
                              return (
                                <div
                                  className="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-7"
                                  key={key}
                                >
                                  <div className="img__part">
                                    <picture>
                                      <source
                                        media="(min-width:650px)"
                                        srcSet={
                                          process.env.IMAGE_URL + appImg?.image
                                        }
                                      />
                                      <source
                                        media="(max-width:649px)"
                                        srcSet={
                                          process.env.IMAGE_URL +
                                          appImg?.["image sd"]
                                        }
                                      />
                                      <img
                                        src={
                                          process.env.IMAGE_URL + appImg?.image
                                        }
                                        alt="Pay Bill Ucbl"
                                      />
                                    </picture>
                                  </div>
                                </div>
                              );
                            }
                          )}
                        </div>
                        <div className="center__btn-part">
                          <div className="right__arrow--btnPart">
                            <a
                              onClick={() => setIsApp((prev) => !prev)}
                              className="btn right__arrow--btn secondaryY__bg--btn inline__blck--btn"
                            >
                              Try It On *268#
                            </a>
                          </div>
                        </div>
                      </div>
                    ) : (
                      <div className="bill__pay--imgSec">
                        <div className="row justify-content-center">
                          {pageData?.Methods[0]?.["Try On USSD images"].map(
                            (ussdImg, key) => {
                              return (
                                <div
                                  className="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6"
                                  key={key}
                                >
                                  <div className="img__part">
                                    <picture>
                                      <source
                                        media="(min-width:650px)"
                                        srcSet={
                                          process.env.IMAGE_URL +
                                          ussdImg?.["image "]
                                        }
                                      />
                                      <source
                                        media="(max-width:649px)"
                                        srcSet={
                                          process.env.IMAGE_URL +
                                          ussdImg?.["image sd"]
                                        }
                                      />
                                      <img
                                        src={
                                          process.env.IMAGE_URL +
                                          ussdImg?.["image "]
                                        }
                                        alt="Pay Bill Ucbl"
                                      />
                                    </picture>
                                  </div>
                                </div>
                              );
                            }
                          )}
                        </div>
                        <div className="center__btn-part">
                          <div className="right__arrow--btnPart">
                            <a
                              onClick={() => setIsApp((prev) => !prev)}
                              className="btn right__arrow--btn secondaryY__bg--btn inline__blck--btn"
                            >
                              Try It On App
                            </a>
                          </div>
                        </div>
                      </div>
                    )}
                  </Fade>
                </div>
              </div>
            </div>
          </div>

          {/* <div className="top__video__tutorial--part">
            <Fade bottom cascade>
              <div className="dashmid__wDesc__header--part">
                <h1
                  dangerouslySetInnerHTML={{
                    __html: isEn
                      ? page_tutorial?.title
                      : page_tutorial?.["title bn"],
                  }}
                ></h1>
                <p
                  dangerouslySetInnerHTML={{
                    __html: isEn
                      ? page_tutorial?.desc
                      : page_tutorial?.["desc bn"],
                  }}
                ></p>
              </div>
            </Fade>
            <section className="main__tutorial--part">
              <div className="container">
                <div className="row">
                  <Fade left cascade>
                    <div className="col-xl-8 col-lg-8 col-md-12">
                      <div className="tutorial__primary--cardMain">
                        <div className="card tutorial__large-card">
                          <div className="top__video--part">
                            <picture>
                              <source
                                media="(min-width:650px)"
                                srcSet={
                                  process.env.IMAGE_URL +
                                  videos.main?.["thumb image"]
                                }
                              />
                              <source
                                media="(max-width:649px)"
                                srcSet={
                                  process.env.IMAGE_URL +
                                  videos.main?.["thumb image sd"]
                                }
                              />
                              <img
                                src={
                                  process.env.IMAGE_URL +
                                  videos.main?.["thumb image"]
                                }
                                alt="Pay Bill Ucbl Video"
                                className="card-img-top"
                                style={{
                                  borderTopLeftRadius: "calc(1.25rem - 1px)",
                                  borderTopRightRadius: "calc(1.25rem - 1px)",
                                }}
                              />
                            </picture>
                            <a
                              onClick={() => {
                                setOpen(true);
                                setVideoId(
                                  process.env.IMAGE_URL + videos.main.video
                                );
                              }}
                              className="play__icon"
                            >
                              <img src="/images/video-icon.svg" alt="" />
                            </a>
                          </div>
                          <div className="card-body">
                            <h5
                              className="vid-text card-title"
                              dangerouslySetInnerHTML={{
                                __html: isEn
                                  ? videos?.main?.title
                                  : videos?.main?.["title bn"],
                              }}
                            ></h5>
                            <p
                              className="card-text vid-text"
                              dangerouslySetInnerHTML={{
                                __html: isEn
                                  ? videos?.main?.desc
                                  : videos?.main?.["desc bn"],
                              }}
                            ></p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </Fade>
                  <Fade right cascade>
                    <div className="col-xl-4 col-lg-4 col-md-12">
                      <div className="scrolling__selectCard--slide">
                        <div className="row scrolling__selectCard--slide-row">
                          {videos.others?.map((vid, key: number) => {
                            return (
                              <div
                                className="col-xl-12 col-lg-12 col-md-5 col-sm-5 col-5 custom__col"
                                key={key}
                              >
                                <div className="small__tutorial-cardMain">
                                  <a href="#!" className="video__select-card">
                                    <picture>
                                      <source
                                        media="(min-width:650px)"
                                        srcSet={
                                          process.env.IMAGE_URL +
                                          vid?.["thumb image"]
                                        }
                                        // className="card-img-top"
                                      />
                                      <source
                                        media="(max-width:649px)"
                                        srcSet={
                                          process.env.IMAGE_URL +
                                          vid?.["thumb image sd"]
                                        }
                                        // className="card-img-top"
                                      />
                                      <img
                                        src={
                                          process.env.IMAGE_URL +
                                          vid?.["thumb image"]
                                        }
                                        alt="Pay Bill Ucbl Video"
                                        // className="card-img-top"
                                        // style={{
                                        //   borderTopLeftRadius: "calc(1.25rem - 1px)",
                                        //   borderTopRightRadius: "calc(1.25rem - 1px)",
                                        // }}
                                      />
                                    </picture>
                                    <div className="desc__part vid-text">
                                      <h5
                                        dangerouslySetInnerHTML={{
                                          __html: isEn
                                            ? vid?.title
                                            : vid?.["title bn"],
                                        }}
                                      ></h5>
                                      <p
                                        dangerouslySetInnerHTML={{
                                          __html: isEn
                                            ? vid?.desc
                                            : vid?.["desc bn"],
                                        }}
                                      ></p>
                                    </div>
                                  </a>

                                  <a
                                    onClick={() => handleSuffle(key)}
                                    className="playicon"
                                  >
                                    <img
                                      src="/images/video-icon-small.svg"
                                      alt=""
                                    />
                                  </a>
                                </div>
                              </div>
                            );
                          })}
                        </div>
                      </div>
                    </div>
                  </Fade>
                </div>
              </div>
            </section>
          </div> */}
        </div>
        <LetUsHelp isEn={isEn} />

        {/* <ModalVideo
          channel="custom"
          autoplay
          isOpen={isOpen}
          url={videoId}
          onClose={() => setOpen(false)}
        /> */}
      </div>
    );
}

export default CashInDetails;
