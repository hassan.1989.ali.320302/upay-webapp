import React, { useEffect, useState } from "react";
import Banner from "../../../components/Banner";
import Footer from "../../../components/Footer";
import Header from "../../../components/Header";
import LetUsHelp from "../../../components/LetUsHelp";
import { GetStaticProps, InferGetStaticPropsType } from "next";
import { Locales } from "../../../components/Header";
import { getWhoWeArePage } from "../../../server";
import { useRouter } from "next/router";

export const getStaticPaths = async () => {
  const apiResult = await getWhoWeArePage();

  const directorData = apiResult.data["rows"]["Data"][0]["Segment three"].map(
    (item) => item.title
  );

  const paths = directorData.map((item) => ({
    params: {
      slug: item,
    },
  }));

  // console.log("paths", paths);

  return {
    paths,
    fallback: true,
  };
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  let page: {
    data: object;
    error: string;
  };
  try {
    // console.log("p", params.slug);
    const res = await getWhoWeArePage();

    page = {
      data: res.data,
      error: "",
    };
  } catch (err) {
    page = {
      data: {},
      error: "Something Went Wrong!",
    };
  }

  // console.log("pppp", page);

  return {
    props: {
      page,
    },
    revalidate: 1,
  };
};

const BoardOfDirectorSingle = ({
  page,
}: InferGetStaticPropsType<typeof getStaticProps>): JSX.Element => {
  const router = useRouter();
  const pageData = page?.data?.rows?.Data[0];
  const content = pageData?.["Segment three"][0];
  const director = content?.["Under Segment three"].filter(
    (item) => item.title === router.query.slug
  )[0];
  const error = page?.error;
  // console.log(pageData);
  const [isEn, setIsEn] = useState<boolean>(true);

  useEffect(() => {
    if (typeof window !== "undefined") {
      +localStorage.getItem("locale") === Locales.English && setIsEn(false);

      window.addEventListener("locale", handleLocaleEvent, false);

      return () => {
        window.removeEventListener("locale", handleLocaleEvent);
      };
    }
  }, []);

  const handleLocaleEvent = () => {
    +localStorage.getItem("locale") === Locales.English
      ? setIsEn(false)
      : setIsEn(true);
  };

  if (router.isFallback) {
    return <div>Loading...</div>;
  } else if (error) {
    return <div className="container">{error}</div>;
  } else
    return (
      <div>
        <Banner
          name={isEn ? content.title : content?.["title bn"]}
          cover={{
            image: pageData?.["cover image"],
            imagesd: pageData?.["cover image sd"],
          }}
        />
        <div className="board-of-director-singlepage">
          <div className="container">
            <div className="row">
              <div className="col-xl-10 mx-auto">
                <div className="top__imagesec">
                  <picture>
                    <source
                      media="(min-width:650px)"
                      srcSet={process.env.IMAGE_URL + director?.["squre image"]}
                    />
                    <source
                      media="(max-width:649px)"
                      srcSet={
                        process.env.IMAGE_URL + director?.["squre image sd"]
                      }
                    />
                    <img
                      src={process.env.IMAGE_URL + director?.["squre image"]}
                      alt="Director"
                      // className="director-icon"
                    />
                  </picture>
                </div>
                <div className="bottom__desc">
                  <h1
                    dangerouslySetInnerHTML={{
                      __html: isEn ? director?.title : director?.["title bn"],
                    }}
                  ></h1>
                  <p
                    className="designation"
                    dangerouslySetInnerHTML={{
                      __html: isEn
                        ? director?.position
                        : director?.["position bn"],
                    }}
                  ></p>
                  <p
                    className="designation_desc"
                    dangerouslySetInnerHTML={{
                      __html: isEn ? director?.desc : director?.["desc bn"],
                    }}
                  ></p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <LetUsHelp isEn={"fd"} />
      </div>
    );
};

export default BoardOfDirectorSingle;
