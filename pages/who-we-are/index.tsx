import React, { useEffect, useState } from "react";
import Banner from "../../components/Banner";
import StorySlider from "../../components/StorySlider";
import OurValueSlider from "../../components/OurValueSlider";
import LetsHelpSlider from "../../components/LetsHelpSlider";
import { GetStaticProps, InferGetStaticPropsType } from "next";
import { getWhoWeArePage } from "../../server";
import { Locales } from "../../components/Header";
import Fade from "react-reveal/Fade";
import LetUsHelpText from "../../translations/letushelp.json";
import WhoWeAreData from "../../translations/whoweare.json";
import ButtonText from "../../translations/button.json";
import LetUsHelp from "../../components/LetUsHelp";
import Link from "next/link";

export const getStaticProps: GetStaticProps = async (context) => {
  let page: {
    data: object;
    error: string;
  };
  try {
    const res = await getWhoWeArePage();
    page = {
      data: await res.data,
      error: "",
    };
  } catch (err) {
    page = {
      data: {},
      error: "Something Went Wrong!",
    };
  }

  return {
    props: {
      page,
    },
    revalidate: 1,
  };
};

const WhoWeAre = ({
  page: { data, error },
}: InferGetStaticPropsType<typeof getStaticProps>): JSX.Element => {
  const [isEn, setIsEn] = useState<boolean>(true);
  const [readMore, setReadMore] = useState<boolean>(false);

  useEffect(() => {
    if (typeof window !== "undefined") {
      +localStorage.getItem("locale") === Locales.English && setIsEn(false);

      window.addEventListener("locale", handleLocaleEvent, false);

      return () => {
        window.removeEventListener("locale", handleLocaleEvent);
      };
    }
  }, []);

  const handleLocaleEvent = () => {
    +localStorage.getItem("locale") === Locales.English
      ? setIsEn(false)
      : setIsEn(true);
  };

  const pageData = data?.rows?.Data[0];
  const storyBaseSize = isEn
    ? pageData?.["segment one"][0].desc.split(".").length
    : pageData?.["segment one"][0]?.["desc bn"].split(".");

  if (error) {
    <div>
      <div className="mt-5">{error}</div>
    </div>;
  } else {
    return (
      <div>
        <Banner
          name={isEn ? pageData?.["cover title"] : pageData?.["cover title bn"]}
          cover={{
            image: pageData?.["cover image"],
            imagesd: pageData?.["cover image sd"],
          }}
        />

        <div className="story__slider--mainPart">
          <StorySlider sliders={pageData?.Slider} />
        </div>
        <div className="our__story--descMain bottom__margin">
          <div className="container">
            <div className="row">
              <Fade left cascade>
                <div className="col-xl-12">
                  <div className="dashleft__header--part">
                    <h1
                      dangerouslySetInnerHTML={{
                        __html: isEn
                          ? pageData?.["segment one"][0].title
                          : pageData?.["segment one"][0]?.["title bn"],
                      }}
                    ></h1>
                  </div>
                  {/* <p
                  dangerouslySetInnerHTML={{
                    __html: isEn
                      ? data.rows["our story"][0].description
                      : data.rows["our story"][0].descriptionbn,
                  }}
                ></p> */}

                  {readMore ? (
                    <Fade collapse when={readMore} appear={readMore}>
                      <p
                        dangerouslySetInnerHTML={{
                          __html: isEn
                            ? pageData?.["segment one"][0].desc
                            : pageData?.["segment one"][0]?.["desc bn"],
                        }}
                      ></p>
                    </Fade>
                  ) : (
                    <p
                      dangerouslySetInnerHTML={{
                        __html: isEn
                          ? pageData?.["segment one"][0].desc.substring(
                              0,
                              1000
                            ) + "..."
                          : pageData?.["segment one"][0]?.["desc bn"].substring(
                              0,
                              1000
                            ) + "...",
                      }}
                    ></p>
                  )}

                  {storyBaseSize > 10 && !readMore && (
                    <div className="right__arrow--btnPart">
                      <a
                        onClick={() => setReadMore((prev) => !prev)}
                        className="btn right__arrow--btn blue__bg--btn inline__blck--btn"
                      >
                        {isEn
                          ? ButtonText.read_more.en
                          : ButtonText.read_more.bn}{" "}
                        <svg
                          width="40"
                          height="24"
                          viewBox="0 0 40 24"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M26.4575 11H6.60938V13H26.4575V16L33.0515 12L26.4575 8V11Z"
                            fill="white"
                          />
                        </svg>
                      </a>
                    </div>
                  )}
                </div>
              </Fade>
            </div>
          </div>
        </div>

        <div className="our__values--mainPart tb__padding">
          <Fade bottom>
            <div className="dashmid__wDesc__header--part">
              <h1
                dangerouslySetInnerHTML={{
                  __html: isEn
                    ? pageData?.["Segment two"][0]?.title
                    : pageData?.["Segment two"][0]?.["title bn"],
                }}
              ></h1>
              <p
                dangerouslySetInnerHTML={{
                  __html: isEn
                    ? pageData?.["Segment two"][0]?.desc
                    : pageData?.["Segment two"][0]?.["desc bn"],
                }}
              ></p>
            </div>
          </Fade>

          <div className="our__values--sliderMain">
            <div className="container">
              <div className="row">
                <Fade bottom>
                  <div className="col-xl-12 col-lg-11 mx-auto">
                    <OurValueSlider
                      values={
                        pageData?.["Segment two"][0]?.["Under Segment two"]
                      }
                      isEn={isEn}
                    />
                  </div>
                </Fade>
              </div>
            </div>
          </div>
        </div>

        <div className="board__director--mainPart tb__padding">
          <div className="container">
            <div className="row">
              <div className="col-xl-12">
                <Fade bottom>
                  <div className="dashleft__wDesc__header--part">
                    <h1
                      dangerouslySetInnerHTML={{
                        __html: isEn
                          ? pageData?.["Segment three"][0].title
                          : pageData?.["Segment three"][0]["title bn"],
                      }}
                    ></h1>
                    <p
                      dangerouslySetInnerHTML={{
                        __html: isEn
                          ? pageData?.["Segment three"][0].desc
                          : pageData?.["Segment three"][0]["desc bn"],
                      }}
                    ></p>
                  </div>
                </Fade>

                <div className="board__members--listsPart">
                  <div className="row">
                    {pageData?.["Segment three"][0]?.[
                      "Under Segment three"
                    ].map((director, key: number) => {
                      return (
                        <div className="col-xl-6 col-lg-6" key={key}>
                          <Fade
                            left={key % 2 === 0 && true}
                            right={key % 2 !== 0 && true}
                          >
                            <div
                              className={
                                key % 2 === 0
                                  ? "card board__directior__left--card"
                                  : "card board__directior__right--card"
                              }
                            >
                              <div className="img__right__part">
                                <picture>
                                  <source
                                    media="(max-width: 767px)"
                                    srcSet={
                                      process.env.IMAGE_URL +
                                      director?.["image sd"]
                                    }
                                  />
                                  <source
                                    media="(min-width: 768px)"
                                    srcSet={
                                      process.env.IMAGE_URL + director.image
                                    }
                                  />
                                  <img
                                    src={
                                      process.env.IMAGE_URL +
                                      director?.["image sd"]
                                    }
                                    alt="sliders"
                                  />
                                </picture>
                              </div>
                              <div className="card-body">
                                <div className="main__intro">
                                  <h5
                                    className="card-title"
                                    dangerouslySetInnerHTML={{
                                      __html: isEn
                                        ? director.title
                                        : director["title bn"],
                                    }}
                                  ></h5>
                                  <p
                                    className="card-text-iden"
                                    dangerouslySetInnerHTML={{
                                      __html: isEn
                                        ? director.position
                                        : director["position bn"],
                                    }}
                                  ></p>
                                  <p
                                    className="card-text"
                                    dangerouslySetInnerHTML={{
                                      __html: isEn
                                        ? director.desc.substring(0, 100) +
                                          "..."
                                        : director["desc bn"].substring(
                                            0,
                                            100
                                          ) + "...",
                                    }}
                                  ></p>
                                  <br />
                                  <Link href={`/who-we-are/${director.title}`}>
                                    <a className="director__readmore">
                                      Read More
                                    </a>
                                  </Link>
                                </div>
                              </div>
                            </div>
                          </Fade>
                        </div>

                        // <div className="col-xl-6 col-lg-6">
                        //   <div className="card board__directior__right--card">
                        //     <div className="img__right__part">
                        //       <img
                        //         className="card-img-top"
                        //         src="/images/director__img-two.png"
                        //         alt="Card image cap"
                        //       />
                        //     </div>
                        //     <div className="card-body">
                        //       <div className="main__intro">
                        //         <h5 className="card-title">Mr. john smith</h5>
                        //         <p className="card-text-iden">Managing Director</p>
                        //         <p className="card-text">
                        //           Lorem ipsum dolor sit amet consectetur adipiscing
                        //           elit sagittis tempor integer natoque arcu,
                        //           suspendisse ullamcorper urna curae commodo cursus
                        //           ad dui euismod ridiculus felis.
                        //         </p>
                        //       </div>
                        //     </div>
                        //   </div>
                        // </div>
                      );
                    })}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <LetUsHelp isEn={isEn} />
      </div>
    );
  }
};

export default WhoWeAre;
