import React, { useEffect, useState } from "react";
import Banner from "../../components/Banner";
import { GetStaticProps, InferGetStaticPropsType } from "next";
import LetUsHelp from "../../components/LetUsHelp";
import { getMediaCenter, getMediaContact } from "../../server";
import { Locales } from "../../components/Header";
import Fade from "react-reveal/Fade";

export const getStaticProps: GetStaticProps = async (context) => {
  let page: {
    data: object;
    error: string;
  };
  try {
    const res = await getMediaContact();
    // console.log("resss", res);
    page = {
      data: res.data,
      error: "",
    };
  } catch (err) {
    page = {
      data: {},
      error: "Something Went Wrong!",
    };
  }

  return {
    props: {
      page,
    },
    revalidate: 1,
  };
};

const MediaContact = ({
  page: { data, error },
}: InferGetStaticPropsType<typeof getStaticProps>): JSX.Element => {
  const pageData = data?.rows?.Data[0];
  const content = pageData?.["segment one"][0];
  const [isEn, setIsEn] = useState<boolean>(true);

  useEffect(() => {
    if (typeof window !== "undefined") {
      +localStorage.getItem("locale") === Locales.English && setIsEn(false);

      window.addEventListener("locale", handleLocaleEvent, false);

      return () => {
        window.removeEventListener("locale", handleLocaleEvent);
      };
    }
  }, []);

  const handleLocaleEvent = () => {
    +localStorage.getItem("locale") === Locales.English
      ? setIsEn(false)
      : setIsEn(true);
  };

  if (error) {
    return <div>{error}</div>;
  } else
    return (
      <div>
        <Banner
          name={isEn ? pageData.title : pageData["title bn"]}
          cover={{
            image: pageData["cover image"],
            imagesd: pageData["cover image sd"],
          }}
        />
        <section>
          <div className="main__details--part tb__padding">
            <div className="container">
              <div className="row">
                <Fade left cascade>
                  <div className="col-xl-12">
                    <h2
                      dangerouslySetInnerHTML={{
                        __html: isEn ? content?.title : content?.["title bn"],
                      }}
                    ></h2>
                    <p
                      dangerouslySetInnerHTML={{
                        __html: isEn ? content?.desc : content?.["desc bn"],
                      }}
                    ></p>
                  </div>
                </Fade>
              </div>
            </div>
          </div>
        </section>
        <LetUsHelp isEn={isEn} />
      </div>
    );
};

export default MediaContact;
